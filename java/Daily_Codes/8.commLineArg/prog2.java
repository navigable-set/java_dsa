/*if both the string array are same...i.e string array in main and string array on command line...(both will go on heap) their identityHashCode() will be different because cmd-line vrn yenarya string ya runtime array/dynamic array chya aahet..aani te "new" ne jat..mhanje tyacha kahi jri zal tri nvin gola bnnar ch aahe..so same string jri asli main madhlya string array chya string sarkhi or cmd-line vrchya pn same jri astil ..tri tyancha vegla gola bnel*/
class Demo{

	public static void main(String ... shweta){	

		String str[] = {"shweta","divya","dikshu"};
		
		//string array in main()
		for(String x : str){
			System.out.println(x + " " + System.identityHashCode(x));	
		}

		//string array from command line
		for(String x : shweta){
			System.out.println(x + " " + System.identityHashCode(x));	
		}
	}
}
