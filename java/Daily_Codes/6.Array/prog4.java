//we can declare object as global...but can be accesible only using object

import java.io.*;
class Demo{
	 int arr1[] = new int[4];	//global

	void fun(){

		System.out.println(arr1[0]);
		//System.out.println(arr[0]);--> Error : can not find symbol..i.e. arr
	}
	
	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		float arr[] = new float[4];	//local
		
		for(int i=0;i<4;i++)
			System.out.println(arr[i]);
			
		//object creation and accessing array with object
		Demo obj = new Demo();
		obj.fun();

		System.out.println(obj.arr1);
		System.out.println(obj.arr1[0]=10);
		
	}
}
