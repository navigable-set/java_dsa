class Demo{

	public static void main(String []  shweta){

		int arr1[] = {10,20,30,40,50};	
		float arr2[] = {10,20,30,40,50};	
		//{} ne pn heap vrch memory bhette..it is same as ==> float arr2[] = new float[]{10,20,30,40,50}; ==>i.e. internally 'new' mhanun ch jato..mhanun heap ve memory milte
		//don different object bntil ek integerArray( [I ) aani dusra floatArray( [F ) type cha..aani data same asla tri he data sharing nahi krt karn float aahe
		System.out.println("arr1 : " +arr1);
		System.out.println("arr2 : " +arr2);

		//getClass() hi 'Object' class chi method aahe..aani hi fkr objects la ch chalte..karn to object kontya class cha aahe te sangte
		System.out.println("arr1 : " + arr1.getClass());	//class [I => Runtime Signature ==> '[' represents array aani 'I' is for Integer i.e Integer type array cha class
		System.out.println("arr2 : " + arr2.getClass());	//class [F => Runtime Signature	 ==> '[' represents array aani 'I' is for Integer i.e Float type array cha class==> Special type of class	
	}
}
