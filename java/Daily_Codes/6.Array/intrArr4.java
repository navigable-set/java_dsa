//identityHashCode() for same values

class Demo{

	public static void main(String [] shweta){

		int arr1[] = {10,20,300,40};
		int arr2[] = {10,20,300,40};

		System.out.println("arr1 : " + arr1);
		System.out.println("arr2 : " + arr2);
		
		System.out.println();

		//char aani int la applicable aahe only
		//same data within limit/range(-128 to 127)..so 'SAME' identityHashCode for '10'
		System.out.println("arr1[0] = 10 : " + System.identityHashCode(arr1[0]));
		System.out.println("arr2[0] = 10 : " + System.identityHashCode(arr2[0]));
		
		System.out.println();

		//same data but not in limit/range(-128 to 127)..so 'DIFFERENT' identityHashCode (i.e. unique number of object) for '300'
		System.out.println("arr1[2] =300 : " + System.identityHashCode(arr1[2]));
		System.out.println("arr2[2] =300 : " + System.identityHashCode(arr2[2]));

		float arr1[] = {10,20,300,40};
		float arr2[] = {10,20,300,40};

		System.out.println("arr1 : " + arr1);
		System.out.println("arr2 : " + arr2);
		
		System.out.println();
		
		//not app;icable to float...that's why different number(identityHasgCode) for same element
		System.out.println("arr1[0] = 10 : " + System.identityHashCode(arr1[0]));
		System.out.println("arr2[0] = 10 : " + System.identityHashCode(arr2[0]));

	}
}
