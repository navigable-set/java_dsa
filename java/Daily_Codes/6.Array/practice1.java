class Demo{

	static void fun(int arr[]){

		int x =50, y =150,z =150;

		System.out.println("In fun :");
		
		System.out.println("x : 50 = " + System.identityHashCode(x));
		System.out.println("y : 150 = " + System.identityHashCode(y));
		System.out.println("z : 150 = " + System.identityHashCode(y));
		
		System.out.println("arr :" + System.identityHashCode(arr));
		
		System.out.println("arr[0] : 50 = " + System.identityHashCode(arr[0]));
		System.out.println("arr[1] : 10 = " + System.identityHashCode(arr[1]));
		System.out.println("arr[2] : 15 = " + System.identityHashCode(arr[2]));
		
		
		arr[0] = arr[0] +10;
		arr[1] = arr[1] +10;
		arr[2] = arr[2] +200;
		System.out.println("In fun after modification:");
		
		System.out.println("arr[0] : 60 = " + System.identityHashCode(arr[0]));
		System.out.println("arr[1] : 20 = " + System.identityHashCode(arr[1]));
		System.out.println("arr[2] : 215 = " + System.identityHashCode(arr[2]));
	}

	public static void main(String [] shweta){
		int arr[] = {50,10,15};
		int x =50, y =150,z =150;
		System.out.println("In main :");
		
		System.out.println("x : 50 = " + System.identityHashCode(x));
		System.out.println("y : 150 = " + System.identityHashCode(y));
		System.out.println("z : 150 = " + System.identityHashCode(y));
		
		System.out.println("arr :" + System.identityHashCode(arr));
		
		System.out.println("arr[0] : 50 = " + System.identityHashCode(arr[0]));
		System.out.println("arr[1] : 10 = " + System.identityHashCode(arr[1]));
		System.out.println("arr[2] : 15 = " + System.identityHashCode(arr[2]));
		

		fun(arr);
		System.out.println("in main after fun ");
		System.out.println("arr[0] : 60 = " + System.identityHashCode(arr[0]));
		System.out.println("arr[1] : 20 = " + System.identityHashCode(arr[1]));

		//why diff. id for the below one in both function for same array : as '215' is beyond the limit i.e. 127 ..its object will not be created in Integer cache...it will be created on heap(outside Integer cache) section separately..no common reference allowed (outsize integer cache) on heap ...and the thing is that even though the data is same  **diff object get created for same data on heap if its beyond the limit**
		System.out.println("arr[2] : 215 = " + System.identityHashCode(arr[2]));
	}
}
