/* ARRAY DECLARATION TYPES (including JAGGED ARRAY)*/

class Demo{

	public static void main(String [] shweta){
		
		//types of declaring array
		int arr1[][] = {{10,20},{30,40,50},{60,70,80,90}};
		
		int arr2[][] = new int [][] {{10,20},{30,40,50},{60,70,80,90}} ;

		int arr3[][] = new int [3][];	//2d-array cha object bnlay

		arr3[0] = new int[]{10,20};	//1d-array cha object bnel
		arr3[1] = new int[]{30,40,50};
		arr3[2] = new int[]{60,70,80,90};

		int arr4[][] = {new int[] {10,20},new int[]{30,40,50},new int[]{60,70,80,90}};

		//accessing 2d array using for-each loop
		System.out.println("arr1 :");
		for(int x[] : arr1){
			for(int y : x){
				System.out.print(y + "\t");
			}
			System.out.println();
		}
		
		System.out.println("arr2 :");
		for(int x[] : arr2){
			for(int y : x){
				System.out.print(y + "\t");
			}
			System.out.println();
		}

		System.out.println("arr3 :");
		for(int x[] : arr3){
			for(int y : x){
				System.out.print(y + "\t");
			}
			System.out.println();
		}

		System.out.println("arr4 :");
		for(int x[] : arr4){
			for(int y : x){
				System.out.print(y + "\t");
			}
			System.out.println();
		}
	}
}
