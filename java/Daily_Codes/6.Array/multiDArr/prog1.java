/*2D array : are the multiple 1D array's
-->[r][] : in 2d ==> rows are compulsary ,columns are not
-->arr.length : given not no. of elements but the no. of 1d-array objects created
-->arr[0].length : will give the no. of elemnts /columns present in that respective 1d array
*/


class Demo{

	public static void main(String [] shweta){

		int arr[][] = new int[2][3];

		arr[0][0] = 1;
		arr[0][1] = 2;
		arr[0][2] = 3;
		arr[1][0] = 4;
		arr[1][1] = 5;
		arr[1][2] = 6;

		System.out.println("by for loop : ");
		for(int i=0; i<2; i++){
			for(int j=0; j<3; j++){
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}
		
		System.out.println("by for each loop : ");
		for(int x[] : arr){
			for(int y : x){
				System.out.print(y + "\t");
			}
			System.out.println();
		}
		System.out.println("address of 2d array object = arr : " + arr);
		System.out.println("address of 1d array object = arr[0] : " + arr[0]);
		System.out.println("data : arr[0][0] : " + arr[0][0]);
	}
}
