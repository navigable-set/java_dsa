//2D array : Input-from user (type 2)

import java.io.*;
class Demo{

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter rows :");
		int row = Integer.parseInt(br.readLine());	
		
		System.out.println("Enter colm :");
		int col = Integer.parseInt(br.readLine());	

		int arr[][] = new int[row][col];

		System.out.println("Enter array elements :");

		for(int i=0; i<arr.length; i++){		// or 'i<row'
			for(int j=0; j<arr[i].length; j++){	//or 'j<col'

				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		System.out.println("array elements are :");
		
		//using for-each loop

		for(int x[] : arr){
			for(int y : x){
				System.out.print(y + "\t");
			}
			System.out.println();
		}
	}
}
