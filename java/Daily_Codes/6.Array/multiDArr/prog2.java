/*2d array : 2D-array mdhe array chi length jr vicharli tr te no. of rows cha count det..mhanje kiti 1D-array che object bnlet te sangto
 1d-array : 1D-array madhe array chi length hi actual no. of elements aste(#or say colm in that one row only)
 BYTECODE CHECK KRR : 'multianewarray' assa  new ne multi-dimension sathi cha aary bntoy(ha ekch call aahe 'new' cha pn te multiple 1d array ch collection aahe...aani te internallay separate/vegvegle 1d array bnvl(jyancha reference te tevt original array chya object mdhe)..aani yach mule "jagged array possible zalay")
*/


class Demo{

	public static void main(String [] shweta){

		int arr[][] = new int[2][3];
		int arr1[] = new int[4];

		System.out.println("2d array length : " + arr.length);
		System.out.println("1d array length : " + arr1.length);

		arr[0][0] =10;
		arr[0][1] =20;
		arr[0][2] =30;
		arr[1][0] =40;
		arr[1][1] =50;
		arr[1][2] =60;

		System.out.println("data i.e arr[0][0] : " + arr[0][0]);			// 10	(actual data)
		System.out.println("1-d array object address : " + arr[0]);	// [I....(" [ " indicates 1d array of Integer)
		System.out.println("2-d array object address : " + arr);	// [[I...(" [[ " indicates 2d array of Integer)

	}
}
