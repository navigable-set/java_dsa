//2D array : Input-from user (type 1)

import java.io.*;
class Demo{

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter rows :");
		int row = Integer.parseInt(br.readLine());	
		
		System.out.println("Enter colm :");
		int col = Integer.parseInt(br.readLine());	

		int arr[][] = new int[row][col];

		System.out.println("Enter array elements :");

		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		System.out.println("array elements are :");
		
		for(int i=0; i<row; i++){
			for(int j=0; j<col; j++){
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}
	}
}
