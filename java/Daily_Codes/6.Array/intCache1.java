//INTEGR CACHE and AUTOBOXING

/*Integer cache : integer cache is a memory used for the better memory management in java...it is applicable to integers and characters only as characters are internally integer...its range is from -128 to 127(these values are almost of 1-byte only and are repetadely used/commonly used values that is why they are stored in the Integer cache area) ...it is like a special box/memory given for a integer ranging in between -128 to 127 ...
--> if multiple integer variables are there but they containing the same integer value that lies in between given range..then all those variables will refer to or have address of that one same memory containing the integer data, which is same in all other variables
--> can be varified using 'identityHashCode'
--> if same data is there..then only one gola/box will be created and is applicable all over the code
-->float/double/short/byte/boolean etc. Integer cache concept is not applicable to them*/

//dusra yenara data jr same asel tr navin gola nko bnvu..yach golya cha reference de..yalach 'integer cache' mhantat!!

class intCacheDemo{

	public static void main(String[] shweta){

		//'10' sathi unique box bnla or gola bnla..aani tyacha ch reference(address) ha 'y' aani 'z' ne ghetla
		int x = 10;
		int y = 10;

		//autoboxing concept ==> 'int' ha internally 'Integer' rapper class mdhe type cast houn ch jato..yalach 'aotoboxing' mhantat
		Integer z = 10;

		//Integr cha object bnvlay..ethe Integer cache chi concept nahi use hot..karn 'new'mule nvin gola bnto
		Integer a = new Integer(10);

		int p = 20;

		System.out.println("x = 10 : " + System.identityHashCode(x));
		System.out.println("y = 10 : " + System.identityHashCode(y));
		System.out.println("z = 10 : " + System.identityHashCode(z));
		System.out.println("a = 10 : " + System.identityHashCode(a));
		System.out.println("p = 20 : " + System.identityHashCode(p));
	}
}
