/*Array passing : function mdhn array mhanjech address pathvto across function...mhnun eka function ne kelela change dusrya function mdhn pn disto karn te ekach array kde bghtay/change kratay/reference aahe
 ==> mhanun JAVA INTERNALLY POINTER ch aahe pn java he 'reference' chya mage lpt*/

class Demo{

	//jya prakarcha data yeil..tya ch prakarach bhand lavaych

	static void fun(int xarr[]){

		System.out.println("In fun");

		System.out.println("xarr : " + System.identityHashCode(xarr));

		for(int x : xarr){
			System.out.println(x);
		}
		//[0] index change kelay.. jr pass krtanna same array i.e same address jr aala asel tr ha change tikde pn disel..mhanje ekch array aahe...jo aapn pass krtoy
		
		xarr[0] = 50;
	}

	public static void main(String [] args){

		int arr[] = {10,20,30,40,50};
		
		System.out.println("In main");
		
		System.out.println("arr : " + System.identityHashCode(arr));
		
		for(int x : arr){
			System.out.println(x);
		}

		//Sop(arr) ==> ethe to address dakvto tya object cha(jo heap vr create zalay aani jyacha address ha 'arr' ya pointer variable mdhe store aahe jyala jaga hi stack vr aahe)..mg jr pass krtanna "fun(arr)" aas lihil tr to 'arr' ha pn addredd ch asel na...ie. pointer..pn java mhant aamhi pointer la support krt nahi..pn java he internallly pointer ch use krt...pn te 'reference' chya mage lpt...aani actual address jatoy mg fun() mdhla 'xarr' pn tyach same original array kde bght asel jikde 'arr' bghtoy
//netanna pointer/address netoy..mg aapn fun() mdhn array mdhe je kahi change kru te ekde original mdhe pn reflect hotil karn same ch address/arral la te doghanche variable bghtay
		fun(arr);
		
		System.out.println("After fun");
		
		for(int x : arr){
			System.out.println(x);
		}
	}
}
