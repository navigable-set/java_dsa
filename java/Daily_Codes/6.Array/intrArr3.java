//types of array..accessing array...printing addresses of object created on heap...printing object's id using identityHashCode

class Demo{

	public static void main(String [] shweta){

		int arr1[] = new int[5];
		float arr2[] = new float[5];
		double arr3[] = new double[5];
		char arr4[] = new char[5];
		boolean arr5[] = new boolean[5];
		Demo arr6[] = new Demo[5];

		System.out.println("int : " + arr1);
		System.out.println("float : " + arr2);
		System.out.println("double : " + arr3);
		System.out.println("char : " + arr4);
		System.out.println("boolean : " + arr5);
		System.out.println("Demo class : " + arr6);

		for(int i=0; i<5; i++){
		
			System.out.println("int : " + arr1[i]);
			System.out.println("float : " + arr2[i]);
			System.out.println("double : " + arr3[i]);
			System.out.println("char : " + arr4[i]);
			System.out.println("boolean : " + arr5[i]);
			System.out.println("Demo class : " + arr6[i]);
			System.out.println();
		}
		
		//as we assign integer to int[]..we assign object of a class to same class array..eg.Demo[]
		Demo obj = new Demo();
		System.out.println("class Demo[] element : " + (arr6[0]=obj));
		
		//since 'obj' is also a objectname/varible/pointer variable(in stack) that has address of actual object i.e. gola which is present on the heap
		//address santoy tyachya aat mdhla
		System.out.println("variable obj : " + obj);
		//both giving same address because 'obj' refers to the one same object created on heap...
		//identityHashCode he unique number sangt tya object variable/pointer variable cha
		System.out.println("obj :" + System.identityHashCode( obj));
		System.out.println("arr6 :" +System.identityHashCode( arr6));
		System.out.println("arr6[0] :" +System.identityHashCode(arr6[0]));
		System.out.println("obj :" +System.identityHashCode(obj));
		System.out.println("arr6 :" +System.identityHashCode(arr6));
		System.out.println("arr6[0] :" +System.identityHashCode( arr6[0]));
	}
}
