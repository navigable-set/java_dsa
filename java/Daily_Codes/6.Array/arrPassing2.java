class Demo{

	static void fun(int arr[]){

		System.out.println("arr in fun :" + arr);	//same as of 'arr' in main..because both are referencing towards same object on heap
		for(int x : arr){
			System.out.println(x);
		}

		for(int i=0; i<arr.length; i++){
			arr[i] = arr[i] + 50;
		}
	}

	public static void main(String [] shweta){
	
		int arr[] = {50,100,150,200};
		int x = 50;
		
		System.out.println("arr[0] in main :" + System.identityHashCode(arr[0])); //arr[0] and 'x' have same data..that is 50..so integer cache concept is used..thats why same asnwer of both of them...mhanje te same address(where 50's object is present) kde bghtay
		System.out.println("x in main :" + System.identityHashCode(x));
		System.out.println("arr in main :" + System.identityHashCode(arr)); //arr is referencing towards the object of size 4 of integer type..and its(complete object) id will be different than others...tya heap vrchya golya chi id asel ti	
		System.out.println("arr in main :" + arr);	//ha address yeil jo arr mdhe store aahe

		fun(arr);

		System.out.println("arr in main :" + arr);
		for(int y : arr){
			System.out.println(y);
		}
	}
}
