/*note : not to override start() method
 * for multithreading : override run() and not to override start() method...
 *  karn Thread chya start() mdhn start0() which is native aani run() ya method la call aahe.. jila aapn nantr override krto..
 *  pn aapn jr start() override keli tr aaplya start() mdhe ti power nahia ki to thread la schedule krun run() la call krel!!
 * main thread is performing everything but new thread is also creates but is not scheduled*/

class MyThread extends Thread{

	public void run(){

		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}

	public void start(){
		System.out.println("In myThread start");
		run();
	}
}

class ThreadDemo{

	public static void main(String [] args){

		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());
	}
}
