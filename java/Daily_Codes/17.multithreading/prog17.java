/*concepts :
 * currentThrea() : ha jo thread working aahe or tyala jyane call kely to thread return krto
 * getName() : ya method la jya object vr call kela jato.. tya object ne jo thread create kela aahe na tyach nav dete
 * SOP(obj) == SOP(Thread.currentThread()) because crerntThread() pn Thread class cha object ch return krt asto*/

class MyThread extends Thread{

	public void run(){
		System.out.println("run : " + getName());	//thread0
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		MyThread obj = new MyThread();
		obj.start();

		//'main' ch return krto.. karn ethe running thread ha 'main' aahe.. konachya object vr call kely te matter nahi krt!!
		
		System.out.println(obj.currentThread());	//main
		System.out.println(obj.getName());		//thread0
		System.out.println(obj);			//Thread[thread0,5,main]
	}
}
