/*thread name*/

class MyThread extends Thread{

	public void run(){

		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());	//thread-0
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		System.out.println("In main");
		
		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());	//main
	}
}
