/*jya 'run()' vr aapla new thread kam krnar aahe.. ti run() aapn 'Thread class' chi run() method override krtoyy.. aani Thread class chi run() method hi exception throw krt nahii.. mhanun aapli overriding 'run()' method pn exception throw kru shakt nahii(error yeto)..mg ekch way aahe exception handling sathi to mhanje try-catch*/


class MyThread extends Thread{

	public void run(){

		for(int i=0; i<10; i++){
			System.out.println("In run");
			
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				
			}
		}
	}
}

class ThreadDemo{

	public static void main(String[] shweta) throws InterruptedException{

		MyThread obj = new MyThread();
		obj.start();

		for(int i=0; i<10; i++){
			System.out.println("In main");
			Thread.sleep(1000);
		}
	}
}
