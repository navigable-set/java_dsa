//

class Demo extends Thread{

	public void run(){

		System.out.println(Thread.currentThread().getName());
		setPriority(11);	//IllegalArgumentException
	}
}

class MyThread extends Thread{

	public void run(){

		System.out.println(Thread.currentThread().getName());
		setPriority(11);	//IllegalArgumentException
	}

}

class ThreadDemo{

	public static void main(String[] shweta){

		System.out.println(Thread.currentThread().getName());

		Demo obj1 = new Demo();
		obj1.start();

		MyThread obj2 = new MyThread();
		obj2.start();
	}
}


/*
Output :
main
Thread-0
Exception in thread "Thread-0" Thread-1
Exception in thread "Thread-1" java.lang.IllegalArgumentException
	at java.lang.Thread.setPriority(Thread.java:1094)
	at Demo.run(prog14.java:8)
java.lang.IllegalArgumentException
	at java.lang.Thread.setPriority(Thread.java:1094)
	at MyThread.run(prog14.java:17)
*/
