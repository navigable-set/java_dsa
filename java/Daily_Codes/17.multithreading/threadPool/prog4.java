//multiple ThreadPool bnvta yetat

import java.util.concurrent.*;

class Demo implements Runnable{

	int num =0;
	Demo(int num){
		this.num= num;
	}

	public void run(){
		System.out.println("Demo task = " + num + " " + Thread.currentThread().getName());
	}
}

class MyThread implements Runnable{

	int num =0;
	MyThread(int num){
		this.num= num;
	}

	public void run(){
		System.out.println("MyThread task = " + num + " " + Thread.currentThread().getName());
	}
}

class ThreadPoolDemo{

	public static void main(String[] shweta){

		ExecutorService es1 = Executors.newFixedThreadPool(5);			
		ExecutorService es2 = Executors.newFixedThreadPool(5);			

		//10 task aahet.. per pool 5 threads aahet
		for(int i=1; i<=10; i++){
			MyThread mt = new MyThread(i);
			Demo dt = new Demo(i);
			es1.execute(mt);
			es2.execute(dt);
		}
		
		es1.shutdown();
		es2.shutdown();
	}
}
