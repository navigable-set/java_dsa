//newSingleThreadExecutor(): ekch thread sgle task execute krto

import java.util.concurrent.*;

class MyThread implements Runnable{

	int num =0;
	MyThread(int num){
		this.num= num;
	}

	public void run(){
		System.out.println("task = " + num + " " + Thread.currentThread().getName());
	}
}

class ThreadPoolDemo{

	public static void main(String[] shweta){

		ExecutorService es = Executors.newSingleThreadExecutor();			

		//single thread i.e. one thread sgle task perform krnar aahe
		for(int i=1; i<=10; i++){
			MyThread mt = new MyThread(i);
			es.execute(mt);
		}
		
		es.shutdown();
	}
}
