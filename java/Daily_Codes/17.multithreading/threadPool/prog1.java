/*ThreadPool : mdhe already threads he create krun thevlele astat... active threads astatt te.. je 'ready to run' state mdhe astat.. aani jshe task assign hotat tshe run hotat.. aani task sampl ki prt pool mdhe yaeun ready to run state la yetat*/

import java.util.concurrent.*;

class MyThread implements Runnable{

	int num = 0;

	MyThread(int num){
		this.num = num;
	}

	public void run(){
		System.out.println("task = " + num + " -> " + Thread.currentThread().getName());
	}
}

class ThreadPoolDemo{

	public static void main(String[] shweta){

		ExecutorService es = Executors.newFixedThreadPool(5);
		
		//==>'shutdown()' method la eror yenar aahe..karn ya interface kde ekch method aahe 'execute()'
		//Executor es = Executors.newFixedThreadPool(5); 
		
		//==>incompatible types.. executorservice cannot be converted to ThreadPoolExecutor
		//ThreadPoolExecutor es = Executors.newFixedThreadPool(5);
		
		//ThreadPoolExecutor es = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);	//typecast krun chalt

		for(int i=1; i<=10; i++){
			MyThread obj = new MyThread(i);
			es.execute(obj);	//he task jail aata.. aani tyala random thread assign hoil... 10 vela execute() la call janar aahe.. mhanun 10 task generate hotil.. aani tyanna random threads miltil
		}

		es.shutdown();		//jr pool shutdown nahi kela tr te threads tshech astil... task chi vat bght.. mhanun threadpool la shutdown kel pahije
	}
}
