/*Multiple threads creation
 * 3- threads => main/ Thread-0/ Thread-1 */

class Demo extends Thread{

	public void run(){
		System.out.println("Demo : " + Thread.currentThread().getName());
	}
}

class MyThread extends Thread{

	public void run(){
		System.out.println("MyThread : " + Thread.currentThread().getName());

		Demo obj = new Demo();
		obj.start();
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		System.out.println("ThreadDemo : " + Thread.currentThread().getName());
		
		MyThread obj = new MyThread();
		obj.start();
	}
}
