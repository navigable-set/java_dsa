/*Priority of thread :
 * jya thread pasn 'new thread' jnmala yeto na .. tevdha to parent thread chi priority gheun yet asto
 * Min_Priority = 0
 * Normal_Priority = 5
 * Max_Priority = 10*/

class MyThread extends Thread{

	public void run(){
		Thread t = Thread.currentThread();	//Thread class cha object return krte
		System.out.println("run : " + t.getPriority());
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		Thread t = Thread.currentThread();
		System.out.println("main1 : " + t.getPriority());

		MyThread obj1 = new MyThread();
		obj1.start();

		t.setPriority(7);	//main thread chi priority aata 7 zalia.. so main thread pasn yenare threads chi priority pn 7 ch asel
		
		System.out.println("main2 : " + t.getPriority());
		
		MyThread obj2 = new MyThread();
		obj2.start();
	}
}
