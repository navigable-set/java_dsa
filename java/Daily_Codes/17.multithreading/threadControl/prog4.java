/*DeadLock scenario : join() use kel ki deadLock ch scenario yeu shakto kdhi tri jr doghi thread he ekmekanna join() krt astil tr*/

class MyThread extends Thread{

	static Thread nmMain = null;

	public void run(){

		try{
			nmMain.join();
		}catch(InterruptedException obj){

		}

		for(int i=0; i<5; i++){
			System.out.println("In Thread-0");
		}
	}
}

class ThreadDemo{

	public static void main(String[] shweta) throws InterruptedException{

		MyThread.nmMain = Thread.currentThread();	//thread(main thread) class cha object return krte currentThread()

		MyThread obj = new MyThread();
		obj.start();

		obj.join();

		for(int i=0; i<5; i++){
			System.out.println("In main");
		}
	}
}
