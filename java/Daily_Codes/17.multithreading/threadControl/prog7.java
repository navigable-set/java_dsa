/*low priority thread la yield krta yet nahi tr fkt.. same or greater priority thread la ch yield chaly
 * trial and error => not the exact code i want to do*/

class Demo extends Thread{

	public void run(){
		
		System.out.println(Thread.currentThread());
		
		for(int i=0; i<5; i++){

			try{
				System.out.println("In Demo");
				Thread.sleep(2000);
			}catch(InterruptedException obj){

			}
		}
	}
}

class MyThread extends Thread{

	public void run(){

		System.out.println(Thread.currentThread());

		Thread.currentThread().setPriority(2);
		
		for(int i=0; i<5; i++){

			System.out.println("In thread-0");
		}

	}
}

class ThreadYieldDemo{

	static Demo obj2 = new Demo();
	
	public static void main(String[] shweta){

		System.out.println(Thread.currentThread().getName());

		MyThread obj1 = new MyThread();
		obj1.start();
		
		obj2.start();
		
		Thread.currentThread().setPriority(4);
		
	//	obj2.stop();	==>error : depricated
		for(int i=0; i<5; i++){

			System.out.println("In main");
		}


		obj2.yield();

		System.out.println(Thread.currentThread());
	}
}
