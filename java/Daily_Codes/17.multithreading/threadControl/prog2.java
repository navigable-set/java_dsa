/*join() : jya object/thread vr aapn join krto tyach kam aadhi kel jato nantr dusrya ch
 * join() [no-arg] hi method jast use nahi krt=> deadlock cha scenario yeu shakto
 * join(long) => hi method use keli jate.. jyat perticular time period sathi ch thread join hoto
 * join(long,int) => milisec and nanosec
 * throws 'InterruptedException'*/

class MyThread extends Thread{

	public void run(){

		for(int i=0; i<5; i++){

			System.out.println(Thread.currentThread().getName());
			
			try{
				Thread.sleep(1000);
			}catch(InterruptedException obj){

			}
		}
	}
}

class ThreadDemo{

	public static void main(String[] shweta) throws InterruptedException{
		
		MyThread obj = new MyThread();
		obj.start();

		obj.join();
		//obj.join(0);	==> same as 'join()' .. wait till die

		for(int i=0; i<5; i++){
			System.out.println(Thread.currentThread().getName());
			Thread.sleep(1000);
		}
	}
}
