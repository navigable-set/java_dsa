/*Thread control krnya sathi 3-methods use kelya jatat :
 * 1.sleep()
 * 2.join()/join(long) etc.
 * 3.yield()
 *
 * sleep(long) => jya thread chya aread mdhe call krto tyachya vr sleep lagte.. throws InterruptedException
 * sleep(long,int) => milisec and nanosec ... throws InterruptedException
*/


class MyThread extends Thread{

	public void run(){
		for(int i=0; i<10; i++){
			System.out.println(Thread.currentThread());
			try{
				Thread.sleep(2000,900);
			}catch(InterruptedException obj){
	
			}
		}
	}
}

class ThreadDemo{

	public static void main(String[] shweta) throws InterruptedException{
	
		System.out.println(Thread.currentThread());
		
		MyThread obj = new MyThread();
		obj.start();

		Thread.currentThread().setName("Shweta");

		for(int i=0; i<10; i++){
			System.out.println(Thread.currentThread());
			Thread.sleep(2000);
		}
	}
}
