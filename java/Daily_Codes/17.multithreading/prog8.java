/* Runnable use kraych ek advantage aahe ki aapn dusra ekhada class pn extends kru shakto(extends Demo and implements Runnable).. aani mg tya class chya methods pn accessible hotat.. pn je Thread class use kela tr aapn dusra konta hi class extends kru shakt nahi(multiple inheritance is not supported by java) */

class Demo{

	void fun(){
		Thread t = Thread.currentThread();
		System.out.println("Demo : " + t);	//hi method Thread class ch object return krte => Thread[threadName, threadPriority, NameOfThreadGroup] return krte
	}
}

class MyThread extends Demo implements Runnable{

	public void run(){
		System.out.println("In Run");
		System.out.println("MyThread : " + Thread.currentThread().getName());

		fun();
	}
}

class ThreadDemo{

	public static void main(String [] shweta){
	
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);

		t.start();
		
		System.out.println("ThreadDemo : " + Thread.currentThread().getName());
	}
}
