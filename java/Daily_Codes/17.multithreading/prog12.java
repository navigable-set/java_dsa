/*jr parent child lihayla lavl aani bolle ki child mdhe thread create krun parent chya goshti access kraa tevha.. Runnable ch use krava lagel aani khalchya sarkha code asel.. tevha 'extends Thread' krun nahi chalnar(multiple inheritance not supported)!!
## aapn ekach/same class mdhe 'new thread' create krun tyach class chi overriding 'run()' access kru shakto*/

class Demo{

	Demo(){
		System.out.println("In Demo constructor");
	}

	void fun(){
		System.out.println("In fun : " + Thread.currentThread().getName());
	}
}

class ThreadDemo extends Demo implements Runnable{

	public void run(){
		System.out.println("In run : " + Thread.currentThread().getName());
		fun();
	}

	public static void main(String[] shweta){
	
		System.out.println("In main1 : " + Thread.currentThread().getName());

		ThreadDemo obj = new ThreadDemo();
		
		Thread t = new Thread(obj);
		t.start();

		System.out.println("In main2 : " + Thread.currentThread().getName());
	}
}
