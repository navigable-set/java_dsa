/*pririty chi pn ek range aahe... from 1-10
 * jr aapn setPriority(int) method use krnar asu.. so aapn priority hi 1-10 mdhlich dili pahije
 * jr jast or kmi dili tr setPriority(int) ek runtime exception throw krte ==> 'IllegalArgumentException'*/

class MyThread extends Thread{

	public void run(){

		Thread t = Thread.currentThread();
		System.out.println("MyThread : " + t.getName() + " " + t.getPriority());
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		Thread t = Thread.currentThread();

		System.out.println("ThreadDemo1 : " + t.getName() + " " + t.getPriority());

		MyThread obj1 = new MyThread();
		obj1.start();

		t.setPriority(11);	//IllegalArgumentException

		MyThread obj2 = new MyThread();
		obj2.start();
		
		System.out.println("ThreadDemo2 : " + t.getName() + " " + t.getPriority());
	}
}

/*
ThreadDemo1 : main 5
Exception in thread "main" MyThread : Thread-0 5
java.lang.IllegalArgumentException
	at java.lang.Thread.setPriority(Thread.java:1094)
	at ThreadDemo.main(prog10.java:25)
*/
