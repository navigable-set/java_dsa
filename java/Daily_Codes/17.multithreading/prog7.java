/*MULTITHREADING USING RUNNABLE INTERFACE
 * Runnable : mdhe fkt ekch method aahe.. run() navachi
 * Runnable use kraych ek advantage aahe ki aapn dusra ekhada class pn extends kru shakto(extends Demo and implements Runnable).. aani mg tya class chya methods pn accessible hotat.. pn je Thread class use kela tr aapn dusra konta hi class extends kru shakt nahi(multiple inheritance is not supported by java) */

class MyThread implements Runnable{

	public void run(){
		System.out.println("In Run");
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadDemo{

	public static void main(String [] shweta){
	
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);

		t.start();

		System.out.println(Thread.currentThread().getName());
	}
}
