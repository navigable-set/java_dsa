/*new thread fkt 'run()' execute krto.. mg jr dusrya method pn 'new thread' la dyaychya astil tr run() mdhe tya methods la call kraycha*/

class MyThread extends Thread{

	public void run(){

		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());	//thread-0
		gun();
	}

	public void gun(){

		System.out.println("In gun");
		System.out.println(Thread.currentThread().getName());	//thread-0
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		System.out.println("In main");
		
		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());	//main
	}
}
