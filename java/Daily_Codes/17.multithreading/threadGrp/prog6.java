/*ThreadGroup methods : 
 *activeCount() : aapn bnvlelya group mdhe kiti threads aahet (own threads + child group threads) tyacha count return krto
 *activeGroupCount() : aapn kiti thread group bnvlet tyacha count return krto*/

class MyThread extends Thread{

	MyThread(ThreadGroup tr, String str){
		super(tr,str);
	}

	public void run(){

		try{
			Thread.sleep(2000);
			System.out.println(Thread.currentThread());
		}catch(InterruptedException obj){

		}
	}
}

class ThreadgroupDemo{

	public static void main(String[] shweta){

		//parent group-1
		ThreadGroup ptg1 = new ThreadGroup("Core2web");

		MyThread obj1 = new MyThread(ptg1,"Java");
		MyThread obj2 = new MyThread(ptg1,"Cpp");
		
		obj1.start();
		obj2.start();

		//parent group-2
		ThreadGroup ptg2 = new ThreadGroup("Biencaps");

		MyThread obj3 = new MyThread(ptg2,"BoomPanda");
		MyThread obj4 = new MyThread(ptg2,"Encubator");

		obj3.start();
		obj4.start();

		//child group-1 of parent group-1
		ThreadGroup ctg1 = new ThreadGroup(ptg1,"BackEnd-developer");

		MyThread obj5 = new MyThread(ctg1,"Nodejs");
		MyThread obj6 = new MyThread(ctg1,"SpringBoot");

		obj5.start();
		obj6.start();

		//child group-2 of parent group-1
		ThreadGroup ctg2 = new ThreadGroup(ptg1,"FrontEnd-developer");
		
		MyThread obj7 = new MyThread(ctg2,"Reactjs");
		MyThread obj8 = new MyThread(ctg2,"Flutter");
		
		obj7.start();
		obj8.start();

		System.out.println(ptg1.activeCount());		//ptg1 -group mdhle total running thread cha count dete(including child thread)
		System.out.println(ptg2.activeCount());		//ctg -group mdhle total running thread cha count dete(including child thread)	

		System.out.println(ptg1.activeGroupCount());	//activeGroupCount() => hi method child group cha count dete
	}
}
