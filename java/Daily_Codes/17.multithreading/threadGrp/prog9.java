//Most important code to clear basic concepts about threadGroup

class MyThread extends Thread{

	MyThread(ThreadGroup e,String str){
                super(e,str);
        
	}

        public void run(){

                try{
			Thread.sleep(2000);
                	System.out.println(Thread.currentThread() + "" + activeCount());	//in this area.only newly created threads will be there.. so no main thread!!
                }catch(InterruptedException e){

                }
        }

        public static void main(String[] shweta) throws InterruptedException{

		//one parent group
		ThreadGroup tg1 = new ThreadGroup("abc");
                MyThread mt1 = new MyThread(tg1,"t1");
                MyThread mt2 = new MyThread(tg1,"t2");

		//2 child group
		ThreadGroup cg1 = new ThreadGroup(tg1,"xyz");
		ThreadGroup cg2 = new ThreadGroup(tg1,"pqr");

		//2 threads in one of child grp
                MyThread mt3 = new MyThread(cg1,"t3");
                MyThread mt4 = new MyThread(cg1,"t4");
                
		mt1.start();
                mt2.start();
                mt3.start();
                mt4.start();

                System.out.println(Thread.currentThread());

                System.out.println("tg1 child group count : " + tg1.activeGroupCount());    
                System.out.println("tg1 parent" + tg1.getParent());
                System.out.println("cg1 child group count : " + cg1.activeGroupCount());    
                System.out.println("cg1 parent" + cg1.getParent());
                
	//	System.out.println("cg1 parent" + mt1.getParent());	==>error : canot find symbol.. karn 'mt1' is a thread/aaplya 'MtThread class' mdhe getParent() navachi ethod nahia

                System.out.println("total threads alive : " + activeCount());	//'5' karn sgle thread he alive aahet(main + all other).. dead nahia.. te jri kontahi grp mdhe asle tri tya sglyancha parent ha 'main group' ch asnar aahe may be

                System.out.println("total grp :" + tg1.getParent().activeGroupCount());   
                System.out.println("get parent1 :" + tg1.getParent()) ;  //main
                System.out.println("get parent1 :" + tg1.getParent().getParent()) ;  //system
                System.out.println("get parent2 :" + tg1.getParent().getParent().getParent()) ;  //null

               // System.out.println("get parent2 :" + tg1.getParent().getParent().getParent().getParent()) ;  //nullPointerException
        }
}
