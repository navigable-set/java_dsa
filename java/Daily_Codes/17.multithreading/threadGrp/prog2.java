/*multiple threadGroups are been created...
 * groups la same same name dili tri chaltat
 * group aani thread ch name pn same asu shakt.. chalt te pn..(eg. 'main' thread and 'main' group)
 */

class MyThread extends Thread{

	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{

	public static void main(String[] shweta){
	/*	
		//same thread names/ same thread-group name/same group and thread name..etc chalt

		ThreadGroup threadGP1 = new ThreadGroup("backend");
		ThreadGroup threadGP2 = new ThreadGroup("frontEnd");

		MyThread obj1 = new MyThread(threadGP1 ,"Framework");
		MyThread obj2 = new MyThread(threadGP1 ,"Framework");
		
		MyThread obj3 = new MyThread(threadGP2 ,"HTML Css JS");
		MyThread obj4 = new MyThread(threadGP2 ,"Framework");/*
	*/
		ThreadGroup threadGP1 = new ThreadGroup("Mobile");
		ThreadGroup threadGP2 = new ThreadGroup("Laptop");

		MyThread obj1 = new MyThread(threadGP1 ,"OnePlus_Nord");
		MyThread obj2 = new MyThread(threadGP1 ,"iPhone14");
		
		MyThread obj3 = new MyThread(threadGP2 ,"Asus");
		MyThread obj4 = new MyThread(threadGP2 ,"HP");
		
		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();
	}
}
