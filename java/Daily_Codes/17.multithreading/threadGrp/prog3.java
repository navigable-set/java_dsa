/*Child Thread-Group : child ThreadGroup sathi ThreadGroup cha 2-parameters vala constructor use krava lagto
 * ThreadGroup(parentThreadgroup-obj, childThreadGroup-Name)*/

class MyThread extends Thread{

	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}

	public void run(){

		System.out.println(Thread.currentThread());
	}
}

class ThreadgroupDemo{

	public static void main(String[] shweta){

		ThreadGroup parentTG = new ThreadGroup("University");
		
		MyThread obj1 = new MyThread(parentTG, "Syllabus");
		MyThread obj2 = new MyThread(parentTG, "ExamTimetable");
		
		ThreadGroup childTG = new ThreadGroup(parentTG,"colleges");
		
		MyThread obj3 = new MyThread(childTG, "Courses");
		MyThread obj4 = new MyThread(childTG, "Teaching");

		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();

		System.out.println("In main " + parentTG);	//getName() method of ThreadGroup sarkhich o/p dete
	}
}
