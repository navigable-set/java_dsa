/*ThreadGroup using Runnable
 * Thread class has constructor of type 'Thread(ThreadGroup,Runnable,String)'*/

class MyThread implements Runnable{

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadgroupDemo{

	public static void main(String[] shweta){

		ThreadGroup tg1 = new ThreadGroup("India");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();
		
		Thread t1 = new Thread(tg1,obj1,"Agriculture");
		Thread t2 = new Thread(tg1,obj1,"Finance");

		t1.start();
		t2.start();
		
		ThreadGroup tg2 = new ThreadGroup(tg1,"Defence");	
		
		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread t3 = new Thread(tg2,obj3,"Army");
		Thread t4 = new Thread(tg2,obj3,"Navy");

		t3.start();
		t4.start();
	}
}
