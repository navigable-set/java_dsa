//Threadgroup kde pn interrupted() method aahe ji interruptedException throw krt nahi.. pn aapn ethe Thead class chi interrupt use kelia

class MyThread implements Runnable{

	public void run(){
		try{
			for(int i=1; i<3; i++){
				System.out.println(Thread.currentThread());
				Thread.sleep(5000);
			}
		}catch(InterruptedException obj){
			System.out.println(obj.toString());
		}
	}
}

class ThreadgroupDemo{

	public static void main(String[] shweta){

		ThreadGroup ptg1 = new ThreadGroup("ParentGroup1");
		
		MyThread mt1 = new MyThread();
		MyThread mt2 = new MyThread();
		
		Thread t1 = new Thread(ptg1,mt1,"thread1");
		Thread t2 = new Thread(ptg1,mt2,"thread2");
		
		ThreadGroup ptg2 = new ThreadGroup("ParentGroup2");
		
		MyThread mt3 = new MyThread();
		MyThread mt4 = new MyThread();
		
		Thread t3 = new Thread(ptg2,mt3,"thread3");
		Thread t4 = new Thread(ptg2,mt4,"thread4");
		
		t1.start();
		t1.interrupt();	//thread class chi interrupt method aahe.. thread la sleep mdhe astanna interrupt gelay mhanun interrupted exception
		t2.start();
		t3.start();
		t4.start();

		System.out.println(ptg1.activeGroupCount());
		System.out.println(Thread.activeCount());
	}
}
