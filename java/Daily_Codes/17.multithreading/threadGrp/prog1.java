/*ThreadGroup : multiple threads same ch kam krt astil tr tya threads che groups bnvle jatat.. so that ektra ch sglyanna task jatil => ThreadGroup mhantat.. threadGroup la pn priority aste(max=10) 
*/

class MyThread extends Thread{

	MyThread(ThreadGroup tg, String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadgroupDemo{

	public static void main(String[] shweta){

		ThreadGroup threadGP = new ThreadGroup("highSchool");

		MyThread mt1 = new MyThread(threadGP, "Science");
		MyThread mt2 = new MyThread(threadGP, "Commerse");
		MyThread mt3 = new MyThread(threadGP, "Arts");

		mt1.start();
		mt2.start();
		mt3.start();
	}
}
