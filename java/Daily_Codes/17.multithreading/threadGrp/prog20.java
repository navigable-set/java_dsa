import java.util.concurrent.*;

class ThreadGroup implements Runnable{

	String workNum = null;

	ThreadGroup(String workNum){
		this.workNum = workNum;
	}

	public void run(){

		System.out.println(Thread.currentThread().getName() + workNum);
	}
}


class MyThread{

	public static void main(String[] shweta){

		ExecutorService exec = Executors.newFixedThreadPool(5);

		ThreadGroup obj1 = null;
		for(int i=0; i<5; i++){
			obj1 = new ThreadGroup(" " +i);
			exec.execute(obj1);
		}
		
		exec.shutdown();
		try{
			exec.execute(obj1);
		}catch(RejectedExecutionException obj){
			System.out.println("Server is shutDown");
		}
	}
}
