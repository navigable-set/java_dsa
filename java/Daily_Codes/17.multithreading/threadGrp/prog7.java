class MyThread extends Thread{

	MyThread(ThreadGroup obj,String str){
		super(obj,str);
	}

	MyThread(){

	}

	public void run(){

		System.out.println(Thread.currentThread() + "" + activeCount());
		try{
			Thread.sleep(2000);
		}catch(InterruptedException e){

		}
	}

	public static void main(String[] shweta) throws InterruptedException{

		//single thread
		MyThread mt = new MyThread();
		mt.start();
		
		//creating parent group
		ThreadGroup obj = new ThreadGroup("main2");

		//threads in parent group
		MyThread obj1 = new MyThread(obj,"shweta");
		MyThread obj2 = new MyThread(obj,"tejashree");
		MyThread obj3 = new MyThread(obj,"darshan");

		obj1.start();
		obj2.start();
		obj3.start();
		
		//child group in parent
		ThreadGroup ctg = new ThreadGroup(obj,"main3");
		
		//threads in child group
		MyThread obj4 = new MyThread(ctg,"abc");
		MyThread obj5 = new MyThread(ctg,"pqr");
		MyThread obj6 = new MyThread(ctg,"xyz");

		obj4.start();
		obj5.start();
		obj6.start();
		
		System.out.println(Thread.currentThread());
		
		//sleep kel tr child threads dead hotil.. so activeCount = 1 yeil..fkt 'main' ch thread asel
		//Thread.sleep(2000);				
		
		//Threads class chi method aahe ji total threads dakhvt.. jevdhe existing aahet tevdhe... threadGroup chi activeCount() hi fkt tya grp mdglech threads dakhvte
		System.out.println(activeCount());		
			
		System.out.println(obj.activeGroupCount());	//count of child group count
	}
}
