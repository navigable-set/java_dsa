//first see prog8.java to clear concepts
//trial and error
//###aapn je kahi threadGroup bnvto tyancha parent ha main group ch aahe(ethe mi extends ThreadGroup kely mhanun jra vegl disty pn normally pn main group hach parent asto sglya grp cha)

class MyThread extends ThreadGroup implements Runnable{

	MyThread(ThreadGroup e,String str){
		super(e,str);
	}

	MyThread(String str){
		super(str);
	}	

	public void run(){

	//	System.out.println(Thread.currentThread() + "" + activeCount());
		try{
			Thread.sleep(2000);
			System.out.println(Thread.currentThread() + "" + activeCount());
		}catch(InterruptedException e){

		}
	}

	public static void main(String[] shweta) throws InterruptedException{

		//single thread.. ha pn threadGroup create zalya sarkha aahe.. karn extends ThreadGroup aahe
		MyThread mt = new MyThread("A");
	//	MyThread p = new MyThread("abc");
		//Thread t2 = new Thread(obj3,b,"pqr");

		Thread t = new Thread(mt);
		t.start();
		
		//creating parent group
		ThreadGroup obj = new ThreadGroup("main2");
		ThreadGroup obj2 = new ThreadGroup(obj,"main3");

		//yacha parent main group nahia tr 'mt' ch aahe.. ha child grp bnla
		ThreadGroup obj3 = new ThreadGroup(mt,"main4");
		
		MyThread a = new MyThread("");
		MyThread b = new MyThread("");
		
		Thread t1 = new Thread(obj,a,"abc");
		Thread t2 = new Thread(obj3,b,"pqr");
		
		t1.start();
		t2.start();

//		System.out.println(Thread.currentThread());
		
		System.out.println(t1.activeCount());		
			
		System.out.println(obj.activeGroupCount());	//count of child group count
		System.out.println("obj" + obj.getParent());

		System.out.println("obj2" + obj2.getParent());
		System.out.println("obj3" + obj3.getParent());
		System.out.println("mt " +mt.getParent());
		System.out.println("a " +a.getParent());
		 
		/**************************************/

		System.out.println("**" + Thread.currentThread().activeCount());
	
		/*'b' cha parent group ha 'main' aahe aani main vr activeGropCount(child grp sangto) la call dilay) i.e. 'main group' chya child grp cha count kadhlay aapn */       
		System.out.println("total grp :" + b.getParent().activeGroupCount());	//6
	}
}

/*
4
1
objjava.lang.ThreadGroup[name=main,maxpri=10]
obj2java.lang.ThreadGroup[name=main2,maxpri=10]
obj3MyThread[name=A,maxpri=10]
mt java.lang.ThreadGroup[name=main,maxpri=10]
a java.lang.ThreadGroup[name=main,maxpri=10]
**4
total grp :6
Thread[abc,5,main2]0
Thread[pqr,5,main4]0
Thread[Thread-0,5,main]1
*/
