/*jr thread eka state(new/ready-to-run/running/dead) mdhe aahe.. aapn tyachi state change kru shakt nahii.. aapn tyala punha start() kru shakt nahii .. exception throw krel ==> 'IllegalThreadStateException'!!
 * note : ekda thread ne tyachi perticular state sodli tr to punha tya state mdhe jat nahi*/

class MyThread implements Runnable{

	public void run(){
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo{

	public static void main(String[] shweta){

		MyThread obj = new MyThread();
		
		Thread t = new Thread(obj);
		t.start();

		System.out.println(Thread.currentThread());

		t.start();	//'IllegalThreadStateException' ... Thread-0 ha alredy aalay.. to tyach tyach kam krtoy.. aapn tya same thread la punha start() nahi kru shakt.. nvyane kam kr aas nahi mhanu shakt ... aapn dusra nvin thread bnvun tyala start kru shakto.. pn same thread chi state nahi bdlu shkt!
	}
}

/*
Output :
Thread[main,5,main]
Thread[Thread-0,5,main]
Exception in thread "main" java.lang.IllegalThreadStateException
	at java.lang.Thread.start(Thread.java:710)
	at ThreadDemo.main(prog11.java:21)
*/
