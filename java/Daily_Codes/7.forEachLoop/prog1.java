/*FOR EACH LOOP : ashya data varti chalto jachya kde collection of data aahe
 		  fkt classes vr chalt...normal varible vr nahi
		  purn data access kraycha asel tevha ch use kraycha..nahi tr nahi...karn to loop mdhec ch kuthe thambt nahi joparyant purn array ch kam hot nahi*/
// Q. identityHshCode ne (mhanje to separate 5 box bnvto ka prt array che..jri array ha ekda ch initialize kelay tri) int aani int la same dakhvty value in for each loop..float la vegl yeil ch..pn mg nvin array bnvto ka??
class Demo{

	public static void main(String [] args){

		int arr[] = {10,10,30,40,50};

		for(int x : arr){
			System.out.println(x);
			System.out.println("x = " + System.identityHashCode(x));				
		}
		
		System.out.println();
	
		for(int x : arr){
			if(x == 30)
				break; 
			System.out.println(x);
			System.out.println("x = " + System.identityHashCode(x));			

		}
		System.out.println();
		
		//float chalel karn data loss nahi hot aahe..
		for(float x : arr){
			System.out.println(arr[0]);	//ha oroginally 'int' aahe
			System.out.println("arr[0] = " + System.identityHashCode(arr[0]));	
			System.out.println(x);		//ha float mdhe read krnar aahe data
			System.out.println("x = " + System.identityHashCode(x));	//ha float mdhe read krnar aahe data

			//System.out.println(arr[x]);	//array index out of bound exception yeil jr int x asel tr..arr[x=10] krel so arrayIndexOutOfBound yeil
			//System.out.println(x[0]);	//array required but int found...that is 'x' ha element of array asel mhanun exception pn yety..#
		}
	}
}
