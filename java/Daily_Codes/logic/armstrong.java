/* Armstrong number : sum of cube's of each digit is equal to original number
 		      e.g. 153 == [(1*1*1)+(5*5*5)+(3*3*3)] = 153 		      
		      	   1634 == [(1*1*1*1)+(6*6*6*6)+(3*3*3*3)+(4*4*4*4)] = 1634
*/

class Armstrong{

	public static void main(String [] shweta){

		int x = 153;
		int temp = x;
		int count = 0;

		while(temp != 0){
			count++;
			temp = temp/10;
		}

		temp = x;
		int sum = 0;

		while(temp != 0){
			int mul =1;
			int rem = temp%10;
			for(int i = 1; i<= count; i++){
				mul = mul *rem;
			}
			sum = sum + mul;
			temp = temp /10;
		}

		if(sum == x)
			System.out.println(x + " is Armstrong number");
		else
			System.out.println(x + " is not a Armstrong number");
	}
}
