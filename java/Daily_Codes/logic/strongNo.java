/* Strong number : a number whose sum of factorial of each digit is equal to original number
 		   e.g. 145 = 1! + 4! + 5! = 145 */

class StrongNo{

	public static void main(String [] shweta){

		int num = 145;
		int temp = num;
		int sum = 0;

		while(temp != 0){
			int fact = 1;
			int rem = temp%10;
			for(int i=1; i<= rem; i++){

				fact = fact*i;
			}
			sum = sum + fact;
			temp = temp/10;
		}

		if(sum == num)
			System.out.println(num + " is Strong number");
		else
			System.out.println(num + " is not a Strong number");
	}
}
