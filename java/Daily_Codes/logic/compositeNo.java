/* Composite number : a number which has factors more than 2 or the positive integer that has at least one divisor other than 1 and itself
 		      e.g. : 6 = 1,2,3,6 or 15 = 1,3,5,15 */

class Composite{

	public static void main(String [] shweta){

		int num = 35;
		int count = 0;

		for(int i=1; i<= num; i++){
			if(num%i == 0)
				count++;
		}

		if(count > 2)
			System.out.println(num + " is a composite number");
		else
			System.out.println(num + " is not a composite number");
	}
}
