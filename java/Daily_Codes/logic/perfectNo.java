/* PERFECT NUMBER : the sum of factor's of given number except itself is equal to the original number
                     e.g. 6 == (1+2+3) 6 ....6 is perfect number*/

class PerfectNo{
        
        public static void main(String[] shweta){
        
                int x = 6;
                int sum = 0;

                for(int i =1; i< x; i++){
                        if(x%i == 0)
                                sum = sum + i;
                }

                if(sum == x)
			System.out.println(x + " is perfect number");
		else
			System.out.println(x + " is not a perfect number");
        }
}                                                                                                                                                            
