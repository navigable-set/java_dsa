/* Automorphic number : a number whose square has the same digits in the end as the number itself 
 			e.g. 0 = 0 || 1 = 1 || 5 = 25 || 6 = 36 || 25 = 625 || 76 = 5776 .... */

class AutomorphicNo{

	public static void main(String [] shweta){

		int num = 76;
		int squr = num*num;
		int temp = num;
		int count = 0;

		while(temp != 0){
			count++;
			temp = temp/10;
		}
		
		int rev = 0; 

		for(int i =1; i<= count ; i++){
			int rem = squr%10;
			rev = rem + (rev * 10);
			squr = squr/10;
		}

		int digit =0;
		while(rev != 0){
			int rem = rev %10;
			digit = rem + (digit * 10);
			rev = rev /10;
		}

		if(digit == num){
			System.out.println(num + " is an Automorphic number");
		}else{
			System.out.println(num + " is not an Automorphic number");
		}
	}
}
