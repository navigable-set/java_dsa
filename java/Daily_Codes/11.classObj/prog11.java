/*JAVA supports Global static variables only (class variables)
 * static variables he commonly disayla pahijet mhanun te kontyahi block mdhe lihita yet nahi
 * static variables can only be written inside class
 * we cannot write static variables inside any scope whether it is static or non-static
 * static variables la priority sglyat jast aste..mhanje sglyat aadhi static variables initialize hotat...mhnaun main chya pn aadhi static block yeto..aani static block mdhe pn aadhi static variables initializa hotat aani nantr static block mdhe aapn je lihily te..je aaplya static block mdhe aadhi sop() asel aani nnatr ekhada static block asel tr te chukich hoil karn static variables ch initialization he sglyat aadhi vhayla hvv aani nantr te sop() mhanje static block ch execution chlat..mhanun ch static block mdhe static variables nahi chalt karn static variables he sglyat aadhi yetat ..static block chya pn aadhi(check bytecode for better understanding) aani baki sglya static/non-static methods ya static block ch kam zalya vrch yenar aahet...mhanun tyanchyat static variables ch initialization nahi chalt..mhanun error yeto
 * mhanje static variables he fkt class chya aat chltat...static/non-static mathod/block madhe nahi chalt*/

class Demo{

	static int x = 20;//static/class variable...Global static
	int i = 10;	//instance variable..Global non-static

	static{

	//	static int w = 20;  -->error : illegal start of expression
		int y= 10;	//local(non-static always and only) variables...in stack frame only

	}

	static void fun(){

	//	static int h = 20;  -->error : illegal start of expression
		int z = 30;	//local(non-static always and only) variables...in statck frame only
	}

	public static void main(String[] shweta){

	//	static int s = 20; --> error : illegal start of expression
		int p = 30;	//local(non-static always and only)
		System.out.println(x);
	}
}
