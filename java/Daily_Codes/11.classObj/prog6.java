//static can be directly access with class name..no need to create object as satic variables get initiated in static block

class StaticDemo{

	StaticDemo(){

		System.out.println("its constructor");
	}

	static int x = 10;
	static int y = 20;

	static void disp(){

		System.out.println(x);
		System.out.println(y);
	}
}

class Client{

	public static void main(String [] shweta){

		System.out.println(StaticDemo.x);
		System.out.println(StaticDemo.y);

		StaticDemo.disp();
	}
}
