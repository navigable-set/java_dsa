/* we can also create objects inside static block */

class Demo{
	
	static int x = 10;
	int y = 20;

	static{
		Demo obj1 = new Demo();
		System.out.println("static block of Demo");
		System.out.println("x = " + x);
		System.out.println("y = " + obj1.y);
		System.out.println("---------------------");
	}

	public static void main(String [] shweta){

		System.out.println("In main");
		System.out.println("x = " + x);
	}
}
