/* static variable: once a static variable is changes , it will reflect all over the code (class variable)
 * instance variables : change in instance variable is limited to that respective object only..it wont reflect in all over the code */

class Employee{

	float salary = 11.00f;
	String name = "shweta";

	static int projCount = 4;

	void empInfo(){

		System.out.println("salary = " + salary);
		System.out.println("name = " + name);
		System.out.println("project count = " + projCount);
	}
}

class Company{

	public static void main(String [] shweta){

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.empInfo();
		emp2.empInfo();

		System.out.println("-----------------------------");

		emp2.salary = 17.00f;
		emp2.name = "tejashree";
		emp2.projCount = 5;
		
		emp1.empInfo();
		emp2.empInfo();
	}
}
