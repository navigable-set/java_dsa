/*Constructor : 2 types according to java(userdefined) : 1.no-argument constructor 2.parameterised constructor ... and one given by compiler if no such userdefined constructor is there called 'default constructor'
 * but, actually only one type of constructor exist that is parameterised ,  for no-argument constructor one hidden parameter goes as a parameter called 'this' ...so there's only parameterised constructor only */

class Demo{

	int x = 10;
	
	Demo(){
		System.out.println("In constructor");
		System.out.println(x);
		System.out.println(this.x);
		System.out.println(this);
	}

	void fun(){
		System.out.println("In fun");
		System.out.println(x);
		System.out.println(this.x);
		System.out.println(this);
	}
	public static void main(String[] shweta){

		Demo obj = new Demo();
		obj.fun();
		System.out.println(obj);
	}
}
