/* ############ CHECK BYTECODE ###########
 * static variables : static block (static comes first even before the main method)
 * instance variables : inside the constructor
 * local variables : inside statck frame...(variables goes on heap but their reference is limited only to stack frame..can be seen from outside but are not accessible) 
 *
 * Q   : what is constructor and why it is used..??
 * ans : constructor is a special type of method..which has no return type and it is used to initialize the instance variables.
 * class name and the name of a constructor is always same..if we dont write any constructor then default constructor will be there..(#check bytecode..where it only gives idea that such things will be there..if we dont create object then at run-time no constructor wull be there)
 *First line in any constructor is 'super()' which means "call the constructor of its parent class..if no any parent then Object class is the parent class of all classes"
 * Q.  : why instance variables are not accessible from the static block..??
 * ans : instance variables get initialized inside the constructor and contructor get implicitely(automatically) calles when an object of a class is creates and the respective constructor get space inside tha Object of a class...as object contains many of the things like pointer to special structure/constructor/instance variables/non-static methods..etc
 * if we don't have created object of a class then constructor won't get space and because of that instance variables will not be initializes..and for that reason..instance variables has no space so we cannot access them as they are not present...that's why compiler gives error like "non-static variables cannot be refernced from a static context" ..
 * so if we want to access a instance variables then we must create the object of our class so that constructor get space and it will then initialise the instance variables..!!*/


class Demo{

	int x = 10;
	static int y = 20;

	public static void main(String [] shweta){
		Demo obj = new Demo();
		System.out.println(obj.x);
		System.out.println(y);
	}
}
