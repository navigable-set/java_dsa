class PuneToDhule{

	float trainTicket = 300.f;
	float travelsTicket = 1000.f;

	PuneToDhule(){
		System.out.println("Route : Pune to Dhule");
	}

	void Traveling(){

		String mode1 = "Train";
		String mode2 = "Travels";

		System.out.println("Available modes of traveling :");
		System.out.println(" 1." + mode1 + "	2." + mode2);
	}
}

class PuneToMumbai{

	float trainTicket = 200.f;
	float travelsTicket = 650.f;
	float flightTicket = 4000.f;

	PuneToMumbai(){
		System.out.println("Route : Pune to Mumbai");
	}

	void Traveling(){

		String mode1 = "Train";
		String mode2 = "Travels";
		String mode3 = "Flight";

		System.out.println("Available modes of traveling :");
		System.out.println(" 1." + mode1 + "	2." + mode2 + 	"	3." + mode3);
	}
}

class TravelRoute{

	public static void main(String [] shweta){

		System.out.println("Let's go..");

		PuneToDhule obj1 = new PuneToDhule();
		obj1.Traveling();
		System.out.println("Train Ticket = " + obj1.trainTicket);
		System.out.println("Travels Ticket = " + obj1.travelsTicket + "\n");

		PuneToMumbai obj2 = new PuneToMumbai();
		obj2.Traveling();
		System.out.println("Train Ticket = " + obj2.trainTicket);
		System.out.println("Travels Ticket = " + obj2.travelsTicket);
		System.out.println("Flight Ticket = " + obj2.flightTicket);
	}
}
