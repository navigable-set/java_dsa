/*STATIC BLOCK : static block comes even before the main() ...because static variables he static block mdhe initialize hotat aani initialization he tevha ch hot jevha tya goshti ekda tri STACK vr gelyat..tya nantr initialization hot...method area mdhe fkt set-up asto goshtincha...
 * mg static or kontya hi goshti initialize tevha ch hottat jevha tya stack vr yetat..otherwise nahi hot..mhanje static variables sathi static block ha stack vr yeto ..aani tya nantr static variables initialise hotat aani tyanchya sathi ch te main peksha pn aadhi yeto static block
 * tsch instance variables sathi constructor chi stack frame jate tevha instance variables he initialize hotat
 * number of static block astil tr te compile time la merge kele jatat ani ekch static block bnto
 * upto 1.6 , many programers were doing code without main with the help of STATIC BLOCK but later they changed and we cannot write any code without main  */

class Demo{

	static int x = 10;

	static{
		System.out.println("static block 1");
	}

	public static void main(String [] shweta){
		System.out.println("In main");
		System.out.println("x = " + x);
	}

	static{
		System.out.println("static block 2");
	}
}


