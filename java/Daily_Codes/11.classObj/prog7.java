/* static block : we can only access STATIC variables/methods direcly from STATIC BLOCK and cannot directly access non-static variables and methods untill we create an object of class so that NON-STATIC VARIABLES get initiated inside the CONSTRUCTOR in OBJECT*/

class Demo{

	int x = 10;

	static int y = 20;

	void fun1(){

		System.out.println(x);
		System.out.println(y);
	}

	static void fun2(){
		//cannot access non-static variables directly within static context
		System.out.println(y);
	}
}

class Client{

	public static void main(String [] shweta){

		Demo obj = new Demo();

		obj.fun1();
		obj.fun2();

		System.out.println(obj.x);
		System.out.println(obj.y);
	}
}
