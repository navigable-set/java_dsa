//static block gets executed for once only
//only one static block will be created for one whole class as it gets merged 
//as here are 2-classes and both have static block then 2-separate static block will be there, one of each class in their respective bytecode
//it gets executed according to its sequence in same class and if in another class then depends when its object gets created first

class SubDemo{

	static{
		System.out.println("static block of SubDemo class");
	}
}

class Demo{

	static{
		System.out.println("static block 1 of Demo");
		SubDemo obj = new SubDemo();	//object is been created and hence SubDemo's static block will be printed
	}

	public static void main(String [] shweta){

		//object is been created still SubDemo's static block will not be printed as its already been printed from the static block of Demo
		SubDemo obj = new SubDemo();	
		System.out.println("In main");
	}

	static{
		System.out.println("static block 2 of Demo");
	}
}
