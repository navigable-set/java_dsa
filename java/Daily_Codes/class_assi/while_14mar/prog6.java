/* even's sum and odd's multiplication between 1 to 10 */

class SumMult{

	public static void main(String [] shweta){

		int x = 1;
		int sum = 0;
		int mult = 1;

		while(x <= 10){
		
			if(x % 2 == 0)
				sum = sum + x;
			else
				mult = mult * x;
			x++;
		}

		System.out.println("sum is : "+ sum);
		System.out.println("multiplication is : "+ mult);
	}
}
