/* check palindrome number*/

class Palindrome{

	public static void main(String [] shweta){

		int num = 1221;
		int temp = num;
		int rev = 0;

		while(num != 0){

			int rem = num % 10;
			rev = (rev * 10 ) + rem;
			num = num/10;
		}

		if(rev == temp)
			System.out.println(temp + " is a palindrome number");
		else 
			System.out.println(temp + " is not a palindrome number");
	}
}
