/* print square of even numbers
   e.g. x = 942111423 */

class EvenSqur{

	public static void main(String [] args){

		int x = 942111423;

		while(x != 0){
			int rem = x % 10;
			if(rem %2 == 0)
				System.out.println(rem * rem);
			x = x/10;
		}
	}
}


