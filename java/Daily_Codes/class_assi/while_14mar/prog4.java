/* count odd number of digits in given number
   e.g. x = 942111423 */

class OddCount{

	public static void main(String [] args){

		int x = 942111423;
		int count = 0;

		while(x != 0){
			int rem = x % 10;
			if(rem %2 == 1)
				count++;
			x = x/10;
		}

		System.out.println("count of odd digits is : " + count);
	}
}


