/* InputStreamReader ==>io package
 * 			connection with keyboard.. i.e. System.in
 *			reads one character at a time only
 *			has methods like read()/close()...etc.
 * 			all its methods throws IOException
 *
 * BufferedReader ==>	io package
 * 			used to store data only which is given by isr
 * 			readLine() method is used to take data from user whose return type is string
 * 			for int/float...we need to parse it using wrapper classes
 * 			all its methods throws IOException*/
				
import java.io.*;
class IsrDemo{

	public static void main(String [] shweta) throws IOException{

		InputStreamReader isr = new InputStreamReader(System.in);

		System.out.println("enter any character :");

		//return-type of read() is --> int
		//possible loosy conversion if we dont convert them into respective datatype
		//char ch = isr.read(); --> error:possible loosy conversion
	
		char ch = (char)isr.read();

		System.out.println("character is : " + ch);
	}
}
