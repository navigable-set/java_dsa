/*StringTokenizer ==> is a class used to break the string into the tokens
 * it is in util package
 * during creating its object we need to pass argument which contains (which string to break , "according what we break")
 */

import java.io.*;
import java.util.*;

class StringDemo{

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Society name , wing, flat number");
		String info = br.readLine();

		System.out.println(info);

		StringTokenizer str = new StringTokenizer(info, " ");
		String token1 = str.nextToken();
		String token2 = str.nextToken();
		String token3 = str.nextToken();

		//respective datatype mdhe convert kely
		System.out.println("Name : " + token1);
		System.out.println("Wing : " + token2.charAt(0));
		System.out.println("flat number : " + Integer.parseInt(token3));
	}
}
