/* user input ===>> SCANNER AND BUFFEREDREADER 
 * Scanner ==> util package....scanners methods dont throw ioException
 * BufferedReader ==> io package...bufferedReader can store only but cannot connect to the keyboard...InputStreamReader can connect to keyboard and can read one character at a time only
 * Scanner ==>sc.close(); ==>IllegalStateException..scanner closed
 */


import java.util.*;
class Demo{

	public static void main(String [] shweta){

		Scanner sc = new Scanner(System.in);


		System.out.println("Enter your Dream company :");
		String cmpName = sc.next();

		//sc.close(); ==>IllegalStateException..scanner closed

		System.out.println("Expected package :");
		float pckg = sc.nextFloat();

		System.out.println("Dream company is : " + cmpName);
		System.out.println("expected package is : " + pckg);
	}
}
