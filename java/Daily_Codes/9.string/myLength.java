import java.io.*;
class MyStrLen{

	static int myLength(String str){

		char carr[] = str.toCharArray();

		int length = 0;
		for(int i=0; i<carr.length; i++){
			length++;
		}
		return length;	//or can directly return 'carr.length'
	}

	public static void main(String [] shweta)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter any string :");
		String str = br.readLine();

		int len = myLength(str);

		System.out.println("length = " + len);
	}
}
