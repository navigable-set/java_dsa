// '+' he internally 'append()' ya method la call krt ji 'StringBuilder' ya class mdhe aahe..aani ti pn string join kraych kam krte..aani ti 'new String' return krte mhanje nvin object bnvte for storing the string created by '+'

class Demo{

	public static void main(String [] shweta){

		String str1 = "shweta";
		String str2 = "bagul";

		String str3 = str1+str2;	//append() la call jaun 'new' return hoil so heap vr bnel(also scp vr pn copy hoil..without refernce) ..ya + ne str1 aani str2 ve kahi frk pdnar nhi..(string is mutable)

		String str4 = new String("shwetabagul");	//'new' mule heap vr bnel

		String str5 = new String(str3);	//'new' mule heap vr jail
		String str6 = "shwetabagul";	//scp vrch reference asel

		System.out.println(str1 + " " + System.identityHashCode(str1));	//scp
		System.out.println(str2 + " " + System.identityHashCode(str2));	//scp
	
		System.out.println(str3 + " " + System.identityHashCode(str3));	//'append()' la call jaun 'new' return hoto aani new mule heap vr jaga milel(also on scp) aani heap vrch reference milto
		System.out.println(str4 + " " + System.identityHashCode(str4));	//'new' mule heap vr navin object jail
		System.out.println(str5 + " " + System.identityHashCode(str5));	//'new' mule heap vr navin object  jail
		System.out.println(str6 + " " + System.identityHashCode(str6));	//scp vr cha refernce asel
	}
}
