class EqualsDemo{

	public static void main(String [] shweta){

		String str1 = "shweta";
		String str2 = "shweta";
		String str3 = new String("shweta");
		String str4 = "shwetabagul";

		//content check krt...aani true/false return krt
		System.out.println(str1.equals(str2));		//true
		System.out.println(str1.equals(str3));		//true
		System.out.println(str1.equals(str4));		//false
	}
}
