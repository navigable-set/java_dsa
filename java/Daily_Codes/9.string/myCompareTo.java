import java.util.*;
class CompareToDemo{

	static int myCompareTo(String str1, String str2){

		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		int len;

		if(arr1.length > arr2.length)
			len = arr2.length;
		else
			len = arr1.length;

		for(int i=0; i<len; i++){
			if(arr1[i] != arr2[i])
				return arr1[i] - arr2[i];
		}
		return arr1.length - arr2.length;
	}

	public static void main(String [] shweta){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter 2 strings..");

		String str1 = sc.next();
		String str2 = sc.next();

		int result = myCompareTo(str1,str2);

		System.out.println(result);
	}
}
