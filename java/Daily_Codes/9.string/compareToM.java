class CompareToDemo{

	public static void main(String [] shweta){

		String str1 = "shweta";
		String str2 = "shweta";
		String str3 = "shweti";
		String str4 = "shwetabagul";

		System.out.println(str1.compareTo(str2));	//0	--> same string la '0' return krto
		System.out.println(str1.compareTo(str3));	//-8	--> jr string diff. aahe tr tya different letters chya ascii cha difference return krto(i.e. 65(a) - 73(i))
		System.out.println(str1.compareTo(str4));	//-5	--> jr ek string purn same aahe..pn length difference aahe tr to tyancha length difference return krel(i.e. 6-11 = -5)
	}
}
