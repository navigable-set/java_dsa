//java string is immutable...you can not make chnages in current string...(if you want modification then simply and give the reference of modifies object)

class Demo{

	public static void main(String [] shweta){

		String str1 = "Shahsi";
		String str2 = new String("Shahsi");

		// '==' is compares the identityHashCode of strings(or address check krto)
		if(str1 == str2)
			System.out.println("equal");
		else
			System.out.println("not equal");

		// 'equals()' compares the actual content of strings(and not its addredd or identityHashCode)
		if(str1.equals(str2))
			System.out.println("equal");
		else
			System.out.println("not equal");
	}
}

