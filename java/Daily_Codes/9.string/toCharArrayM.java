//toCharArray() : is a method that converts the string into character array..you cannot directly assign a string to a char[]..you need to use 'toCharArray()' method !!

import java.io.*;
class ToCharArrayDemo{

	static void charArray(String str){

		char carr[] = str.toCharArray();

		for(int i=0; i<carr.length; i++){
			System.out.println(carr[i]);
		}
	}

	public static void main(String [] shweta)throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter any string :");
		String str = br.readLine();

		System.out.println("string in form of character array : ");
		charArray(str);

		System.out.println("String = " + str);
	}
}
