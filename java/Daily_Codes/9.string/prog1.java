// "" --> string literal ne bnvlelei string hi 'scp' vr jate aani 'new' ne i.e. object ne bnvlelei string hi heap vr jate with reference aani without reference scp vr pn object jato ..scp he string constant pool aahe..aani scp vr repetation chalt nahi mhanje same strinh chalt nahi..constant goshti lagtat..
// hashcode ha 'content' ve depend asto..same content la same hashcode yeto
// identityHashCode() ha object la bhetnara unique id aahe..aani to depend krto data kontya section mdhe present aahe...heap vr asel tr same data asel tri different object bnun tyan chya respective i.e. different id bhettil pn integerCache/scp asel tr same id yetil karn ekch object kde reference gheun bslet te!!
//new mhantl ki nvinch object 
//khalchya code mdhe 2 object he heap(shweta,shweta) vr aani 2 he scp(shweta,bagul) vr bntil

class Demo{

	public static void main(String [] shweta){

		String str1 = "shweta";			//scp
		String str2 = new String("shweta");	//heap
		String str3 = new String("shweta");	//heap
		String str4 = "shweta";			//scp(but already present..so common refernce)
		String str5 = "bagul";			//scp(nvin object bnel fkt scp vr)

		System.out.println("str1 : "+ System.identityHashCode(str1));
		System.out.println("str2 : "+ System.identityHashCode(str2));
		System.out.println("str3 : "+ System.identityHashCode(str3));
		System.out.println("str4 : "+ System.identityHashCode(str4));
		System.out.println("str5 : "+ System.identityHashCode(str5));

		//content vrn hashcode bnvla jato..so same content asel tr same hashCode() yeil..one formula is used to create hashCode()
		System.out.println("str1 : "+ str1.hashCode());
		System.out.println("str2 : "+ str2.hashCode());
		System.out.println("str3 : "+ str3.hashCode());
		System.out.println("str4 : "+ str4.hashCode());
		System.out.println("str5 : "+ str5.hashCode());
	
	}
}
