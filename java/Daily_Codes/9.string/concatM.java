class ConcatDemo{

	public static void main(String [] shweta){

		String str1 = "Core2";
		String str2 = "Web";

		//concat() returns a 'new' object with concatinated string (reference from heap section)

		String str3 = str1.concat(str2);	

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}
