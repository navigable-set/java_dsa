/*default : lets use prog1 files default variable as default can be used outside the file but in same folder only*/

class Check{

	public static void main(String[] shweta){

		AccessSpecifier as = new AccessSpecifier();

		System.out.println("lets print the default variable from different file in same folder");

		System.out.println(as.x);
		
		as.x = 50;
		System.out.println(as.x);
		
		System.out.println(as.z);
		
		as.z = 100;
		System.out.println(as.z);

	}
}
