class AccessSpecifier{

	AccessSpecifier(){
		System.out.println("default can be acees from same folder");
		System.out.println("private can be acees from same class only");
	}

	int x = 10;
	private int y = 20;
	static int z = 30;

	void disp(){
	
		System.out.println(x);
		System.out.println(y);	//private can only access in same class/members of the class
		System.out.println(z);
	}
}

class Demo{

	public static  void main(String [] shweta){

		AccessSpecifier as = new AccessSpecifier();
		
		System.out.println(as.x);
		/*System.out.println(as.y);	-->'error' as y has private access so cannot be acees outside its class*/
		System.out.println(AccessSpecifier.z);

		as.disp();
	}

}
