import arithFun.Addition;
import arithFun.Substraction;

import java.util.Scanner;

class Demo{

	public static void main(String[] shweta){

		Scanner sc = new Scanner(System.in);

		int num1 = sc.nextInt();
		int num2 = sc.nextInt();

		Addition ad = new Addition(num1,num2);
		Substraction sb = new Substraction(num1,num2);

		int sum1 = ad.add();
		int sub1 = sb.sub();

		System.out.println("addition = " + sum1);
		System.out.println("substraction = " + sub1);
	}
}
