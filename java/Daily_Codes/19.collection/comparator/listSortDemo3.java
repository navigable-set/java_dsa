//user defined objets sort kruya.. in one same class using Comparator(I)..with generics

import java.util.*;

class Demo implements Comparator<Demo>{

	String name = null;

	Demo(){

	}

	Demo(String name){

        	this.name = name;
	}

	public int compare(Demo obj1,Demo obj2){
		
		//without generics if compare(Object,Object) lihila asta tr typecast krav lagl ast... pn generics mule 'Demo' parameter chalel
		//return (((Demo)obj1).name).compareTo(((Demo)obj2).name);
		
		return (obj1.name).compareTo(obj2.name);
	}
	
	public String toString(){
		return name;
	}

	public static void main(String[] shweta){

		Demo obj1 = new Demo("Shweta");
		Demo obj2 = new Demo("Yuvraj");
		Demo obj3 = new Demo("Bagul");
		Demo obj4 = new Demo("Yuvraj");

		ArrayList<Demo> al = new ArrayList<Demo>();
		
		al.add(obj1);
		al.add(obj2);
		al.add(obj3);
		al.add(obj4);

		System.out.println(al);

		//Collections.sort(al,new Demo("hii"));		//hi pn same o/p deil
		Collections.sort(al,new Demo());
		
		System.out.println(al);
	}
}
