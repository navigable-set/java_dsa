//List mdhe user defined objects he sort krtoy using Comparator(I)... mhanje sorting sathi objects he comparable type che nstil tri sorting krta yayla pahije mhanun Comparator(I) use kela jato

import java.util.*;
class Employee {

	String empName = null;
	float sal = 0.0f;

	Employee(String empName, float sal){
		this.empName = empName;
		this.sal = sal;
	}

	public String toString(){

		return "{" + empName + " : " + sal + "}";
	}
}

class SortByName implements Comparator{

	//typecasting kely.. generics use krun pn hot
	public int compare(Object obj1, Object obj2){
		return (((Employee)obj1).empName).compareTo(((Employee)obj2).empName);
	}
}

class SortBySal implements Comparator{

	//salary hi float type chi aahe..mg lossy conversion hoil from float to int mahnun.. typecast kely.. int mdhe
	public int compare(Object obj1, Object obj2){
		return (int) (((Employee)obj1).sal - ((Employee)obj2).sal);	//+ve aahe mhanun ascending order ne sorting hoil		
	//	return -(int) (((Employee)obj1).sal - ((Employee)obj2).sal);	//-ve mule descending order ne sorting hoil ethe
	}
}

class ListSortDemo{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		//objects he comparable type che nahia.. tri sorting possible aahe just because of Comparator(I)
		al.add(new Employee("shweta",300000.0f));
		al.add(new Employee("aakash",450000.0f));
		al.add(new Employee("tejashree",420000.0f));
		al.add(new Employee("darshan",800000.0f));
		al.add(new Employee("sarang",700000.0f));
		al.add(new Employee("sarang",700000.0f));

		System.out.println("before sorting");
		System.out.println(al);

		System.out.println("Sort by name :");
		Collections.sort(al, new SortByName());
		System.out.println(al);
	
		System.out.println("Sort by salary :");
		Collections.sort(al, new SortBySal());
		System.out.println(al);
	}
}
