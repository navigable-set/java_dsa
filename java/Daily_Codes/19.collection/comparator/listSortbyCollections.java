//comparable use kruya.. sorting sathi.. (condition : data is of comparable type)!!
//public static void swap(java.util.List<?>, int, int);

import java.util.*;
class Employee implements Comparable{

	String empName = null;
	float sal = 0.0f;

	Employee(String empName, float sal){
		this.empName = empName;
		this.sal = sal;
	}

	public int compareTo(Object obj){
		return empName.compareTo(((Employee)obj).empName);
	}

	public String toString(){

		return "{" + empName + " : " + sal + "}";
	}
}


class ListSortDemo{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		al.add(new Employee("shweta",200000.0f));
		al.add(new Employee("aakash",400000.0f));
		al.add(new Employee("tejashree",400000.0f));
		al.add(new Employee("darshan",800000.0f));
		al.add(new Employee("sarang",700000.0f));
		al.add(new Employee("saareg",700000.0f));

		Collections.sort(al);
		System.out.println(al);

/*		//###Collections class mdhe 'swap()' navachi method aahe.. ji dilele index che objects he swap krun dete
		Collections.swap(al,2,3);
		System.out.println(al);
*/

	}
}
