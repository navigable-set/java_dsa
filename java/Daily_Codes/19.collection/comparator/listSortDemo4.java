//comparator in user-defined in list for sorting

import java.util.*;
class Movies{

	String movieName = null;
	double totColl = 0.0;
	float imdbRating = 0.0f;
	
	Movies(String movieName, double totColl, float imdbRating){

		this.movieName = movieName;
		this.totColl = totColl;
		this.imdbRating = imdbRating;
	}

	public String toString(){

		return "{" + movieName + "," + totColl + "," + imdbRating + "}" ;
	}
}

class SortByName implements Comparator<Movies>{

	public int compare(Movies obj1, Movies obj2){
		return ((obj1.movieName).compareTo(obj2.movieName));
	}
}

class SortByColl implements Comparator<Movies>{

	public int compare(Movies obj1, Movies obj2){
		return (int) (obj1.totColl - obj2.totColl);
	}
}

//Rating ne sort krtanna output wrong yety
class SortByRating implements Comparator<Movies>{

	public int compare(Movies obj1, Movies obj2){
		return (int) ((obj1.imdbRating) - (obj2.imdbRating));
	}
}


class UserListSort{

	public static void main(String[] shweta){

		ArrayList<Movies> al = new ArrayList<Movies>();

		al.add(new Movies("Shiddat",250.00,8.8f));
		al.add(new Movies("ZNMD",180.00,9.5f));
		al.add(new Movies("Sita-Ramam",300.00,9.8f));
		al.add(new Movies("Queen",200.00,9.2f));
		al.add(new Movies("Kakan",30.00,9.9f));

		System.out.println("Before sorting : ");
		System.out.println(al);

		System.out.println("Sort by name : ");
		Collections.sort(al,new SortByName());
		System.out.println(al);
		
		System.out.println("Sort by collection : ");
		Collections.sort(al,new SortByColl());
		System.out.println(al);

		System.out.println("Sort by imdbRating : ");
		Collections.sort(al,new SortByRating());
		System.out.println(al);
	}
}
