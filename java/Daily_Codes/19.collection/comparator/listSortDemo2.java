//Sorting List by Comparator...using generics

import java.util.*;
class Employee {

        String empName = null;
        float sal = 0.0f;

        Employee(String empName, float sal){
                this.empName = empName;
                this.sal = sal;
        }

	public String toString(){

                return "{" + empName + " : " + sal + "}";
        }
}

class SortByName implements Comparator<Employee>{

        //typecasting kray chi grj nahia karn generics use kely
        public int compare(Employee obj1, Employee obj2){
                return (obj1.empName).compareTo(obj2.empName);
        }
}

class SortBySal implements Comparator<Employee>{

        //salary hi float type chi aahe..mg lossy conversion hoil from float to int mahnun.. typecast kely.. int mdhe
        public int compare(Employee obj1, Employee obj2){
                return (int) (obj1.sal - obj2.sal);     //+ve aahe mhanun ascending order ne sorting hoil               
        }
}

class ListSortDemo{

        public static void main(String[] shweta){

                ArrayList<Employee> al = new ArrayList<Employee>();

                al.add(new Employee("shweta",300000.0f));
                al.add(new Employee("aakash",450000.0f));
                al.add(new Employee("tejashree",420000.0f));
                al.add(new Employee("darshan",800000.0f));
                al.add(new Employee("sarang",700000.0f));
                al.add(new Employee("sarang",700000.0f));
 
 		System.out.println("before sorting");
                System.out.println(al);

                System.out.println("Sort by name :");
                Collections.sort(al, new SortByName());
                System.out.println(al);

                System.out.println("Sort by salary :");
                Collections.sort(al, new SortBySal());
                System.out.println(al);
        }
}
