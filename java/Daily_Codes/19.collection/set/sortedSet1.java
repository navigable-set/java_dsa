//methods of SortedSet

import java.util.*;
class SortedSetDemo{

	public static void main(String[] shweta){

		//interface cha reference chalto..aani to TreeSet cha parent aahe
		SortedSet ss = new TreeSet();

		//duplicates not allowed..Soretd fprmat of data ==> SortedSet(I)
		ss.add("Aasawari");
		ss.add("Divya");
		ss.add("Dhanashree");
		ss.add("Aasawari");
		ss.add("Shweta");
		ss.add("Nikita");

		System.out.println(ss);

		System.out.println("Methods of SortedSet : ");

		//public abstract java.util.SortedSet<E> subSet(E, E);
		System.out.println(ss.subSet("Dhanashree","Nikita"));	//SubSet kadhun deto tya set mdhla..within given range
	
		//public abstract java.util.SortedSet<E> headSet(E);
		System.out.println(ss.headSet("Nikita"));

		//public abstract java.util.SortedSet<E> tailSet(E);
		System.out.println(ss.tailSet("Nikita"));	

  		//public abstract E first();
		System.out.println(ss.first());
		
 		//public abstract E last();
		System.out.println(ss.last());
	}
}
