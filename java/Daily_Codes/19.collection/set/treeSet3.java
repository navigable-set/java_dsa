//user-defined TreeSet example... ha code pn chaltoy pn Sorting nahi hot aahe..karn compareTo() mdhe proper logic lihav lagel

import java.util.*;

class Demo implements Comparable{

	String name = null;

	Demo(String name){
		this.name = name;
	}

	public int compareTo(Object obj){
		return 1;
		//return -1; //he reverse order ie. decending order ne answer deil
	}

	public String toString(){
		return name;
	}
}

class TreeSetDemo{

	public static void main(String[] shweta){
		
		TreeSet ts = new TreeSet();
		/*
		ArrayList al = new ArrayList();	
		ts.add(al);	==> classCastexception : ArrayList cannot be cast to Comparable*/				

		ts.add(new Demo("Shweta"));
		ts.add(new Demo("sangram"));
		ts.add(new Demo("aakash"));
		ts.add(new Demo("prachi"));
		ts.add(new Demo("abhishek"));
		ts.add(new Demo("sushen"));

		System.out.println(ts);		//sort krun detoy data aani duplicate pn allowed nahia
	}
}
