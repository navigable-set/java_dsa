//user-defined classe's objects in TreeSet using 'Comparable(I)'

import java.util.*;

class Movies implements Comparable{

	String movieName = null;
	float totalColl = 0.0f;

	Movies(String movieName,float totalColl){
		this.movieName = movieName;
		this.totalColl = totalColl;
	}

	public int compareTo(Object obj){

		return movieName.compareTo(((Movies)obj).movieName);	// + ve mule to ascending order ne data sort krun deto
	//	return -(movieName.compareTo(((Movies)obj).movieName));	// - ve lavla li to descending order ne sort krun deto data
	}		

	public String toString(){
		return "{" + movieName + " : " + totalColl + "}" ;
	}
}

class UserDefinedDemo{

	public static void main(String[] shweta){

		TreeSet ts = new TreeSet();

		ts.add(new Movies("Gadar2",250.5f));
		ts.add(new Movies("OMG2",150.5f));
		ts.add(new Movies("Jailor",50.5f));
		ts.add(new Movies("Gadar2",250.5f));	//duplicate
		ts.add(new Movies("Jailor",100.5f));	//duplicate
	
		System.out.println(ts);
	}
}
