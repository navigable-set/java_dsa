//Navigable set methods : 

import java.util.*;

class NavigableSetDemo{

	public static void main(String[] shweta){

		NavigableSet ns = new TreeSet();

		ns.add("Aasawari");
                ns.add("Divya");
                ns.add("Dhanashree");
                ns.add("Aasawari");
                ns.add("Shweta");
                ns.add("Nikita");

		System.out.println(ns);

		//public abstract E lower(E);
		System.out.println(ns.lower("Divya"));		//'lower' ha previous object return krto
	//	System.out.println(ns.lower("Aasawari"));	//'null' karn first object chya aadhi konta ch object nsel
	
		//public abstract E floor(E);
		System.out.println(ns.floor("Divya"));

		//public abstract E ceiling(E);
		System.out.println(ns.ceiling("Divya"));

		//public abstract E higher(E);
		System.out.println(ns.higher("Divya"));		//'higher' ha next object return krto

	  	//public abstract E pollFirst();
		System.out.println(ns.pollFirst());		//remove krte first object aani return krte
	
		//public abstract E pollLast();
		System.out.println(ns.pollLast());		//remove krte last object aani return krte
		
		//public abstract java.util.NavigableSet<E> descendingSet();
		System.out.println(ns.descendingSet());		//set la desscending order ne print krun deto

		System.out.println(ns);
	}
}
