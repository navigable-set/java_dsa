//aapn 'comparable' use krun HashSet/LinkedHashSet mdhe userDefined objects je duplicate aahet te remove kru shakt nahi.. pn TreeSet la chalt te!!
import java.util.*;
class Demo implements Comparable{

	int id = 0;
	String name = null;

	Demo(int id,String name){
		this.id = id;
		this.name = name;
	}

	public String toString(){
		return id + ":" + name;
	}

	public int compareTo(Object obj){

		//###'this' ha current object la represent krtoy..'new' vale.... aani 'obj' mhanje... add() method hi jo ek ek object pathvnar aahe tya Set-collection mdhle ..compareTo(obj) la call krtanna... zero-th index pasn aani ts tse pudhe... tya index cha object ha...'obj' mdhe aahe... aani 'new' vala tr 'this' ne hoto ch aahe.. may be
		System.out.println("This.name = "+ this.name);
		System.out.println("This= "+ System.identityHashCode(this)) ;
		System.out.println("obj = "+ System.identityHashCode(obj)) ;
		System.out.println("Demo.name = "+((Demo)obj).name);

		//typecast krtoy
		return (name.compareTo(((Demo)obj).name));
	}
}

class UserDefinedDemo{

	public static void main(String[] shweta){

		TreeSet ls = new TreeSet();

	//LinkedHashSet aani HashSet la duplicate pn add hotay..vr compareTo() jri asli tri ...karn LinkedHashSet aani HashSet chi add() hi internally compareTo(Object) la call krt nahi!!.. pn TreeSet use keli tr tyla comparable type cha ch data lagto..mg tyachi TreeSet chi add() hi internally compareTo() la call krte
	//	LinkedHashSet ls = new LinkedHashSet();	
	//	HashSet ls = new HashSet();
		
		ls.add(new Demo(7,"Dhoni"));
		ls.add(new Demo(18,"virat"));
		ls.add(new Demo(45,"Rohit"));
		ls.add(new Demo(1,"sachin"));
		ls.add(new Demo(12,"Dhoni"));	//id vegli aahe..pn aapn compare he string ne krtoy mhanun add nahi honar ..duplicate aahe mhanun
		ls.add(new Demo(new Integer(18),"sachin"));

		System.out.println(System.identityHashCode(ls));
		System.out.println(ls);
	}
}
