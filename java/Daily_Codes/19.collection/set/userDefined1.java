//user-defined classes : la klt nahi ki data duplicate hotoy..te aaplyala handle krav lagt..predefined mdhe tyala mahiti ast sgl ki duplicate aahe ki nahia te... 

import java.util.*;
class Demo {

	int id = 0;
	String name = null;

	Demo(int id,String name){
		this.id = id;
		this.name = name;
	}

	public String toString(){
		return id + ":" + name;
	}
}

class UserDefinedDemo{

	public static void main(String[] shweta){

		LinkedHashSet ls = new LinkedHashSet();

		ls.add(new Demo(7,"mahi"));
		ls.add(new Demo(18,"virat"));
		ls.add(new Demo(45,"rohit"));
		ls.add(new Demo(1,"sachin"));
		ls.add(new Demo(45,"rohit"));	//hashSet/LinkedHashSet(c) mdhe userDefined mdhe to duplicate pn add krun ghetoy karn tyala klt nahia ki te doghi object same aahet.
		ls.add(new Demo(new Integer(1),"sachin"));	//he pn duplicate ch zal...new or without-new che integer aso..collection mdhe sglech Object aahet

		System.out.println(ls);
	}
}
