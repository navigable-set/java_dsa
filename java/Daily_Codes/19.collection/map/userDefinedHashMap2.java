//userDefined objets he HashMap la key mhanun deuya aani duplicate keys accept nko krayla mhanun sorting kruya.. pn nahi hot aahe..!!
//[hint : TreeSet use kela tri pn!!]

import java.util.*;
class Company implements Comparable{

	String cName = null;
	int foundation_year = 0;

	Company(String cName,int foundation_year){
		
		this.cName = cName;
		this.foundation_year = foundation_year;
	}

	public String toString(){

		return "{" + cName + " :" + foundation_year + "}";
	}

	public int compareTo(Object obj){
		
		return cName.compareTo(((Company)obj).cName);
	}
}

class UserDefinedDemo{

	public static void main(String[] shweta){

		HashMap hm = new HashMap();
		
		//company and their foundationYear
		Company obj1 = new Company("Instagram",2010);
		Company obj2 = new Company("Youtube",2005);
		Company obj3 = new Company("Facebook",2004);
			
		//company with their parentCompany
		hm.put(obj1,"Google");
		hm.put(obj2,"Google");
		hm.put(obj3,"Meta");
	
		//public java.util.TreeMap(java.util.Map<? extends K, ? extends V>);
		TreeMap tm = new TreeMap(hm);
		
		System.out.println(tm);
		//HashMap nahi sort zala tr HashMap la TreeMap mdhe convert krun tya TreeMap la Sort kely.. HashMap nahi sort zala 
		System.out.println(hm);

		//no change... Hashmap is not sorted
		//public void putAll(java.util.Map<? extends K, ? extends V>);
//		hm.putAll(tm);
//		System.out.println(hm);

//		hm = tm.values();
//		System.out.println(hm);		//error : collection to map

/*
		System.out.println(hm.hashCode());  //1628069879

		System.out.println(obj1.hashCode());	//1252169911
		System.out.println(obj2.hashCode());	//2101973421
		System.out.println(obj3.hashCode());	//685325104
*/
	}
}
