//Iterating on Map by converting 'Map' into 'Set'

import java.util.*;
class IteratorOnMap{

	public static void main(String[] shweta){

		TreeMap tm = new TreeMap();

		tm.put("Ind","India");
                tm.put("Pak","Pakistan");
                tm.put("Pak","New_Pakistan");
                tm.put("Ban","Bangladesh");
                tm.put("Aus","Australia");
                tm.put("Sl","Srilanka");

		System.out.println(tm);

		//public java.util.Set<java.util.Map$Entry<K, V>> entrySet();
/*
 		//ya mule aaplyala 'key,value' asa separate data nahi milnar
		//karn Normal Set mdhe fkt 'single objects' astat aani Map mdhe 'key,value' mhanje 2-objects astat eka entry mdhe... mg tya single objects mdhn 'key,value' separe nahi krta yenar may be tayla.. mhanun aadhi ch tyala generics sarkh deun sangav lagel..te khali kely aapn!!
		Set s = tm.entrySet();

		Iterator itr = s.iterator();

		while(itr.hasNext()){
			
			//Map.Entry data = itr.next();		//error : object cannot be converted to Entry
			//System.out.println(data.getKey() + " : " + data.getValue());	
			
			System.out.println(itr.next());			//will give (key,value) directly 
			
			//System.out.println(itr.next().getKey());	//error : 'getKey()' sathi
		}
*/
		//entrySet aaplyala key,value data deto..tyala key,value format mdhe ch aaplyala thevay cha asel tr 'Entry' use krava ch lagel.. nsel key,value hva tr ..normal set ne pn hot iterate!!

		Set<Map.Entry> s = tm.entrySet();
		Iterator<Map.Entry> itr = s.iterator();

		while(itr.hasNext()){
			
			//System.out.println(itr.next());	
			//System.out.println(itr.next().getKey());	
			//System.out.println(itr.next().getValue());	

			Map.Entry data = itr.next();
			System.out.println(data.getKey() + " : " + data.getValue());	
		}
	}

}
