//userDefined objets he HashMap la key mhanun deuya

import java.util.*;
class Company {

	String cName = null;
	int foundation_year = 0;

	Company(String cName,int foundation_year){
		
		this.cName = cName;
		this.foundation_year = foundation_year;
	}
/*
	Company(int x){
		foundation_year = x;
	}*/

	public String toString(){

		return "{" + cName + " :" + foundation_year + "}";
	}
}

class UserDefinedDemo{

	public static void main(String[] shweta){

		HashMap hm = new HashMap();
		
		//company and their foundationYear
		Company obj1 = new Company("Instagram",2010);
		Company obj2 = new Company("Youtube",2005);
		Company obj3 = new Company("Facebook",2004);
		Company obj4 = new Company("Youtube",2005);	//same content aahe..tri hashMap add krtoy karn tyala klt nahi ki duplicate keys aahet ya(user-defined mdhe)
			
		//company with their parentCompany
		//key is object of user-defined type
		hm.put(obj1,"Google");
		hm.put(obj2,"Alphabet");
		hm.put(obj3,"Meta");
		hm.put(obj4,"Google");

/*		//nvin objets asle tri content same aahe..mhanun duplicate key cha data add nahi honar.. pn hech predefined sthi nahi klt tyala
		hm.put(new String("Instagram"),"Google");
		hm.put(new String("Facebook"),"meta");
		hm.put(new String("Instagram"),"meta");
		hm.put(new String("LinkedIn"),"meta");
*/
/*		hm.put(new Company(10),"Google");
		hm.put(new Company(20),"Bagul");
		hm.put(new Company(10),"Bara");
*/				
/*		hm.put(20,"google");
		hm.put(10,"Bara");
		hm.put(10,"Bagul");
		
*/		System.out.println(hm);
	}
}
