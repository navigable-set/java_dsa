//weakHashMap : HashMap mdhli entry hi garbage collector free kru shakt nahi jri tya entry/object la reference nsel tr.. 
//HashMap ha dominate krto Garbage collector la mhanun WeakHashMap aanlay

import java.util.*;

class Demo{

	String name = null;

	Demo(String name){
		this.name = name;
	}

	//overriding toString() of object
	public String toString(){

		return name;
	}

	//overriding finalize() of object
	public void finalize(){
		
		System.out.println("Notify");	//GC jevha objects free krto..tevha aadhi tya class la notify krto ki mi tujha object free  krtoy aata
	}
}

class WeakHashMapDemo{

	public static void main(String[] shweta){
	
		WeakHashMap wm = new WeakHashMap();
		
		//khali dilele sgle remove nahi krt aahet entry!!
		//HashMap wm = new HashMap();
		//LinkedHashMap wm = new LinkedHashMap();
		//IdentityHashMap wm = new IdentityHashMap();
		//Hashtable wm = new Hashtable();
		//TreeMap pn entry remove nahi krt
/*
		wm.put(10,100);
		wm.put(4,49);
		wm.put(5,25);
		wm.put(12,144);

*/
		Demo obj1 = new Demo("shweta");
		Demo obj2 = new Demo("tejashree");
		Demo obj3 = new Demo("darshan");
		Demo obj4 = new Demo("aakash");
		Demo obj5 = new Demo("dikshu");
		
		wm.put(obj1,2002);
		wm.put(obj2,1997);
		wm.put(obj3,2003);
		wm.put(obj4,2003);
		wm.put(obj5,2004);
		wm.put(10,2000);

		System.out.println(wm);		
		
		obj1 = null; 
		
		//forcefully remove krtoy
		System.gc();		//finalize() la call jail
		
		System.out.println(wm);		//obj1 chi entry remove zali aata
	}
}
