//Garbage collector working


class Demo{

	String name = null;

	Demo(String name){

		this.name = name;
	}

	public String toString(){

		return name;
	}

	//overriding 'finalize()' of Object class.. because GC calls this method for giving Notification ki to ya class cha object free krnar aahe aata!
	public void finalize(){

		System.out.println("Notify");
	}
}

class GCDemo{

	public static void main(String[] shweta){

		Demo obj1 = new Demo("Core2web");
		Demo obj2 = new Demo("Biencaps");
		Demo obj3 = new Demo("Incubator");

		System.out.println(obj1);	//object print kela tr to 'toString()' call deun print hot asto
		System.out.println(obj2);
		System.out.println(obj3);

		obj1 = null;
		obj2 = null;
		
		System.gc();	//forcefully sangtoy GC la ki tu ya objects la aata ch free kr..mhanun to finalize la call krto

		System.out.println("In Main");
	}
}
