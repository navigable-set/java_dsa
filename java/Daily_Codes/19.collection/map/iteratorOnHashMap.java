//Iterating on Map by converting 'Map' into 'Set'

import java.util.*;
class IteratorOnMap{

        public static void main(String[] shweta){
	
     		HashMap tm = new HashMap();

//		TreeMap tm = new TreeMap();
//              LinkedHashMap tm = new LinkedHashMap();
//              IdentityHashMap tm = new IdentityHashMap();		
//		Hashtable tm = new Hashtable();

                tm.put("Ind","India");
                tm.put("Pak","Pakistan");
                tm.put("Pak","New_Pakistan");
                tm.put("Ban","Bangladesh");
                tm.put("Aus","Australia");
                tm.put("Sl","Srilanka");

                System.out.println(tm);
  		
		//'Map' mdhli method aahe hi ==> entrySet()
		//public abstract java.util.Set<java.util.Map$Entry<K, V>> entrySet();
		
/*		Set s = tm.entrySet();
		Iterator itr = s.iterator();

		while(itr.hasNext()){

			System.out.println(itr.next());
			//System.out.println(itr.next().getKey());	//error : cannot find symbol => getKey() sathi
		}
*/
		Set<Map.Entry> s = tm.entrySet();
		Iterator<Map.Entry> itr = s.iterator();
		
		while(itr.hasNext()){

			//System.out.println(itr.next().getKey());
			Map.Entry data = itr.next();
			System.out.println(data.getKey() + " : " + data.getValue());	
		}
	}
}
