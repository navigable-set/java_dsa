//user-defined objets la sort kruya in TreeMap

import java.util.*;

class Platform implements Comparable<Platform>{

	String cName = null;
	int foundation_year = 0;

	Platform(String cName, int foundation_year){

		this.cName = cName;
		this.foundation_year = foundation_year;
	}

	public int compareTo(Platform obj){
		
		return cName.compareTo(obj.cName);
	}

	public String toString(){

		return "[" + cName + " : " + foundation_year + "]";
	}
}

class TreeMapDemo{

	public static void main(String[] shweta){

		TreeMap<Platform,String> tm = new TreeMap<Platform,String>();

		tm.put(new Platform("Google",1998),"Alphabet");
		tm.put(new Platform("YouTube",2005),"Google");
		tm.put(new Platform("YouTube",2006),"newCompany");
		tm.put(new Platform("FaceBook",2004),"Meta");
		tm.put(new Platform("Instagram",2010),"Meta");

		System.out.println(tm);
	}
}
