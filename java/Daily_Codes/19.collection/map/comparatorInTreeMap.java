//user-defined objets la sort kruya in TreeMap : using comparator

import java.util.*;

class Platform{

	String cName = null;
	int foundation_year = 0;

	Platform(String cName, int foundation_year){

		this.cName = cName;
		this.foundation_year = foundation_year;
	}
	
	public String toString(){

		return "[" + cName + " : " + foundation_year + "]";
	}
}

class SortbyName implements Comparator{

	public int compare(Object obj1, Object obj2){
		
		return (((Platform)obj1).cName).compareTo(((Platform)obj2).cName);
	}
}

class SortbyYear implements Comparator{

	public int compare(Object obj1, Object obj2){
		
		return (((Platform)obj1).foundation_year) -  (((Platform)obj2).foundation_year) ;
	}
}

class TreeMapDemo{

	public static void main(String[] shweta){

		//TreeMap tm = new TreeMap();	//error : Platform cannot be cast to java.lang.Comparable
  		
  		//public java.util.TreeMap(java.util.Comparator<? super K>);
		
		TreeMap tm = new TreeMap(new SortbyName());
		//TreeMap tm = new TreeMap(new SortbyYear());
		
		tm.put(new Platform("Google",1998),"Alphabet");
		tm.put(new Platform("YouTube",2005),"Google");
		tm.put(new Platform("YouTube",2006),"newCompany");
		tm.put(new Platform("FaceBook",2005),"Meta");
		tm.put(new Platform("Instagram",2010),"Meta");
/*
		//error: no suitable method found for sort(TreeMap,SortbyName)
		Collections.sort(tm,new SortbyName());		//tyachya kde 'list' parameter sathi aahe sort()... 'map' sathi nahia

*/		System.out.println(tm);
	}
}
