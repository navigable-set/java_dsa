//LinkedHashMap(c) is a child class of HashMap(c)
//LinkedHashMap is used if you want the output in a sequence, same as it is inserted(means linkedHashMap does not call hashFunction internally)
//duplicate keys are not allowed
//duplicate values are allowed

import java.util.*;
class LinkedHashmapDemo{

	public static void main(String[] shweta){
	
		LinkedHashMap lm = new LinkedHashMap();
		//HashMap lm = new HashMap();
/*
		lm.put("shweta","EQTech");
		lm.put("Tejashree","Capgemini");
		lm.put("Darshan","Microsoft");
		lm.put("Aakash","EQTech");
		lm.put("shweta","Amazon");
*/
		//output is same as insertion order 
		lm.put(10,1);
		lm.put(20,2);
		lm.put(new Integer(10),3);
		lm.put(new Integer(40),4);
		lm.put("shweta","infinity");
		lm.put(30,5);

/*
		//public V get(java.lang.Object);
		System.out.println(lm.get("shweta"));	

		//public V getOrDefault(java.lang.Object, V);
		System.out.println(lm.getOrDefault("shweta","Microsoft"));	

		//lm.clear();		
*/		
		System.out.println(lm);
	}
}
