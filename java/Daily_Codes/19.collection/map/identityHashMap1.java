//IdentityHashMap(c) is chils of 'Map(I)' ==> where duplicate 'keys' are allowded as it does not compare the keys(data) but 'address' of the objects that are been added into it

import java.util.*;
class IdentityHashMapDemo{

	public static void main(String[] shweta){

		IdentityHashMap im = new IdentityHashMap();

		//"" =>ya string 'scp' vr janar aahet..mahnun nvin object nahi bnnarjr same string(key) asel tr.. pn jr 'new' ne objects create kele tr nvin ch object bnto
		im.put("shweta","EQTech");
		im.put("aakash","Capgemini");
		im.put("tejashree","Capgemini");
		im.put("Darshan","Microsoft");
		im.put("Darshan","Amazon");
		//nvin object('new' mule)
		im.put(new String("aakash"),"EQTech");
		
		//2-object(outOf Integer catche range)
		im.put(155,10);
		im.put(155,5);
	
		//only 1-object(Integer cache)
		im.put(20,5);
		im.put(20,8);

		//2-object('new' mule)
		im.put(new Integer(20),2);
		im.put(new Integer(20),6);

		System.out.println(im);
	}
}
