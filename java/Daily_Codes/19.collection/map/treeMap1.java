//TreeMap(c) class ha SortedMap(I) aani NavigableMap(I).. yancha child class aahe

import java.util.*;
class TreeMapDemo{

	public static void main(String[] shweta){

		TreeMap tm = new TreeMap();	//data sort houn yenar

		tm.put(10,100);
		tm.put(13,169);
		tm.put(30,50);
		tm.put(35,42);
//		tm.put("shweta",49);	//error : classCastException : Integer and String
		tm.put(new Integer(10),200);
		tm.put(12,144);

		System.out.println(tm);
	}
}
