//SortedMap(I) is a child interface of Map

import java.util.*;
class SortedMapDemo{

	public static void main(String[] shweta){

		SortedMap tm = new TreeMap();
		//NavigableMap tm = new TreeMap();

		tm.put("Ind","India");
		tm.put("Pak","Pakistan");
		tm.put("Pak","New_Pakistan");
		tm.put("Ban","Bangladesh");
		tm.put("Aus","Australia");
		tm.put("Sl","Srilanka");

		System.out.println(tm);		//sorting by String
	
		//public abstract java.util.SortedMap<K, V> subMap(K, K);
		System.out.println(tm.subMap("Aus","Pak"));
		
		//public abstract java.util.SortedMap<K, V> headMap(K);
		System.out.println(tm.headMap("Pak"));
	
  		//public abstract java.util.SortedMap<K, V> tailMap(K);
		System.out.println(tm.tailMap("Pak"));
	
  		//public abstract K firstKey();
		System.out.println(tm.firstKey());
	
  		//public abstract K lastKey();
		System.out.println(tm.lastKey());
  
		//public abstract java.util.Set<K> keySet();
		System.out.println(tm.keySet());
  
		//public abstract java.util.Collection<V> values();
		System.out.println(tm.values());
  
		//public abstract java.util.Set<java.util.Map$Entry<K, V>> entrySet();
		System.out.println(tm.entrySet());
	}
}
