//HashMap(c) ha 'Map' ch child aahe.. mhanun (K,V) pair mdhe data yeto yat!!
//key ==> unique lagte
//value ==> duplicate asel tr chalte HashMap mdhe
//data add krnya sathi 'put(K,V)' mathod use keli jate 'Map' mdhe

import java.util.*;
class HashMapDemo{

	public static void main(String[] shweta){

		HashSet hs = new HashSet();

		hs.add("Shweta");
		hs.add("Tejashree");
		hs.add("Darshan");
		hs.add("Aakash");

		System.out.println(hs);	//hashSet aani hashMap cha output cha sequence same aahe..mhanje hashSet internally hashMap la ch call krto

	//	HashMap<String,String> hm = new HashMap<String,String>();	//generics
		HashMap hm = new HashMap();

		//keys duplicate aalya tr..value replace hotat
		//value duplicate chaltat
		hm.put("Shweta","EQTech");
		System.out.println(hm.put("Shweta","Amazon"));
		hm.put("Tejashree","Capgemini");
		hm.put("Darshan","Microsoft");
		hm.put("Aakash","Amazon");

		System.out.println(hm);
	}
}
