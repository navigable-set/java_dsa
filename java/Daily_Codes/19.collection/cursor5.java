//check type of cursor (name of their innner classes)


import java.util.*;
class ArrayListDemo{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20.5f);
		al.add("shweta");
		al.add(35.09);
		al.add('A');

		System.out.println(al);

		Iterator itr = al.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		System.out.println("ArrayList Iterator : " + itr.getClass().getName());		//ArrayList$Itr
	
		ListIterator li = al.listIterator();

		while(li.hasNext()){
                        System.out.println(li.next());
                }

		System.out.println("ArrayList ListIterator : " + li.getClass().getName());		//ArrayList$ListItr

//**********************************************************************************************************

		//LinkedList mdhe iterator()/listIterator() he internally 'LinkedList$ListItr' ya inner class cha object(i.e. cursor) detat... LinkedList (class) use kela tr tyat Iterator/ListIterator (interfaces) chya methods la body aahe.... tya cursor/object vr Iteraor/ListIterator chya methods la call jail... pn LinkedList mdhe iteraor()	internally listIterator() la ch call krto..mhanun same name of class(innerclass)
		LinkedList ll = new LinkedList();

		ll.add("hello");
		ll.add(14);
		ll.add('S');
		ll.add(33.f);
		ll.add(20.25);

		System.out.println(ll);

		Iterator itr2 = ll.iterator();

		while(itr2.hasNext()){
			System.out.println(itr2.next());
		}
		
		System.out.println("LinkedList Iterator : " + itr2.getClass().getName());	//LinkedList$ListItr

		ListIterator li2 = ll.listIterator();

		while(li2.hasNext()){
			System.out.println(li2.next());
		}

		System.out.println("LinkedList ListIterator : " + li2.getClass().getName());	//LinkedList$ListItr
	
	}
}
