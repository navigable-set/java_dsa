import java.util.*;

class IteratorDemo{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		al.add("Ashish");
		al.add("Rahul");
		al.add("Kanha");
		al.add("Badhe");

		System.out.println(al);

		Iterator itr = al.iterator();
		
		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		System.out.println(al);

	//	System.out.println(itr.next());	//'NoSuchElementException'..karn itr aata last object vr hota..next() mule pudhe gela..pudhe kahi nahi

		//ajun ek 'itr1' navacha iterator bnvla aahe
		Iterator itr1 = al.iterator();
		while(itr1.hasNext()){
			System.out.println(itr1.next());
		}

		//aata 'itr1' ha cursor collection chya last element/object vr aahe... hasNext() aaplyala false return krel..mhanun ya for mdhe janar ch nahi
		while(itr1.hasNext()){
			System.out.println("in second while");
			System.out.println(itr1.next());
		}

		System.out.println(al);
	//	System.out.println(itr.next());	//'NoSuchElementException'..karn itr aata last object vr hota..next() mule pudhe gela..pudhe kahi nahi
	}
}
	
