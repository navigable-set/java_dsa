import java.util.*;

class LinkedListDemo{

	public static void main(String[] shweta){

		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.add("shweta");
		ll.add("darshan");

		System.out.println(ll);
		
		System.out.println("in iterator");

	//	Iterator itr = ll.listIterator();	==> same output... listIterator() cha return type ha ListIterator aahe..aani tyacha parent Iterator aahe.. mhanun compatible aahe te!!
		Iterator itr = ll.iterator();

		while(itr.hasNext()){
			System.out.println(itr.next());
		}

		System.out.println(ll);
		System.out.println(itr.getClass().getName());
		
		System.out.println("in listIterator");

	//	ListIterator li = ll.iterator();	==>Iterator(parent) cannot be converted to ListIterator(child)
		ListIterator li = ll.listIterator();

		while(li.hasNext()){
			System.out.println(li.next());
		}
		System.out.println(ll);
		System.out.println(itr.getClass().getName());
	}
}
