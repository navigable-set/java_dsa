//vector class vr 4 cursors chaltil ==>
//1.Enumeration 2.Iterator 3.ListIterator 4.SplitIterator

import java.util.*;
class VectorDemo{

	public static void main(String[] shweta){
		
		Vector v = new Vector();
		/*
		Vector v = new Vector(4);
		System.out.println(v.capacity());	//capacity = 4
		*/	
		v.addElement("shweta");
		v.addElement(10);
		v.addElement("bagul");
		v.addElement(29.6f);
		v.add('S');

		System.out.println(v.removeElement(19));	//false....karn remove nahi zalay to object karn to present nahia... object parameter
		System.out.println(v.capacity());		//default capacity = 10 
		System.out.println(v);

		Iterator itr = v.iterator();

		while(itr.hasNext()){
			System.out.println(itr.next());
		}
		System.out.println("Vector - Iterator : " + itr.getClass().getName());		//java.util.Vector$Itr

		ListIterator li = v.listIterator();

		while(li.hasNext()){
			System.out.println(li.next());
		}
		System.out.println("Vector - ListIterator : " + li.getClass().getName());	//java.util.Vector$ListItr

		Enumeration em = v.elements();

		while(em.hasMoreElements()){
			System.out.println(em.nextElement());
		}
		System.out.println("Vector - Enumeration : " + em.getClass().getName());	//java.util.Vector$1
	}
}
