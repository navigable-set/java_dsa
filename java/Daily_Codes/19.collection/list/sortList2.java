//list mdhe pre-defined objects sort krta yetat.. using... 'Collections' ya class chi sort(list) method... user-defined objects pn sort hotat using 'Collections' class chi 'sort()' method
//collections class chi sort() method cha parameter ha list aahe.. ti list sort krun dete.. aani duplicate pn as it is rahtat.. ji ki 'list' chi property aahe


import java.util.*;

class Demo implements Comparable{

	String name = null;

	Demo(String name){
		this.name = name;
	}

	public int compareTo(Object obj){
		return name.compareTo(((Demo)obj).name);
	}

	public String toString(){
		return "{" + name + "}";
	}
}
class SortingList{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		al.add(new Demo("shweta"));
		al.add(new Demo("diksha"));
		al.add(new Demo("rudra"));
		al.add(new Demo("rudra"));
		al.add(new Demo("ravi"));
		al.add(new Demo("prema"));
		al.add(new Demo("siya"));

		System.out.println(al);
		Collections.sort(al);
		System.out.println(al);
	}
}
