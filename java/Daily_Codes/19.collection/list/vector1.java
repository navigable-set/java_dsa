//vector is legacy class...
//many methods are there.. only few are tried

import java.util.*;
class VectorDemo{

	public static void main(String[] shweta){
	
		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);
		v.addElement(50);

		System.out.println(v);

		System.out.println("capacity = " + v.capacity());

		v.removeElement(30);
		
		System.out.println(v);

		ArrayList al = new ArrayList();

		al.add("shweta");
		al.add("divya");
		al.add("dikshu");

		v.addAll(al);

		System.out.println(v);
		System.out.println(al);
	}
}
