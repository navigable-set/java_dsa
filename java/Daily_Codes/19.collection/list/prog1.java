//checking scenario : parent chya eka method la call kelay.. tithn dusrya method la call aahe..pn ti override zali aahe mhanun child chya method la call jaill!!
//LinkedList(c) class mdhe pn ticha parent class AbstractSequentialList class aahe jya mdhe iterator() la body aahe.. aani ti LinkedList mdhe override nahi kelia.. pn listIterator() hi method override keli aahe

class Parent{

	int run(){
		
		System.out.println("In parent run");
		return fun();
	}

	int fun(){
		
		System.out.println("In parent fun");
		return 1;
	}
}

class Child extends Parent{

	int fun(){
		
		System.out.println("In child fun");
		return 1;

	}

	public static void main(String[] shweta){
		
		Parent obj = new Child();
		System.out.println(obj.run());

		System.out.println("End main");
	}
}
