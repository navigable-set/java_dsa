//ArrayList
//generics (i.e. '< >') use krun note kadhu shakto.. pn generics mule same type of data/objects dyava lagel

import java.util.*;
class ArrayListDemo extends ArrayList{

	public static void main(String[] shweta){

		//removeRange() hi protected aahe mhanun aapn te extends krun child class cha object bnvlay...
		ArrayListDemo al = new ArrayListDemo();

		//boolean add(E)
		//System.out.println(al.add(10));	//true return krto
		al.add(10);
		al.add(20.5f);
		al.add("shashi");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);

		//add(int,E)
		al.add(3,"core2web");
		System.out.println(al);

		//int size()
		System.out.println(al.size());

		//boolean contains(Object)
		System.out.println(al.contains("shashi"));
		System.out.println(al.contains(30));

		//int indexOf(Object)
		System.out.println(al.indexOf(20.5f));
		
		//int lastIndexOf(Object)
		System.out.println(al.lastIndexOf(20.5f));

		//E get(int)
		System.out.println(al.get(3));

		//E set(int,E)	==> set hi replace sarkhi aahe
		//jo object replace honar aahe to return krte
		System.out.println(al.set(3,"Incubator"));

		System.out.println(al);

		ArrayList al2 = new ArrayList();
		al2.add("salman");
		al2.add("shahrukh");
		al2.add("aamir");

		System.out.println(al2);

		//boolean addAll(Collection)	==>last la add krte dusr complete collection
		al.addAll(al2);
		System.out.println(al);

		//boolean addAll(int,Collection)
		al.addAll(3,al2);
		System.out.println(al);
		
		//protected void removeRange(int,int)
		al.removeRange(3,5);	//range hi last number la '-1' aste range
		System.out.println(al);

		//E remove(int)
		System.out.println(al.remove(4));
		System.out.println(al);

		//Object[] toArray()	==>collection la array mdhe convet krto
		Object arr[] = al.toArray();
		//Integer arr[] = al.toArray(); //error: incompatible types: Object[] cannot be converted to Integer[]...parent to child convert nahi hot

		System.out.println(arr);
		for(Object data : arr){
			System.out.print(data + " ");
		}

		System.out.println();

		System.out.println(al);

		//void clear()	==>sgla data clear hoto i.e. collection mdhle sgle objects gayab/clear zalet
		al.clear();
		System.out.println(al);
		System.out.println(al2);

		al2.clear();
		System.out.println(al2 + " " +al);

		/*boolean removeAll(Object)	==>al2 aani al mdhe je common astil astil te sgle remove hotil tya collection mdhn jya vrn call kelia method
		System.out.println(al2.removeAll(al));
		System.out.println(al2);
		System.out.println(al2.retainAll(al));		//retain nahi krt aahe
		
		//System.out.println(al);
		System.out.println(al2);*/
		
	}
}
