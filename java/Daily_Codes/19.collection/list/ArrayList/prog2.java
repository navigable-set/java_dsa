//printing collection's object using 'for-loop' and 'for-each-loop'

import java.util.*;
class ArrayListDemo{

	public static void main(String [] shweta){

		ArrayList al = new ArrayList();

		al.add("shweta");
		al.add(10);
		al.add(21.0f);
		al.add(10);

		System.out.println(al);
		
		//for-each on collection directly
		
		for(Object obj : al){
                        System.out.print(obj + " ");
		}
                System.out.println();
		
		/* for-each la type identify krta yet nahi
		 * 'var' ha jya type cha data yeto..tya type cha bnto!!
		 * version-10 pasn he chalt...1.8 la error yeto

		for(var obj : al){
                        System.out.print(obj + " ");
		}
                System.out.println();
		*/
		
	       	//for loop on colletion using get(int) method
                for(int i=0; i<al.size(); i++){
                        System.out.print(al.get(i) + " ");
                }
                System.out.println();
//*******************************************************************************
		//Array==>for-each loop on array of collection of type Object
		
		Object arr[] = al.toArray();
		
		for(Object data : arr){
			System.out.print(data + " ");
		}

		System.out.println();
		
		//Array==>for-loop on array of collection of type Object
               	for(int i=0; i<al.size(); i++){
                        System.out.print(arr[i] + " ");
                }
		System.out.println();
	}
}
