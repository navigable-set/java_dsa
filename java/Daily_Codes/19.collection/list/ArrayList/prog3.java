//UserDefined classes in Collection ==> must Override 'toString()' method in userDefined classes

import java.util.*;

class CricPlayer{

	int jerNo = 0;
	String name = "Null";

	CricPlayer(int jerNo, String name){
		this.jerNo = jerNo;
		this.name = name;
	}

	/*
	public String toString(){
		return name;
	}*/
	
	//we can override toString() in any customised way..!

	public String toString(){
		return jerNo + " : " + name;
	}
}

class ArrayListDemo{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		//String type cha object aahe...String class chya toString() method la call jato
		al.add("shweta");

		//userDefined class cha object thevlay collection mdhe...mg jevha to collection mdhle object read krayla jato tevha to 'toString()' method la call krto.. jr aapn override nahi keli tr Object class chya toString() la call jail ji hashcode return krte!!

		al.add(new CricPlayer(7,"Dhoni"));
		al.add(new CricPlayer(18,"Virat"));
		al.add(new CricPlayer(45,"Rohit"));	

		//both will give same output!!
		System.out.println(al.toString());
		System.out.println(al);

		/*CricPlayer obj = al.get(1);		//error
		System.out.println(obj.toString());*/

		System.out.println(al.get(1).getClass().getName());
	}
}
