//add a userDefined classes objects randomly in a Collection

import java.util.*;

class SubjectToppers{

	String subName = null;
	String name = null;


	SubjectToppers(String subName,String name){
		
		this.subName = subName;
		this.name = name;
	}

	public String toString(){
		
		return subName + " : " + name;
	}
}

class Result{

	float cgpa = 0.0f;
	String name = null;

	Result(float cgpa, String name){
		
		this.cgpa = cgpa;
		this.name = name;
	}

	public String toString(){

		return name + " : " + cgpa;
	}
}

class ArrayListDemo{

	public static void main(String[] shweta){

		//'al' is collection of predefined and user-defined type of objects
		ArrayList al = new ArrayList();
		
		//String type of object
		al.add("Examination");
		//Result type of object
		al.add(new Result(7.5f,"aakash"));
		al.add(new Result(7.8f,"Shweta"));
		al.add(new Result(8.3f,"prachi"));
		//SubjectToppers type of object
		al.add(new SubjectToppers("WT","Radhika"));
		al.add(new SubjectToppers("AI","Mansi"));
		al.add(new SubjectToppers("DSBDA","Atharv"));

		System.out.println(al);

		//Result type of object
		al.add(3,new Result(8.2f,"Abhishek"));
		//String type of object
		al.add(5,"Toppers");
		
		System.out.println(al);
	}
}
