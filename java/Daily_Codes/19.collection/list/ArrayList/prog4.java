//UserDefined classes in Collection ==> must Override 'toString()' method in userDefined classes

import java.util.*;

class CricPlayer {

	int jerNo = 0;
	String name = "Null";

	CricPlayer(int jerNo, String name){
		this.jerNo = jerNo;
		this.name = name;
	}

	public String toString(){
		return jerNo + " : " + name;
	}
}

class ArrayListDemo{

	int x = 10;
	public static void main(String[] shweta){

		//int x =10; ==> jr x main() mdhe lihila tr to local instance variable zala..tyala object mdhe jaga nahi milnar...mhanun object through access nahi krta yenar.. cannot find symbol yeil

		ArrayList al = new ArrayList();

		al.add("shweta");

		al.add(new CricPlayer(7,"Dhoni"));
		al.add(new CricPlayer(18,"Virat"));
		al.add(new CricPlayer(45,"Rohit"));	

		System.out.println(al);

		//same hashcode
		ArrayListDemo obj1 = new ArrayListDemo();
		ArrayListDemo obj2 = obj1;

		System.out.println(System.identityHashCode(obj1));
		System.out.println(System.identityHashCode(obj2));


		System.out.println(obj1.x);		
		System.out.println(obj2.x);		

		obj1.x = 50;
		
		System.out.println(obj1.x);	//50	
		System.out.println(obj2.x);	//50	
		
//		CricPlayer obj = al.get(1);		==> error #### not getting it
//		System.out.println(al.get(1).toString());
		System.out.println(al.get(1).getClass().getName());
	}
}
