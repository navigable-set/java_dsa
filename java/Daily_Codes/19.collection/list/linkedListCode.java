//LInkedList has many methods but aapn kahi kahi ch bghnar aahot

import java.util.*;

class LinkedListDemo{

	public static void main(String[] shweta){

		LinkedList ll = new LinkedList();

		ll.add(10);
		ll.addFirst(20);
		ll.addLast(20);

		System.out.println(ll);

		ll.add(2,25);
		
		System.out.println(ll);
		
		//first object return krte
		System.out.println(ll.element());
		
		//object remove krte
		//E remove(int) || boolean remove(Object)
		System.out.println(ll.remove(0));

		//remove(Object) => Object type mjanje aapla collection mdhla object dyava lagel... E asla ki direct element i.e. name deta yet
		System.out.println(ll.remove(ll.get(0)));
		System.out.println(ll);
		
		System.out.println(ll.element());
		
		System.out.println(ll);

		//first element remove krt
		ll.removeFirst();
		
		System.out.println(ll);

		ll.add(0,10);
		System.out.println(ll);

		//colelction add kruya
		ArrayList al = new ArrayList();
		al.add("shweta");
		al.add("darshan");
		al.add("tejashree");

		System.out.println(al);

		ll.addAll(1,al);
		System.out.println(ll);
		
		System.out.println(ll.pop());
		System.out.println(ll);
	}
}
