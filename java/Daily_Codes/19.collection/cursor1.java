//Iterator (Interface) ==> universal cursor

import java.util.*;

class IteratorDemo{

	public static void main(String[] shweta){

		ArrayList al = new ArrayList();

		al.add("Ashish");
		//al.add("Kanha");
		al.add("Rahul");
		al.add("Kanha");
		al.add("Badhe");

		System.out.println(al);

		Iterator itr = al.iterator();

		while(itr.hasNext()){

			if("Rahul".equals(itr.next()))		
				itr.remove();	

//Rahul aani Badhe... aas output yeil..next() mhatl ki te pudhech jato aani to object return krto(current)... ethe 2-da next() hoty..mhanun skip hoil
			System.out.println(itr.next());	
		}	

		System.out.println(al);
	}
}
