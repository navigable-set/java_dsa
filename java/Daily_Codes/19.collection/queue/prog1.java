//ArrayDeque(c) child of Deque

package util;

import java.util.*;
class ArrayDequeDemo{

	public static void main(String[] shweta){
	
		Deque ad = new ArrayDeque();

		ad.add(20);
		ad.offer(10);
		ad.offer(30);
		ad.offer(8);
		ad.offer(50);
		ad.offer(10);

		System.out.println(ad);

		//public boolean offerFirst(E);
		ad.offerFirst(3);
		System.out.println(ad);
  		//public bolean offerLast(E);
		ad.offerLast(80);
		System.out.println(ad);

		//static final int inc(int, int);
		System.out.println(ArrayDeque.inc(2,5));
	}
}

/*
public boolean offerFirst(E);
  public boolean offerLast(E);
  public E removeFirst();
  public E removeLast();
  public E pollFirst();
  public E pollLast();
  public E getFirst();
  public E getLast();
  public E peekFirst();
  public E peekLast();
*/
/*
static final int inc(int, int);
static final int inc(int, int);
  static final int dec(int, int);
  static final int inc(int, int, int);
  static final int sub(int, int, int);
  static final <E> E elementAt(java.lang.Object[], int);
  static final <E> E nonNullElementAt(java.lang.Object[], int);
*/
