/*
 * switch ==> 	switch is statement
 * 		swich has cases..where we check if given constant value matches with any case (labels)or not
 * 		default statement is also given (not compulsion)
 * 		cases are separated by break statement(without break the correct case will consider all other cases below and will print them too)
 * 		cases strictly need constant values/expressein and not any variable
 * 		break is applicable on switch and loops only
 * 		duplicate cases not allowed
 * 		from 1.7 --> string and enum(which were not supported till 1.6 version) also allowed along with int/float/char/byte/short in switch(_)
 */
 

class SwitchDemo{

	public static void main(String [] shweta){

		int ch = 5;

		System.out.println("Before switch!");

		switch (ch){

			case 1 : System.out.println("one");
				 break;
			
			case 2 : System.out.println("two");
				 break; 

			case 3 : System.out.println("three");
				 break; 

			case 4 : System.out.println("four");
				 break; 

			case 5 : System.out.println("five");
				 break; 

			default : System.out.println("not between 1-5");
				 break; 
		}
		System.out.println("After switch!");
	}
}
