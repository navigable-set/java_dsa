/* logical operator : &&(logical AND) and ||(logical OR)
                      JAVA mdhe logical operator boolean mhanun use krto
*/

class Logical{

	public static void main(String [] args){

		int x = 5;
		int y = 7;

		//int ans = x && y;		error : bad operand
		//int ans1 = x<y && y>x;	error : incompatible types ..boolean can not converted to int

		boolean ans1 = x<y && y<x;
		boolean ans2 = x<y || y<x;

		System.out.println(ans1);
		System.out.println(ans2);
	}
}
