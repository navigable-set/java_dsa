/*try - catch - finally
 * finally => is used to close the connections
 * finally gets executed always whether exception occurs or not*/

import java.io.*;
class Demo{

	void m1(){
		System.out.println("In m1");
	}

	void m2(){
		System.out.println("In m2");
	}

	public static void main(String[] shweta){

		System.out.println("Start main");

		Demo obj = new Demo();
		obj.m1();

		obj = null;

		try{
			obj.m2();
		}catch(NullPointerException e){
			System.out.println("Exception catched");
		}finally{
			System.out.println("Connection closed");
		}
		
		System.out.println("End main");
		
	}
}
