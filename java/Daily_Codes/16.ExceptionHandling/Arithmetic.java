/*ArithmeticException = he RuntimeException aahe... lang package mdhe ch aahe ha class*/

class ArithmeticDemo{

	void m2(){

		System.out.println("In m2");
	}

	void m1(){

		System.out.println("In m1");
		System.out.println(10/0);
		m2();
	}
	public static void main(String [] shweta){

		System.out.println("start main");

		ArithmeticDemo obj = new ArithmeticDemo();
		obj.m1();

		System.out.println("End main");
	}
}
/*
Output :
start main
In m1
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at ArithmeticDemo.m1(Arithmetic.java:13)
	at ArithmeticDemo.main(Arithmetic.java:21)
*/
