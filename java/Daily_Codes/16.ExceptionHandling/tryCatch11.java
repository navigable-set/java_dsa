/*aapn try catch use krun pn Default Exception Handler sarkh print kru shakto... DEH he internally printStackTrace() method la ch call krt...
 * 3 methods aahet => 
 * 1. printStackTrace() = thread+Exception+Discription+stackTrace sangt
 * 2. getMessage() = Discription sangt
 * 3. toString() = threadName aani Discription sangte*/

import java.io.*;
class Demo{

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter data :");

		try{
			System.out.println(br.readLine());
			br.close();
			System.out.println(br.readLine());

		}catch(IOException obj){

			System.out.print("Exception in Thread " + Thread.currentThread().getName() + " ");
			obj.printStackTrace();
/*
			System.out.println(obj.getMessage());	//Discription sangte
			System.out.println(obj.toString());	//Exception name aani Discription sangte
			//System.out.println(obj);	//internall call to 'toString()' =>Exception name aani Discription sangte
*/
		}

		System.out.println("End main");
	}
}
