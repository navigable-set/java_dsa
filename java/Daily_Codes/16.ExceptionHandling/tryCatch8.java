/*Nested catch chlt..pn same same exception nahi chalt
 * finally ha ekdacch lihita yeto eka try sathi.. jshe multiple catch lihito tshe multiple finally nahi chalty... ekach ch try sathi*/

import java.io.*;
class Demo{

	public static void main(String[] shweta){

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter data :");

		int x = 0;

		try{
			x = Integer.parseInt(br.readLine());

		}catch(IOException obj1){

			System.out.println("IOException occured");

		}catch(NumberFormatException obj2){
			
			System.out.println("NumberFormatException occured");

		}catch(Exception obj3){
			
			System.out.println("Exception Handled");
		
	/*	}catch(Exception obj){
		
		************after compilation **********
		tryCatch8.java:29: error: exception Exception has already been caught
		}catch(Exception obj){
		 ^
		1 error
	
	*/
		}finally{
			System.out.println("Connections closed");	
		}/*finally{	
			System.out.println("Connections closed again");	
		}
		
		************after compilation **********
		tryCatch8.java:31: error: 'finally' without 'try'
		}finally{
		 ^
		1 error
		*/
	}
}
