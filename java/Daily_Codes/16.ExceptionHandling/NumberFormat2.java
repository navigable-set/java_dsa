/* IOException is thrown and Numberformat is handled in try-catch*/

import java.io.*;
class Demo{

	void m1()throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("In m1");
		
		int x =0;
		
		try{
			x = Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){
			System.out.println("In catch");
		}

		System.out.println("x = : " + x);
	}
	public static void main(String [] shweta) throws IOException{

		System.out.println("In main");
		Demo obj = new Demo();
		obj.m1();	
	}
}
