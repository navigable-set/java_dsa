/*NullPointerException = is RuntimeException*/

class Demo{

	void m1(){
		System.out.println("In m1");
	}
	
	void m2(){
		System.out.println("In m2");
	}

	public static void main(String[] shweta){

		System.out.println("In main");

		Demo obj = new Demo();

		obj.m1();

		obj = null;

		obj.m2();

		System.out.println("End main");
	}
}

/*
Output :
In main
In m1
Exception in thread "main" java.lang.NullPointerException
	at Demo.main(Nullpointer.java:23)
*/
