/*eg. NumberFormatException*/

import java.io.*;

class Demo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("In main");

		String str = br.readLine();
		System.out.println(str);

		int num = 0;
		try{
			num = Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){
			System.out.println("Please enter integer data");
			num = Integer.parseInt(br.readLine());
		}
		System.out.println(num);
	}
}


/*
Output :
In main
shweta
shweta
bagul
Please enter integer data	//exception handled
7
7
*/
