/*IO and Numberformat both in try-catch*/

import java.io.*;
class Demo{

	void m1(){

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("In m1");
		
		int x =0;
		
		try{
			System.out.println("Enter data :");
			x = Integer.parseInt(br.readLine());
		}catch(IOException obj1){
			System.out.println("IO-Exception catch block");
		}catch(NumberFormatException obj2){
			System.out.println("Number-format catch block");
		}

		System.out.println("x = : " + x);
	}

	public static void main(String [] shweta){

		System.out.println("In main");
		
		Demo obj = new Demo();
		obj.m1();

		System.out.println("In main");
	}
}
