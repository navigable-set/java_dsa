/*Realtime example (mine) of exception handling : phonePay app mdhe insufficient balance exception yet*/

import java.util.*;

class AccountBalanceException extends RuntimeException {

	AccountBalanceException(String msg){
		super(msg);
	}
}

class Client{

	public static void main(String[] shweta){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter the amount :");
		int amout = sc.nextInt();

		int acc_balance = 1000;

		try {
			if(amout > acc_balance){
				throw new AccountBalanceException("Inssuficient Account balance .");
			}else{
				System.out.println("paid successfully!!");
			}
		}catch(AccountBalanceException e){
			System.out.println(e.getMessage());
		}
	}
}
