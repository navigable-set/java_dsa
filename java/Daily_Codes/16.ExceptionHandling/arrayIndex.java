/*ArrayIndexOutOfBounds = is a Runtime Exception*/


class Demo{

	public static void main(String [] shweta){

		System.out.println("Start main");
		
		int arr[] = new int[] {10,20,30,40,50};

		for(int i=0; i<=arr.length; i++){
			System.out.println(arr[i]);
		}

		System.out.println("End main");
	}
}
/*
Output :
Start main
10
20
30
40
50
Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: 5
	at Demo.main(arrayIndex.java:13)
*/
