/* union catch : we can writr/handle multiple exception in one catch only by using OR i.e.( | )
 * but there should not be parent child exception classes in same catch block... it will raise an error of subclass*/

class Demo{

	public static void main(String [] shweta){

		try{
			System.out.println(10/0);

		}catch(NullPointerException | ArithmeticException | NumberFormatException obj){
			
			System.out.println("Arithmetic Exception handled");
		}

		System.out.println("End main");
	}
}
