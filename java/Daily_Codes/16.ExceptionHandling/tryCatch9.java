/*Nested try-catch
 * can write try-catch wirhin try/catch and also finally*/

import java.util.*;
class Demo{

	public static void main(String [] shweta){

		Scanner sc = new Scanner(System.in);

		int x = 0;

		try{
			System.out.println(10/0);
			try{
				System.out.println("In inner try");

			}catch(ArithmeticException obj1){
				
				System.out.println("In inner catch");
			}

		}catch(ArithmeticException obj1){

			System.out.println("In Outer catch");
			try {
				System.out.println("Enter data :");
				x = sc.nextInt();

			}catch(InputMismatchException obj2){

				System.out.println("Outer catch - InputMismatchException handled");
			}

		}catch(Exception obj2){

			System.out.println("Exception handled");
		}finally{

			System.out.println("Connections closed");
		}
			
		System.out.println("x = " + x);
	}
}
