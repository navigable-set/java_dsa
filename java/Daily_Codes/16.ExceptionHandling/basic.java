//'throw' konihi kru shakt.. jr eka method ne exception handle nahi kely tr tya method la call krnarya method ne tri te handle kel pahhije nahitr abnormal termination hot aani default exception handler te handle krt... pn hech jr 'throws' asel tr te sglya method ne throw kelch pahije... pn 'throw' ch thod vegl aahe!!
//throw he mage fekt jat to object... mg jr catch nsel kel tr abnormal termination otherwise normal.. pop hot jatat mage yatanna stack frame jr catch nsel kel tr

class Demo{

        static void run(){
                System.out.println("start run");
                throw new ArithmeticException("exception handled in main");
        }

        static void gun(){
                System.out.println("start gun");
                run();
                System.out.println("end gun");

        }

        public static void main(String[] shweta){

                System.out.println("start main");
                try{
                        gun();
                }catch(ArithmeticException ae){
                        System.out.println(ae);	//internally call to => 'toString()'
                }
                System.out.println("end main");
        }
}
