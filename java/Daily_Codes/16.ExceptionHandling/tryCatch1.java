/*try ha ekta lihita yet nahi..tya sobt catch/finally lagtoch..tsch catch aani finally ekt nahi yet..tr tyanchya sobt try pahilech/lagtoch
 * try -> risky code
 * catch -> handling code/handler code
 * finally -> connection close krnya sathi use kel jat
 * exception yeo/nko yeo or catch mdhe nahi gel..or exception he catch mdhe handle nahi kel tri finally ha execute honarch aahe
 * catch = hi special method sarkhi aahe..tyala jaga exception table mdhe milte.. method teble mdhe nahi.. catch sathi pn stack vagaira jate*/

/*eg. Arithmetic Exception*/

import java.util.*;
class Demo{

	public static void main(String [] shweta){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number :");
		int x = sc.nextInt();

		try{
			System.out.println(10/x);	//jr 'x' chi value zero aali tr ethe exception yeil!!
		}catch(ArithmeticException obj){
			System.out.println("Exception Occured..!!");
		}

		System.out.println("End main");
	}
}


/*
Output :
Enter number :
0
Exception Occured..!!
End main
*/
