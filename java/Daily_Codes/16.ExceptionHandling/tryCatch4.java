/*Nested catch block => parent aadhi aani nante child(exact matching) asel tr te error deil.. pn aadhi child(exact matching) aani nantr parent asel tr chalt
 * nested catch mdhe..eka try sathi brech catch pn chaltat or nested try-chjatch pn ast(switch sarkh working ast..jithe match zal te execute hoil)
 * extra che checked-exception(compile time) nahi lihayche karn mg compiler error deil pn extra che checked exception lihile tri compiler la kahi frk pdt nahi..error nahi det*/


class Demo{

	public static void main(String [] args){

		System.out.println("Start main");

		try{
			System.out.println(10/0);
		}catch(ArithmeticException obj1){
			System.out.println("Exception handled in arithmetic catch");
		}catch(RuntimeException obj2){
			System.out.println("Exception handled in Runtime catch");
		}

		System.out.println("End main");
	}
}

/*
Output :
Start main
Exception handled in arithmetic catch
End main
*/
