/*throw he exception handle nahi krt tr fkt customized message gheun janya ch kam krty.. mg tech try-catch chya help ne aapn handling code lihu shakto(catch mdhe) or predefined methods use krun tyanchya sarkh exception display kru shakto.. */

import java.io.*;
import java.util.*;

class DataOverFlowException extends RuntimeException{

	DataOverFlowException(String msg){
		super(msg);			
	}
}

class DataUnderFlowException extends RuntimeException{

	DataUnderFlowException(String msg){
		super(msg);
	}
}

class Client{

	public static void main(String [] shweta){

		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array elments :");
		System.out.println("Note : enter elements within 1 - 100");

		for(int i=0; i<arr.length; i++){
			int data = sc.nextInt();

			try{
				if(data > 100){
					throw new DataOverFlowException("data is greater than 100");
				}else if(data < 0){
					throw new DataUnderFlowException("data is less than zero");
				}else{
					arr[i] = data;
				}

			}catch(DataOverFlowException | DataUnderFlowException obj){
				obj.printStackTrace();
			}
		}

		for(int i=0; i<arr.length; i++){

			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
