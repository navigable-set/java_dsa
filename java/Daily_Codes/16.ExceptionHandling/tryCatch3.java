/*e.g InterruptedException*/

class Demo{

	public static void main(String [] shweta){

		System.out.println("Start main");

		for(int i=0; i<5; i++){
			System.out.println("In loop");

			try{
				Thread.sleep(5000);		//throw pn kru shakto karn te compile time exception aahe
			}catch(InterruptedException e){
				System.out.println("Exception handled");
			}
		}

		System.out.println("End main");
	}
}

/*
Output :
Start main
In loop
In loop
In loop
In loop
In loop
End main
*/
