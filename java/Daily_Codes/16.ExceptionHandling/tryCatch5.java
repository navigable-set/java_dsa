/*Nested catch = parentr aadhi nantr child -> error pn aadhi child aani nantr parent chalel*/

import java.io.*;
class Demo{

	public static void main(String [] shweta){

		System.out.println("Start main");

		try{
			System.out.println(10/0);
		}catch(RuntimeException obj1){
			System.out.println("Exception handled in Runtime");
		}catch(ArithmeticException obj2){
			System.out.println("Exception handled in IOException");
		}

		System.out.println("End main");
	}
}


/*
tryCatch5.java:14: error: exception ArithmeticException has already been caught
		}catch(ArithmeticException obj2){
		 ^
1 error
*/
