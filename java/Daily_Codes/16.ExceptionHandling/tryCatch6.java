/*extra che checked exception nahi lihayche otherwise error yeto..pn extra che unchecked exception lihile tri kahi frk pdt nahi karn te compiler bghnar ch nahia.. te mhant ki runtime la aas kahi hou shakt ki te exception yeil*/

import java.io.*;
class Demo{

	public static void main(String [] shweta){

		System.out.println("Start main");

		try{
			System.out.println(10/0);
		}catch(NullPointerException obj1){	//Runtime
			System.out.println("NullPointerException is handled");
		}catch(ArithmeticException obj2){	//Runtime
			System.out.println("ArithmeticException is handled");
		}catch(IOException obj3){	
			//(compiletime)IOException he try mdhe occur ch honar nahia..mg ugach ka lihilyy..mhanun compiler error deto!!
			System.out.println("IOException is handled");
		}

		System.out.println("End main");
	}
}

/*
tryCatch6.java:16: error: exception IOException is never thrown in body of corresponding try statement
		}catch(IOException obj3){
		 ^
1 error
*/
