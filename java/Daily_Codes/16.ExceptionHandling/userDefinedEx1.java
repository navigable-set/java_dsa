/*throw use krun aapn user-defined exception lihu shakto... jr aaplyala mahitia ki exception yenar aahe mg te aapn ch manually handle krto.. throw use krun jo msg dyaycha to pass krto.. pn aapla class(je user defined exception asnar aahe) ha throwable chya hierarchy mdhla hva*/


import java.io.*;
import java.util.*;

class DataOverFlowException extends IOException{

	DataOverFlowException(String msg){
		super(msg);			//super(parent constructor la call) kelya mule to msg Throwable pryant jato(throwable chya constructor mdhe ek method call aahe).. aani tyachya methods mg exception with msg print krun detil!!
	}
}

class DataUnderFlowException extends IOException{

	DataUnderFlowException(String msg){
		super(msg);
	}
}

class Client{

	//aapn extends IOException kely...je checked exception aahe... so aaplya methods ne pn throw kel pahije!! nahi tr compiler error deil
	public static void main(String [] shweta) throws DataUnderFlowException,DataOverFlowException{

		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array elments :");
		System.out.println("Note : enter elements within 1 - 100");

		for(int i=0; i<arr.length; i++){
			int data = sc.nextInt();

			if(data < 0) 
				throw new DataUnderFlowException("data is less than zero");	//ethe aapn exception he handle nahi kelet try-catch mdhe.. tr fkt mage mage to msg send ketoy(super())...jvm cha default exception handler ch bghnar aahe jr exception aal tr(abnormal termination) pn jr aaplyala handle kraych asel tr try-catch use kru shakto.. next example mdhe try-catch use kely!!

			if(data > 100)
				throw new DataOverFlowException("data is greater than 100");

			arr[i] = data;
		}

		for(int i=0; i<arr.length; i++){

			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}

/*
Enter array elments :
Note : enter elements within 1 - 100
1
2
155
Exception in thread "main" DataOverFlowException: data is greater than 100
	at Client.main(userDefinedEx1.java:38)
*/
