/*NumberFormatException = is RuntimeException .. 'BufferedReader' asel tr he exception yet jr String la aapn interger vagaira mdhe parse krnya cha try kel tr*/

import java.io.*;
class Demo{

	void m1() throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("In m1");

		System.out.println("Enter data");
		
		int num = Integer.parseInt(br.readLine());

		System.out.println(num);
	}

	public static void main(String [] shweta) throws IOException{
		
		System.out.println("Start main");

		demo obj = new demo();
		obj.m1();

		System.out.println("End main");

	}
}

/*
Output :

Start main
In m1
Enter data
wd
Exception in thread "main" java.lang.NumberFormatException: For input string: "wd"
	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.lang.Integer.parseInt(Integer.java:580)
	at java.lang.Integer.parseInt(Integer.java:615)
	at demo.m1(NumberFormat.java:14)
	at Demo.main(NumberFormat1.java:24)
*/
