/*
A b C d
E f G h
I j K l
M n O p
*/

class Demo{

	public static void main(String [] shweta){

		char ch1 = 'A';
		char ch2 = 'a';
		int n = 4;

		for(int i=1; i<= n; i++){

			for(int j=1; j<=n; j++){

				if(j%2 != 0){
					System.out.print(ch1 + " ");
				}else{
					System.out.print(ch2 + " ");
			        }
				ch1++;
				ch2++;
			}
			System.out.println();
		}
	}
}
