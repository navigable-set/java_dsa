/*
10
I	H
7	6	5
D	C	B	A
*/

class Demo{

	public static void main(String[] shweta){

		int row = 4;
		int val = 10;
		char ch = 'J';

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){

				if(i%2 != 0)
					System.out.print(val + "\t");
				else
					System.out.print(ch + "\t");

				ch--;
				val--;
			}
			System.out.println();
		}
	}
}
