/*
1 2 3 4
2 3 4
3 4
4
*/

class Demo{

	public static void main(String[] shweta){

		int row =4;

		for(int i=1; i<=row; i++){
			int num =i;
			for(int j=1; j<=row-i+1; j++){
				System.out.print(num++ + "\t");	
			}
			System.out.println();	
		}
	}
}
