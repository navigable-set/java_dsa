/*
10
10 9
9 8 7
7 6 5 4
*/

class Demo{

	public static void main(String[] shweta){


		int num =10;
		int row =4;

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				System.out.print(num-- + "\t");
			}
			num++;
			System.out.println();
		}
	}
}
