/*
F
E 1
D 2 E
C 3 D 4
B 5 C 6 D
A 7 B 8 C 9
*/

class Demo{

	public static void main(String [] shweta){

		int row =6;
		char ch =70;
		int num =1;

		for(int i=1; i<=row; i++){
			for(int j=1; j<=i; j++){
				if(j%2 !=0){
					System.out.print(ch++ + "\t");
				}else{
					System.out.print(num++ + "\t");
				}
			}
			ch = (char)(70-i);
			System.out.println();
		}
	}
}
