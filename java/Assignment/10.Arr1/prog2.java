/*WAP to take size of array from user and also take integer elements from user Print
product of even elements only
input : Enter size : 9
Enter array elements : 1 2 3 2 5 10 55 77 99
output : 40
// 2 * 2 * 10
*/

import java.io.*;
class ArrayDemo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("array is :");

		for(int i=0; i<size; i++){
			System.out.print(arr[i] +"\t");
		}
		System.out.println();

		int evenProdt = 1;

		for(int i=0; i<size; i++){
			if(arr[i]%2 == 0){
				evenProdt = evenProdt * arr[i];
			}
		}

		System.out.println("product of even elements is : " + evenProdt);
	}
}
