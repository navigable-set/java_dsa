/*WAP to take size of array from user and also take integer elements from user Print sum
of odd elements only
input : Enter size : 5
Enter array elements : 1 2 3 4 5
output : 9 (1 + 3 + 5)
*/

import java.io.*;
class ArrayDemo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("array is :");

		for(int i=0; i<size; i++){
			System.out.print(arr[i] +"\t");
		}
		System.out.println();

		int oddSum = 0;

		for(int i=0; i<size; i++){
			if(arr[i]%2 == 1){
				oddSum = oddSum + arr[i];
			}
		}

		System.out.println("sum of odd elements is : " + oddSum);
	}
}
