/*Write a program, take 7 characters as an input , Print only vowels from the array
Input: a b c o d p e
Output : a o e
*/

import java.io.*;
class ArrayDemo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("array is :");

		for(int i=0; i<size; i++){
			System.out.print(arr[i] +"\t");
		}
		System.out.println();
		System.out.println("Output :");

		for(int i=0; i<size; i++){
			if(arr[i]%5 == 0){
				System.out.print(arr[i] + "\t");
			}
		}
		System.out.println();
	}
}
