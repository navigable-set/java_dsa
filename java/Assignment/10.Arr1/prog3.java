/*WAP to take size of array from user and also take integer elements from user Print
product of odd index only
input : Enter size : 6
Enter array elements : 1 2 3 4 5 6
output : 48
//2 * 4 * 6
*/

import java.io.*;
class ArrayDemo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("array is :");

		for(int i=0; i<size; i++){
			System.out.print(arr[i] +"\t");
		}
		System.out.println();

		int oddProdt = 1;

		for(int i=0; i<size; i++){
			if(i%2 == 1){
				oddProdt = oddProdt * arr[i];
			}
		}

		System.out.println("product of odd index's elements is : " + oddProdt);
	}
}
