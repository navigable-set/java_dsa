/* Calculate profit and loss*/

class Demo{

	public static void main(String [] args){

		int sp = 12000;
		int cp = 8000;

		if(sp > cp)
			System.out.println("profit of "+ (sp-cp));
		else if(cp > sp)
			System.out.println("loss of "+ (cp-sp));
		else
			System.out.println("No profit no loss");
	}
}
