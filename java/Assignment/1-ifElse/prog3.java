/* number is positive or negative*/

class Demo{

	public static void main(String[] args){

		int var = -4;

		if(var > 0)
			System.out.println(var + " is positive number");
		else if(var < 0)
			System.out.println(var + " is negative number");
		else
			System.out.println(var + " is neither positive nor negative");
	}
}
