/* if num = 0-5 ==> spelling
          > 5  ==> num is greater than 5
          = -ve ==> -ve number
*/

class Demo{

	public static void main(String[] shweta){

		int var = 5;

		if(var >= 0 && var <= 5){
			if(var == 0)
				System.out.println("Zero");
			if(var == 1)
				System.out.println("One");
			if(var == 2)
				System.out.println("Two");
			if(var == 3)
				System.out.println("Three");
			if(var == 4)
				System.out.println("Four");
			if(var == 5)
				System.out.println("Five");
		}else if(var > 5)
			System.out.println(var +" is greater than 5");
		else
			System.out.println(var + " is negative number");
	}
}
