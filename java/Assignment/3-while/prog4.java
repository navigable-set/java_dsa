/*Count the odd digits of given number*/

class Demo{

	public static void main(String [] shweta){

		int num = 942111423;
		int temp = num;
		int count = 0;

		while(num != 0){
			int rem = num %10;

			if(rem %2 != 0)
				count++;

			num = num/10;
		}

		System.out.println("count of odd digits is "+ count + " in "+ temp);
	}
}
