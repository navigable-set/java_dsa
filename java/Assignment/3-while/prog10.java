/*Check whether the number is palindrome number or not*/

class Demo{

	public static void main(String [] shweta){

		int num = 2332;
		int temp = num;
		int rev = 0;

		while(num!= 0){

			int rem = num %10;
			rev = (rev *10)+rem;
			num = num/10;
		}

		if(temp == rev)
			System.out.println(temp +" is a palindrome number");
		else
			System.out.println(temp +" is not a palindrome number");
	}
}
