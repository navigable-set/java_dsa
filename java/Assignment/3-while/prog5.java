/*square of a even digits of given number*/

class Demo{

	public static void main(String [] shweta){

		int num = 942111423;
		int temp = num;
		int sqr = 1;

		while(num != 0){
			int rem = num %10;

			if(rem %2 == 0)
				System.out.println(rem*rem);

			num = num/10;
		}
	}
}
