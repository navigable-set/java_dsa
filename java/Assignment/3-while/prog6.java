/*sum of all even numbers and multiplication of odd number between 1 -10*/

class Demo{

	public static void main(String [] shweta){

		int sum =0;
		int mult = 1;

		for(int i=1; i<= 10; i++){

			if(i%2 == 0)
				sum = sum + i;
			else
				mult = mult * i;
		}

		System.out.println("sum of even numbers between 1 to 10 = " + sum);
		System.out.println("multiplication of odd numbers between 1 to 10 = " + mult);
	}
}
