/*
 Use <<< GAUSS THEORAM  >>> for patterns similar to above one....
 
 	m = (n*(n+1))/2
 	
        m = ((5*(5+1))/2 = 30/2= 15 --->for 5 row--> 15 values
 					for 4 row--> 10 values
					for 3 row--> 6 values

***************************************************************

ROW =15

O
14	13
L	K	J
9	8	7	6
E	D	C	B	A

ROW = 4

10
I	H
7	6	5
D	C	B	A

*/

import java.io.*;
class Demo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter number of rows :");
		int row= Integer.parseInt(br.readLine());

		int val = (row *(row+1))/2;
		char ch = (char)(val +64);
		int rwval = row;	//to decide from where to start...wether from char or num..if rows are even..will start with integer and if rows are odd then will start with character


		if(row > 6){
			
			System.out.println("Invalide number of rows..!! for given pattern...rows should not be greater than 6");	//if rows are greater than 6 then char will go beyond 'Z' ..and will print non-printable symbols too...
		}else{
			for(int i=1; i<=row; i++){

				for(int j=1; j<=i; j++){
				
					if(rwval % 2 != 0)
						System.out.print(ch +"\t");
					else
						System.out.print(val +"\t");
			
					ch--;	
					val--;
				}
				rwval--;
				System.out.println();
			}
		}
	}
}
