/*
write a program to print a series of prime numbers from entered range. ( Take a start and end number
from a user )
Perform dry run at least from 10 to 20 …
Input:-
Enter starting number: 10
Enter ending number: 100
Output:-
Series = 11 13 17 19 ….. 89 97
*/

import java.io.*;
class Demo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter start and end value :");
		int start = Integer.parseInt(br.readLine());
		int end = Integer.parseInt(br.readLine());

	
		if(start < end){

			for(int i=start; i<=end; i++){
				int count =0;
				for(int j=1; j<=i; j++){
					if(i%j==0)
						count++;
				}
				if(count == 2)
					System.out.print(i + " ");
			}
		}else {
			int num=0;
			for(int i=end; i<=start; i++){
				int count =0;
				for(int j=1; j<=i; j++){
					if(i%j==0){
						num++;
						count++;
					}

				}
				if(count == 2)
					System.out.print(i + " ");
				if((start == end) && count >2)
					System.out.print(i +" is not a prime number");	//what if for diff. numbers are there but no prime no. is found!!
			}
		}
		System.out.println();
	}
}
