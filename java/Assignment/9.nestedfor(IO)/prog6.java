/*
Write a program, and take two characters if these characters are equal then print them as it is but if
they are unequal then print their difference.
{Note: Consider Positional Difference Not ASCIIs}
*/

import java.io.*;
class Demo{

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter any 2 characters :");
		char ch1 = (char)br.read();

		br.skip(1);
		char ch2 = (char)br.read();

		if(ch1 == ch2 || ch1 == ch2+32 || ch1 == ch2-32){
			System.out.println(ch1 +" is equal to " + ch2);
		}else{
			int count = 0;
			if((ch1 >= 65 && ch1<=90) && (ch2>=97 && ch2<=122)){
				if(ch1+32 > ch2 ){ 
					for(int i=ch2; i<ch1+32; i++){
						count++;
					}
				}else{
					for(int i=ch1+32; i<ch2; i++){
						count++;
					}
				}
			}else if((ch2 >= 65 && ch2<=90) && (ch1>=97 && ch1<=122)){
				if(ch2+32 > ch1 ){ 
					for(int i=ch1; i<ch2+32; i++){
						count++;
					}
				}else{
					for(int i=ch2+32; i<ch1; i++){
						count++;
					}
				}

			}else if(ch1 > ch2 ){ 
				for(int i=ch2; i<ch1; i++){
					count++;
				}
			}else{
				for(int i=ch1; i<ch2; i++){
					count++;
				}
			}

			System.out.println("The difference between "+ ch1 + "" +" and "+ ch2 +"" + " is "+ count);
		}
	}
}
