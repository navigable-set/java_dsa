/*
WAP to print all even numbers in reverse order and odd numbers in the standard way. Both separately.
Within a range. Take the start and end from user
Input: Enter start number - 2
Enter End number - 9
Output:
8642
3579
*/


import java.io.*;
class Demo{

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Start number :");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End number :");
		int end = Integer.parseInt(br.readLine());

		if(start >= end){
			if(start > end)
				System.out.println("wrong values for start and end as start cannot be greater than end value!!");
			else{
				if(start %2 ==0)
					System.out.println("start and end are same... " + start + " is even");
				else	
					System.out.println("start and end are same... " + start + " is odd");
			}

		}else{
			System.out.println("Even numbers between " + start + " " + end + " are :");
			for(int i = end; i>= start; i--){
	
				if(i %2 == 0)
					System.out.print(i + " ");
			}
			System.out.println();
			
			System.out.println("Odd numbers between " + start + " " + end + " are :");
			for(int i = start; i<= end; i++){

				if(i %2 != 0)
					System.out.print(i + " ");
			}
			System.out.println();
		}
	}
}
