/*
33] Multiply left and right array sum.
Pitsy needs help with the given task by her teacher. The task is to divide an array
into two sub-array (left and right) containing n/2 elements each and do the sum of
the subarrays and then multiply both the subarrays.
Note: If the length of the array is odd then the right half will contain one element
more than the left half.
Example 1:
Input : arr[ ] = {1, 2, 3, 4}
Output : 21
Explanation:
Sum up an array from index 0 to 1 = 3. Sum up an array from index 2 to 3 =
7. Their multiplication is 21.
Example 2:
Input : arr[ ] = {1, 2}
Output : 2
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ T ≤ 100
1 ≤ N ≤ 1000
1 ≤ A[i] ≤ 100
*/

import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
		
		int size1 = arr.length/2;
		int size2 = arr.length - size1;

		int leftArr[] = new int[size1];
		int rightArr[] = new int[size2];

		int j = 0;
		int k = 0;

		int lsum = 0;
		int rsum = 0;

		for(int i =0; i<arr.length; i++){

			if(i < leftArr.length){
				leftArr[j] = arr[i];
				lsum = lsum + leftArr[j];
				j++;
			}else{
				rightArr[k] = arr[i];
				rsum = rsum + rightArr[k];
				k++;
			}
		}

		int mul = lsum * rsum;

		System.out.println("Output : " + mul);
	}
}
