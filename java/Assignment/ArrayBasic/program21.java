/*
21] First element to occur k times
Given an array of N integers. Find the first element that occurs at least K number
of times.
Example 1:
Input :
N = 7, K = 2
A[] = {1, 7, 4, 3, 4, 8, 7}
Output :
4
Explanation:
Both 7 and 4 occur 2 times.
But 4 is first that occurs 2 times
As at index = 4, 4 has occurred
at least 2 times whereas at index = 6,
7 has occurred at least 2 times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10^4
1 <= K <= 100
1<= A[i] <= 200
*/

import java.io.*;
import java.util.*;

class ArrayDemo{

	int lastIndex(int arr[],int ele){

		int index = -1;

		for(int i =0; i<arr.length; i++){
			if(ele == arr[i]){
				index = i;
			}
		}
		return index;
	}

	int eleCount(int arr[], int x){

		int count = 0;

		for(int i=0; i<arr.length; i++){
			if(x == arr[i]){
				count++;
			}
		}

		return count;
	}

        public static void main(String[] shweta) throws IOException{

		ArrayDemo obj = new ArrayDemo();
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
	
		System.out.println("Enter key :");
		int key = Integer.parseInt(br.readLine());

		int flag = 0;
		TreeSet ts = new TreeSet();

		for(int i=0; i<arr.length; i++){
			if(key == obj.eleCount(arr,arr[i])){
				flag = 1;
				ts.add(arr[i]);
			}
		};

		if(flag == 1){
			int index = -1;
			int current = -1;
			int pre = -1;

			Iterator itr = ts.iterator();
			
			while(itr.hasNext()){
				
				current = obj.lastIndex(arr,(int)itr.next());
				
				if(current < pre || index == -1){
					index = current;
				}
				pre = current;
			}
			System.out.println("Output : " + arr[index]);

		}else{
			System.out.println("No element found for given key : " + key);

		}
	}
}
