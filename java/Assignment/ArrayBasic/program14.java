/*
14] Maximum repeating number
Given an array Arr of size N, the array contains numbers in range from 0 to K-1
where K is a positive integer and K <= N. Find the maximum repeating number in
this array. If there are two or more maximum repeating numbers return the element
having least value.
Example 1:
Input:
N = 4, K = 3
Arr[] = {2, 2, 1, 2}
Output: 2
Explanation: 2 is the most frequent element.
Example 2:
Input:
N = 6, K = 3
Arr[] = {2, 2, 1, 0, 0, 1}
Output: 0
Explanation: 0, 1 and 2 all have the same frequency of 2.But since 0 is
smallest, you need to return 0.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(K)
Constraints:
1 <= N <= 10^7
1 <= K <= N
0 <= Arri <= K - 1
*/


import java.io.*;

class RepeatingNumber{

	int countEle(int arr[], int ele){

		int count = 0;
		for(int i=0; i<arr.length; i++){

			if(ele == arr[i]){
				count++;
			}
		}
		return count;
	}

	int repeatingEle(int arr[]){

		int max = -1;
		
		for(int i=0; i<arr.length-1; i++){
			if(countEle(arr,arr[i]) < countEle(arr,arr[i+1])){
				max = i+1;
			}		
			if(countEle(arr,arr[i]) > countEle(arr,arr[i+1])){
				max = i;
			}
		}

		//some conditions are not satisfied... if 2 elements have same maximum count and other 2 have minimum.. then one element with maximum count but minimum value should be returned..but here..the element with maximum value(and maximum count of repeat) is returned
//e.g. 1,2,2,1,3,1,2,4 expected: 1 but observed: 2
		//if all elements have same count of maximum occurance...then return element with minimum value
		if(max == -1){
			int min = arr[0];
			for(int i=0; i<arr.length; i++){
				if(min > arr[i])
					min = arr[i];
			}
			return min;
		}

		return arr[max];
	}

	public static void main(String[] shweta) throws IOException{

		RepeatingNumber obj = new RepeatingNumber();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements between 0 to " + (size-1) + " :");
		int flag = 0;

		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] > size-1 || arr[i] < 0)
				flag = 1;
		}

		if(flag == 1){
			System.out.println("Array elements are out of given range");
		}else{
			int ret = obj.repeatingEle(arr);
			System.out.println("output : " + ret);
		}
	}
}
