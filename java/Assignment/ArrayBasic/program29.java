/*
29] Last index of One
Given a string S consisting only of '0's and '1's, find the last index of the '1' present
in it.
Example 1:
Input:
S = 00001
Output:
4
Explanation:
Last index of 1 in the given string is 4.
Example 2:
Input:
0
Output:
-1
Explanation:
Since, 1 is not present, so output is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= |S| <= 10^6
S = {0,1}
*/

import java.io.*;
class ArrayDemo{

        int lastIndex(char arr[]){

                int ret = -1;

                for(int i=0; i<arr.length; i++){

                        if(arr[i] == '1'){
                                ret = i;
                        }
                }
                return ret;
        }

        public static void main(String[] shweta) throws IOException{

                ArrayDemo obj = new ArrayDemo();

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter any String that contains zero and one only :");
                String str = br.readLine();

                char arr[] = str.toCharArray();

                int flag = 1;

                for(int i=0; i<arr.length; i++){
                        if(arr[i] != 48 && arr[i] != 49){
                                flag = 0;
                                break;
                        }
                }

                if(flag == 0){
                        System.out.println("given string is invalid as it is not a complete binary string!!");
                }else{
                        int ret = obj.lastIndex(arr);
                        System.out.println(ret);
                }
        }
}
