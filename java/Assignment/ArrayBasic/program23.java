/*
23] Find the smallest and second smallest element in an array
Given an array of integers, your task is to find the smallest and second smallest
element in the array. If smallest and second smallest do not exist, print -1.
Example 1:
Input :
5
24356
Output :
23
Explanation:
2 and 3 are respectively the smallest
and second smallest elements in the array.
Example 2:
Input :
6
121367
Output :
12
Explanation:
1 and 2 are respectively the smallest
and second smallest elements in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5
*/


import java.util.*;
import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

/*			
		//not correct

		int min = arr[0];
		int secMin = arr[0];

		for(int i =0; i<arr.length; i++){
			if(min > arr[i]){
				secMin = min;
				min = arr[i];
			}
		}
		
		if(secMin != min){
			System.out.println("smallest : " + min );
			System.out.println("second smallest : " + secMin );
		}else{
			System.out.println("-1");
		}
*/

		System.out.println("********");

		TreeSet ts = new TreeSet();

		for(int i=0; i<arr.length; i++){
			ts.add(arr[i]);
		}

		int tsSize = ts.size();
		int givenSize = 0;

		if(tsSize > 1){
			Iterator itr = ts.iterator();
	
			while(itr.hasNext()){
				givenSize++;

				if(givenSize <= 2)
					System.out.println(itr.next());
				else
					break;
			}
		}else{
			System.out.println("-1");
		}
	}
}
