/*
39] Leaders in an array
Write a program to print all the LEADERS in the array. An element is a leader if it
is greater than all the elements to its right side. And the rightmost element is
always a leader.
For example:
Input: arr[] = {16, 17, 4, 3, 5, 2},
Output : 17, 5, 2
Input: arr[] = {1, 2, 3, 4, 5, 2},
Output: 5, 2
*/

import java.io.*;

class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		System.out.println("Output :");
		for(int i=0; i<arr.length-1; i++){
			
			int flag = 1;

			for(int j=i ; j<arr.length; j++){

				if(arr[i] < arr[j]){
					flag = 0;
					break;
				}
			}

			if(flag == 1)
				System.out.println(arr[i]);
		}

		System.out.println(arr[arr.length-1]);
	}
}
