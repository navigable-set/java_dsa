/*
28] Remove Duplicates from unsorted array
Given an array of integers which may or may not contain duplicate elements. Your
task is to remove duplicate elements, if present.
Example 1:
Input:
N =6
A[] = {1, 2, 3, 1, 4, 2}
Output:
1234
Example 2:
Input:
N =4
A[] = {1, 2, 3, 4}
Output:
1234
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1<=N<=10^5
1<=A[i]<=10^5
*/

import java.util.*;
import java.io.*;

class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
		
		System.out.println("**********");

		//using collection
		LinkedHashSet ls = new LinkedHashSet();

		for(int i =0; i<arr.length; i++){
			ls.add(arr[i]);
		}

		Iterator itr = ls.iterator();

		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}
}
