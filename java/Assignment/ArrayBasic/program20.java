/*
20] Check if pair with given Sum exists in Array (Two Sum)
Given an array A[] of n numbers and another number x, the task is to check
whether or not there exist two elements in A[] whose sum is exactly x.
Examples:
Input: arr[] = {0, -1, 2, -3, 1}, x= -2
Output: Yes
Explanation: If we calculate the sum of the output,1 + (-3) = -2
Input: arr[] = {1, -2, 1, 0, 5}, x = 0
Output: No
*/

import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
		
		System.out.println("Enter sum :");
		int sum = Integer.parseInt(br.readLine());

		int flag = 0;
		int val = 0;

		for(int i=0; i<arr.length-1; i++){

			for(int j=i+1; j<arr.length; j++){

				val = arr[i] + arr[j];
				if(val == sum){
					flag = 1;
					System.out.println("Yes");
					break;
				}
			}
			if(flag == 1)
				break;
		}

		if(flag == 0)
			System.out.println("No");
	}
}
