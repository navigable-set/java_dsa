/*
43] Count number of elements between two given elements in
array
Given an unsorted array and two elements num1 and num2. The task is to count the
number of elements occurring between the given elements (excluding num1 and
num2). If there are multiple occurrences of num1 and num2, we need to consider
the leftmost occurrence of num1 and rightmost occurrence of num2.
Example 1:
Input : Arr[] = {4, 2, 1, 10, 6}
num1 = 4 and num2 = 6
Output : 3
Explanation:
We have an array [4, 2, 1, 10, 6] and
num1 = 4 and num2 = 6.
So, the leftmost index of num1 is 0 and the rightmost index of num2 is 4.
So, the total number of elements between them is [2, 1, 10] So, the function
will return 3 as an answer.
Example 2:
Input : Arr[] = {3, 2, 1, 4}
num1 = 2 and num2 = 4
Output : 1
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
2 ≤ N ≤ 10^5
1 ≤ A[i], num1, num2 ≤ 10^5
*/

import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("Enter number1 and number2 : ");

                int num1 = Integer.parseInt(br.readLine());
                int num2 = Integer.parseInt(br.readLine());

                int count = 0;

                //rightmost num1
                for(int i=0; i<arr.length; i++){

                        if(num1 == arr[i]){
                                num1 = i;
                                break;
                        }
                }

                //leftmost num2
                int index = -1;
                for(int i=0; i<arr.length; i++){

                        if(num2 == arr[i]){
                                index = i;
                        }
                }
                num2 = index;

                if(num1 < num2){
                        for(int i = num1+1 ; i < num2; i++){
                                count++;
                        }
                }else{
                        for(int i = num2+1 ; i < num1; i++){
                                count++;
                        }
                }

                System.out.println("count : " + count);
        }
}
