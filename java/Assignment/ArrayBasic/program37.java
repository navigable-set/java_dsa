/*
37] Move all negative numbers to beginning and positive to end
with constant extra space
An array contains both positive and negative numbers in random order. Rearrange
the array elements so that all negative numbers appear before all positive numbers.
Examples :
Input: -12, 11, -13, -5, 6, -7, 5, -3, -6
Output: -12 -13 -5 -7 -3 -6 11 6 5
*/

import java.util.*;
import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		//array ne pn kru shakto..negative aani positive cha count ghyava lagel..mg size deta yeil
		ArrayList negativeal = new ArrayList();
		ArrayList positiveal = new ArrayList();

		for(int i=0; i<arr.length; i++){
			
			if(arr[i] < 0){
				negativeal.add(arr[i]);
			}else{
				positiveal.add(arr[i]);
			}
		}

		Iterator itr = negativeal.iterator();

		int i = 0;

		while(itr.hasNext()){
			arr[i] = (int)itr.next();
			i++;
		}

		itr = positiveal.iterator();
		
		while(itr.hasNext()){
			arr[i] = (int)itr.next();
			i++;
		}

		for(i=0; i<arr.length; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
