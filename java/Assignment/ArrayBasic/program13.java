/*
13] Find unique element
Given an array of size n which contains all elements occurring in multiples of K,
except one element which doesn't occur in multiple of K. Find that unique element.
Example 1:
Input :
n = 7, k = 3
arr[] = {6, 2, 5, 2, 2, 6, 6}
Output :
5
Explanation:
Every element appears 3 times except 5.
Example 2:
Input :
n = 5, k = 4
arr[] = {2, 2, 2, 10, 2}
Output :
10
Explanation:
Every element appears 4 times except 10.
Expected Time Complexity: O(N. Log(A[i]) )
Expected Auxiliary Space: O( Log(A[i]) )
Constraints:
3<= N<=2*10^5
2<= K<=2*10^5
1<= A[i]<=10^9
*/

import java.util.*;
import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }


		//calculation count for each element.. and placing them in new array sequentially
		int arr2[] = new int[size];

		for(int i=0; i<arr.length; i++){
		
			int count = 0;
			
			for(int j =0; j<arr.length; j++){
				if(arr[i] == arr[j])
					count++;
			}

			arr2[i] = count;
		}

		int var = -1;
/*
 		//not correctly working
		for(int i=1; i<arr2.length; i++){
				
			if(arr2[i-1]%i != arr2[i]%i){
				var = i;
				if(i == 0 || i == arr2.length-1)
					var = i-1;
			}
		}

*/
		//checking whether which count is different
		for(int i=0; i<arr2.length-1; i++){
			if(arr2[i] != arr2[i+1]){
				var = i;
				if(i == arr2.length-2){
					var = i+1;
				}
			}
		}

		if(var != -1)
			System.out.println(arr[var]);
		else	
			System.out.println("not present");
	}
}
