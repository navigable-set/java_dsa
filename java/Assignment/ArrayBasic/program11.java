/*
11] Product of maximum in first array and minimum in second
Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and minimum
element of the second array.
Example 1:
Input : A[] = {5, 7, 9, 3, 6, 2},
B[] = {1, 2, 6, -1, 0, 9}
Output : -9
Explanation:
The first array is 5 7 9 3 6 2.
The max element among these elements is 9.
The second array is 1 2 6 -1 0 9.
The min element among these elements is -1.
The product of 9 and -1 is 9*-1=-9.
Example 2:
Input : A[] = {0, 0, 0, 0},
B[] = {1, -1, 2}
Output : 0
Expected Time Complexity: O(N + M).
Expected Auxiliary Space: O(1).
Output:
For each test case, output the product of the max element of the first array and the
minimum element of the second array.
Constraints:
1 ≤ N, M ≤ 10^6-10^8 ≤ A i , B i ≤ 10^8
*/

import java.io.*;
class ArrayDemo{

	int getMaxEle(int arr[]){

	//	int max = Integer.MIN_VALUE;		//also working correct ...(only problem if trying to assign a fix-value randomly like -1 etc.)
		int max = arr[0];
		for(int i=0; i<arr.length; i++){
			if(max < arr[i])
				max = arr[i];
		}
		return max;
	}

	int getMinEle(int arr[]){

    	//	int min = Integer.MAX_VALUE;
		int min = arr[0];

                for(int i=0; i<arr.length; i++){
                        if(min > arr[i])
                                min = arr[i];
                }
                return min;
        }

	
        public static void main(String[] shweta) throws IOException{

		ArrayDemo obj = new ArrayDemo();

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size for first array :");
                int size1 = Integer.parseInt(br.readLine());

                int arr1[] = new int[size1];

                System.out.println("Enter Elements for first array : ");

                for(int i =0; i<arr1.length; i++){

                        arr1[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("Enter size for second array :");
                int size2 = Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter Elements for second array : ");

                for(int i =0; i<arr2.length; i++){

                        arr2[i] = Integer.parseInt(br.readLine());
                }

		int max = obj.getMaxEle(arr1);
		int min = obj.getMinEle(arr2);

		int result = max * min ;

		System.out.println("result : " + result);

	}
}
