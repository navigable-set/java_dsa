/*
22] Exceptionally odd
Given an array of N positive integers where all numbers occur even number of
times except one number which occurs odd number of times. Find the exceptional
number.
Example 1:
Input:
N =7
Arr[] = {1, 2, 3, 2, 3, 1, 3}
Output: 3
Explanation: 3 occurs three times.
Example 2:
Input:
N =7
Arr[] = {5, 7, 2, 7, 5, 2, 5}
Output: 5
Explanation: 5 occurs three times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10^5
1 ≤ arr[i] ≤ 10^6
*/

import java.io.*;
import java.util.*;

class ArrayDemo{

	int getCount(int arr[], int key){

		int count = 0;

		for(int i=0; i<arr.length ; i++){
			if(key == arr[i])
				count++;
		}

		return count;
	}

        public static void main(String[] shweta) throws IOException{

		ArrayDemo obj = new ArrayDemo();

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
		
                System.out.println("*************");

		TreeSet ts = new TreeSet();
				
		for(int i=0; i<arr.length ; i++){

			ts.add(arr[i]);	
		}

		Iterator itr = ts.iterator();
			
		while(itr.hasNext()){

			int x = (int)itr.next();
			int val = obj.getCount(arr,x);

			if(val % 2 != 0)
				System.out.println(x);
		}
	}
}
