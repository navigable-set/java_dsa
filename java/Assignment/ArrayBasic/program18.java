/*
18] Find Subarray with given sum | Set 1 (Non-negative
Numbers)
Given an array arr[] of non-negative integers and an integer sum, find a subarray
that adds to a given sum.
Note: There may be more than one subarray with sum as the given sum, print first
such subarray.
Examples:
Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
Output: Sum found between indexes 2 and 4
Explanation: Sum of elements between indices 2 and 4 is 20 + 3 + 10 = 33
Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
Output: Sum found between indexes 1 and 4
Explanation: Sum of elements between indices 1 and 4 is 4 + 0 + 0 + 3 = 7
Input: arr[] = {1, 4}, sum = 0
Output: No subarray found
Explanation: There is no subarray with 0 sum
*/

import java.io.*;
class ArrayDemo{

	void subArray(int arr[], int sum){
	
		int flag = 0;
		int i = 0;
		int j = i;

		for(i=0; i<arr.length; i++){
		 	
			int result = 0;
			
			for(j=i; j<arr.length; j++){
				
				result = result + arr[j];
	
				if(result == sum){
					flag = 1;
					System.out.println(result);
					break;
				}
			}
			if(flag == 1)
				break;
		}
		
		if(flag == 0)
			System.out.println("No subarray found");
		else
			System.out.println("Sum found between indexes " + i + " and " + j );
	}

        public static void main(String[] shweta) throws IOException{

		ArrayDemo obj = new ArrayDemo();

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		System.out.println("Enter the sum :");
		int sum = Integer.parseInt(br.readLine());

		obj.subArray(arr,sum);
	}
}
