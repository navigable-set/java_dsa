/*
36] Find a peak element which is not smaller than its neighbors
Given an array arr of n elements that is first strictly increasing and then maybe
strictly decreasing, find the maximum element in the array.
Note: If the array is increasing then just print the last element will be the maximum
value.
Examples:
Input: array[]= {5, 10, 20, 15}
Output: 20
Explanation: The element 20 has neighbors 10 and 15, both of them are less
than 20.
Input: array[] = {10, 20, 15, 2, 23, 90, 67}
Output: 20 or 90
Explanation: The element 20 has neighbors 10 and 15, both of them are less
than 20, similarly 90 has neighbors 23 and 67.
*/


import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		int pre = arr[0];
		int flag = 0;

		for(int i =0; i<arr.length-1; i++){

			if(pre < arr[i] && arr[i+1] < arr[i]){
				System.out.println("Output : " + arr[i]);
				flag = 1;
			}
			pre = arr[i];
		}

		if(flag == 0)
			System.out.println("No-peak element present");
	}
}
