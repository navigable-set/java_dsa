/*
6] Elements in the Range
Given an array arr[] containing positive elements. A and B are two numbers
defining a range. The task is to check if the array contains all elements in the given
range.
Example 1:
Input: N = 7, A = 2, B = 5
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: Yes
Explanation: It has elements between range 2-5 i.e 2,3,4,5
Example 2:
Input: N = 7, A = 2, B = 6
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: No
Explanation: Array does not contain 6.
Note: If the array contains all elements in the given range then driver code outputs
Yes otherwise, it outputs No
Expected Time Complexity: O(N).
Expected Auxiliary Space: O(1).
Constraints:
1 ≤ N ≤ 10^7
*/

import java.io.*;
class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		System.out.println("Enter element to start the rang :");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter element for ending range :");
		int end = Integer.parseInt(br.readLine());

		int flag = 0;

		for(int i = start; i<= end; i++){
			flag = 0;
			for(int j =0; j<arr.length; j++){
				if(i == arr[j]){
					flag = 1;
				}
			}
			if(flag == 0)
				break;
		}

		if(flag == 0)
			System.out.println("No");
		else
			System.out.println("Yes");
	}
}
