/*
26] Positive and negative elements
Given an array arr[ ] containing equal number of positive and negative elements,
arrange the array such that every positive element is followed by a negative
element.
Note- The relative order of positive and negative numbers should be maintained.
Example 1:
Input:
N =6
arr[] = {-1, 2, -3, 4, -5, 6}
Output:
2 -1 4 -3 6 -5
Explanation: Positive numbers in order are 2, 4 and 6. Negative numbers in
order are -1, -3 and -5. So the arrangement we get is 2, -1, 4, -3, 6 and -5.
Example 2:
Input:
N =4
arr[] = {-3, 2, -4, 1}
Output:
2 -3 1 -4
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N ≤ 10^6
1 ≤ arr[i] ≤ 10^9
*/

import java.io.*;
import java.util.*;

class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size(even) :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements..where half are positive and half are negative : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		if(size %2 == 0){
			int arr1[] = new int[size/2];	//for negative
			int arr2[] = new int[size/2];	//for positive

			int j = 0;
			int k = 0;
		
			for(int i=0; i<arr.length; i++){

				if(arr[i] < 0){
					arr1[j] = arr[i];
					j++;
				}else{
					arr2[k] = arr[i];
					k++;
				}
			}
	
			j = 0;
			k = 0;

			for(int i=0; i<arr.length; i++){

				if(i%2 == 0){
					arr[i] = arr2[j];
					j++;
				}else{
					arr[i] = arr1[k];
					k++;
				}
			}

			System.out.println("********");
			for(int i =0; i<arr.length; i++){
				System.out.println(arr[i]);
			}
		}else{
			System.out.println("invalid size for given problem");
		}
	}
}
