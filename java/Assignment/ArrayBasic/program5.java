/*
5] Replace all 0's with 5
You are given an integer N. You need to convert all zeros of N to 5.
Example 1:
Input:
N = 1004
Output: 1554
Explanation: There are two zeroes in 1004
on replacing all zeroes with "5", the new
number will be "1554".
Example 2:
Input:
N = 121
Output: 121
Explanation: Since there are no zeroes in
"121", the number remains as "121".
Expected Time Complexity: O(K) where K is the number of digits in N
Expected Auxiliary Space: O(1)
Constraints:
1 <= n <= 10000
*/

import java.io.*;

class ReplaceZero{

	public static void main(String[] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the number :");
		int num = Integer.parseInt(br.readLine());

		int temp = num;
		int rev = 0;

		//first replace zeros with 5 and place them in reverse nnumber..i.e digit-wise changes
		while(temp != 0){

			int rem = temp % 10;
			temp = temp/10;

			rev = (rev *10) + rem;

			//if zero then only add '5'
			if(rem == 0)
				rev = rev + 5;
		}

		temp = rev;
		rev = 0;

		//again reverse the number to get actual sequence
		while(temp != 0){
			int rem = temp % 10;
			temp = temp/10;
			
			rev = (rev * 10) + rem;
		}
		num = rev;

		System.out.println(num);
	}
}
