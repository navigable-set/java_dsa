/*
7] Form largest number from digits
Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
of the array such that after combining all the elements of the array, the number
formed is maximum.
Example 1:
Input:
N=5
A[] = {9, 0, 1, 3, 0}
Output:
93100
Explanation:
Largest number is 93100 which can be formed from array digits.
Example 2:
Input:
N =3
A[] = {1, 2, 3}
Output:
321
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10^7
0 <= Ai <= 9
*/

import java.io.*;
import java.util.*;
class RearrangeArray{

	public static void main(String[] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i=0; i<arr.length; i++){

			arr[i] = Integer.parseInt(br.readLine());
		}

		//sorting the array using sort() method of Arrays class
		//Arrays.sort(arr);
		
		//sorting using 'bubble-sort algorithm' but we sorting it in descending order
		for(int i=0; i<arr.length; i++){

			for(int j=0; j<arr.length-i-1; j++){

				if(arr[j] < arr[j+1]){
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}

		//after sorting array in decresing order... combining the array elements to form the possible maximum element
		int num = 0;
		for(int i=0; i<arr.length; i++){
			num = (num*10) + arr[i];
		}
		
		System.out.println("Sorted array in decresing order is : ");
		for(int i=0; i<arr.length; i++){
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	
		System.out.println("possible maximum number is : " + num);	
	}
}
