/*
9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
Examples 1:
Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
Output: arr[] = { 1, 2, 4, 5 }
Examples 2:
Input: arr[] = { 4, 5, 9, 8, 1 }, index = 3
Output: arr[] = { 4, 5, 9, 1 }
*/


import java.io.*;
class ArrayDemo{

	void removeEle(int arr[], int index){

		int arr2[] = new int[arr.length - 1];

		int j = 0;
		for(int i=0; i<arr.length; i++){

			if(i == index)
				continue;
			
			arr2[j] = arr[i];
			j++;
		}

		System.out.println("Array after removing the element at given index is : ");
		for(int i =0; i< arr2.length; i++){
			System.out.print(arr2[i] + " ");
		}
		System.out.println();

	}

        public static void main(String[] shweta) throws IOException{

		ArrayDemo obj = new ArrayDemo();

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
		
		System.out.println("Enter the index for removing the element at that index : ");
		int index = Integer.parseInt(br.readLine());

		if(index >= 0 && index < arr.length)
			obj.removeEle(arr,index);
		else
			System.out.println("Invalid index position");
		
	}
}
