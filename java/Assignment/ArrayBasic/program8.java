/*
8] Even occurring elements
Given an array Arr of N integers that contains an odd number of occurrences for all
numbers except for a few elements which are present even number of times. Find
the elements which have even occurrences in the array.
Example 1:
Input:
N = 11
Arr[] = {9, 12, 23, 10, 12, 12,
15, 23, 14, 12, 15}
Output: 12 15 23
Example 2:
Input:
N =5
Arr[] = {23, 12, 56, 34, 32}
Output: -1
Explanation:
Every integer is present odd number of times.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10^5
0 ≤ Arr[i] ≤ 63
*/

import java.util.*;
import java.io.*;

class ArrayDemo{

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
                
  		System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }
		
		//using Arraylist
		ArrayList al = new ArrayList();


		for(int i=0; i<arr.length;i++){

			int count = 0;
                        
			for(int j = 0; j<arr.length;j++){
                                
                                if(arr[i] == arr[j])
                                        count++;
                        }
                        
                        if(count%2 == 0){

				if(!(al.contains(arr[i])))
                              		al.add(arr[i]);      
			}
		}

		if(al.isEmpty())
			System.out.println("-1");
		else
			System.out.println(al);
	}
}
