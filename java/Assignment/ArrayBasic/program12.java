/*
12] First and last occurrences of X
Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.
Example 1:
Input:
N =4, X =3
arr[] = { 1, 3, 3, 4 }
Output:
12
Explanation:
For the above array, first occurance of X = 3 is at index = 1 and last
occurrence is at index = 2.
Example 2:
Input:
N = 4, X = 5
arr[] = { 1, 2, 3, 4 }
Output:
-1
Explanation:
As 5 is not present in the array, so the answer is -1.
Expected Time Complexity: O(log(N))
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10^50 <= arr[i], X <= 10^9
*/

import java.io.*;
class ArrayDemo{

	int firstOccr(int arr[], int key){

		int first  = -1;

		for(int i = 0; i<arr.length; i++){
			
			if(key == arr[i]){
				first = i;
				return first;
			}
		}
		return first;
	}
	
	int lastOccr(int arr[], int key){

		int last  = -1;

		for(int i = arr.length -1 ; i >= 0 ; i--){
			
			if(key == arr[i]){
				last = i;
				return last;
			}
		}
		return last;
	}

        public static void main(String[] shweta) throws IOException{

		ArrayDemo obj = new ArrayDemo();

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter array size :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements : ");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		System.out.println("Enter the element to get its first and last occurance");

		int key = Integer.parseInt(br.readLine());

		int first = obj.firstOccr(arr,key);
		int last = obj.lastOccr(arr,key);

		if(first != -1)
			System.out.println("first and last occurance is : " + first + " " + last);
		else
			System.out.println(first);
	}
}
