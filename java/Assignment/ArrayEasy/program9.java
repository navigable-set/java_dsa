/*
Que 9 : Find transition Point
Given a sorted array containing only 0s and 1s, find the transition point.
Example 1:
Input:
N=5
arr[] = {0,0,0,1,1}
Output: 3
Explanation: index 3 is the transition point where 1 begins.
Example 2:
Input:
N=4
arr[] = {0,0,0,0}
Output: -1
Explanation: Since, there is no "1", the answer is -1.
Expected Time Complexity: O(LogN)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 500000
0 ≤ arr[i] ≤ 1
*/


import java.util.*;
class ArrayDemo{

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int flag = 0;

		for(int i =0; i<arr.length; i++){

			if(arr[i] != 0 && arr[i] != 1){
				flag = 1;
				break;
			}
		}

		if(flag == 0 && arr.length>0){

		//	Arrays.sort(arr);	//given array ha sorted ch asnar aahe..so sort kray chi grj nahia..jr kel tr sorted manner nusar index return hoil..je actual array shi similar nsel
			int index = -1;
			int val = arr[0];

			for(int i =0; i<arr.length; i++){
	
				if(arr[i] != val){
	
					index = i;
					break;
				}
			}
			
			System.out.println("Transition point : " + index);

		}else{
			System.out.println("Array is invalid");
		}
	}
}
