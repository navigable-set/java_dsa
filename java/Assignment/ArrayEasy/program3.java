/*
Que 3 : Find Duplicates in an Array
Given an array of size N which contains elements from 0 to N-1, you need to find all the
elements occurring more than once in the given array. Return the answer in ascending
order. If no such element is found, return list containing [-1].
Note: The extra space is only for the array to be returned. Try and perform all operations
within the provided array.
Example 1:
Input:
N=4
a[] = {0,3,1,2}
Output:
-1
Explanation:
There is no repeating element in the array. Therefore output is -1.
Example 2:
Input:
N=5
a[] = {2,3,1,2,3}
Output:
23
Explanation:
2 and 3 occur more than once in the given array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10 5
0 <= A[i] <= N-1, for each valid i
*/


import java.util.*;
import java.io.*;

class ArrayDemo{

	static int[] findDuplicate(int arr[]){
		
		//to get each element...so that can be given to 'frequency()' to get its count
		ArrayList al = new ArrayList();
		
		//to store unique numbers
		TreeSet ts = new TreeSet();

		//to store duplicate elements
		TreeSet ts2 = new TreeSet();

		for(int i =0; i<arr.length; i++){
			
			ts.add(arr[i]);
			al.add(arr[i]);
		}

		Iterator itr = ts.iterator();

		while(itr.hasNext()){

			int obj = (int)itr.next();
			int count = Collections.frequency(al,obj);
			
			if(count > 1)
				ts2.add(obj);	
		}

		//to return from the function(according to problem statement)
		int arr2[] = new int[ts2.size()];
		itr = ts2.iterator();

		int i = 0;
		while(itr.hasNext()){

			arr2[i] = (int)itr.next();
			i++;
		}
			
		Arrays.sort(arr2);

		return arr2;		
	}	

        public static void main(String[] shweta) throws IOException{

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter number :");
                int size = Integer.parseInt(br.readLine());

                int arr[] = new int[size];

                System.out.println("Enter array Elements from 0 to " + (size-1) + " :");

                for(int i =0; i<arr.length; i++){

                        arr[i] = Integer.parseInt(br.readLine());
                }

		int retArr[] = findDuplicate(arr);

		System.out.println("Output :");

		for(int i =0; i<retArr.length; i++){

			System.out.println(retArr[i]);
		}	

		if(retArr.length == 0)
			System.out.println("-1");
	}

}
