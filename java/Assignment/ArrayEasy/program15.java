/*
Que 15 : Union of two sorted Array
Union of two arrays can be defined as the common and distinct elements in the two
arrays.
Given two sorted arrays of size n and m respectively, find their union.
Example 1:
Input:
n = 5, arr1[] = {1, 2, 3, 4, 5}
m = 3, arr2 [] = {1, 2, 3}
Output: 1 2 3 4 5
Explanation: Distinct elements including both the arrays are: 1 2 3 4 5.
Example 2:
Input:
n = 5, arr1[] = {2, 2, 3, 4, 5}
m = 5, arr2[] = {1, 1, 2, 3, 4}
Output: 1 2 3 4 5
Explanation: Distinct elements including both the arrays are: 1 2 3 4 5.
Example 3:
Input:
n = 5, arr1[] = {1, 1, 1, 1, 1}
m = 5, arr2[] = {2, 2, 2, 2, 2}
Output: 1 2
Explanation: Distinct elements including both the arrays are: 1 2.
Expected Time Complexity: O(n+m).
Expected Auxiliary Space: O(n+m).
Constraints:
1 <= n, m <= 10 5
1 <= arr[i], brr[i] <= 10 6
*/


import java.util.*;
class ArrayDemo{

	static TreeSet union(int arr1[], int arr2[]){

		TreeSet ts = new TreeSet();

		for(int i =0; i<arr1.length; i++){
			ts.add(arr1[i]);
		}
		
		for(int i =0; i<arr2.length; i++){
			ts.add(arr2[i]);
		}

		return ts;
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                //array-1
                System.out.println("Enter array-1 size :");
                int size1 = sc.nextInt();

                int arr1[] = new int[size1];

                System.out.println("Enter array-1 elements : ");

                for(int i=0; i<arr1.length; i++){

                        arr1[i] = sc.nextInt();
                }

                //array-2
                System.out.println("Enter array-2 size :");
                int size2 = sc.nextInt();

                int arr2[] = new int[size2];

                System.out.println("Enter array-2 elements : ");

                for(int i=0; i<arr2.length; i++){

                        arr2[i] = sc.nextInt();
                }

		TreeSet ts = union(arr1,arr2);

		Iterator itr = ts.iterator();

		System.out.println("Output : ");

		while(itr.hasNext()){

			System.out.print(itr.next() + "	");
		}
		System.out.println();
	}
}
