/*
Que 19 : Elements with left side smaller and right side greater
Given an unsorted array of size N. Find the first element in the array such that all of its
left elements are smaller and all right elements to it are greater than it.
Note: Left and right side elements can be equal to required elements. And extreme
elements cannot be required.
Example 1:
Input:
N=4
A[] = {4, 2, 5, 7}
Output:
5
Explanation:
Elements on the left of 5 are smaller than 5 and on the right of it are greater than 5.
Example 2:
Input:
N=3
A[] = {11, 9, 12}
Output:
-1
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
3 <= N <= 10 6
1 <= A[i] <= 10 6
*/ 


import java.util.*;
class ArrayDemo{

	static int getEle(int arr[]){

		int val = -1;
		
		//array with size 0/1/2 will retun ret..also if no element fount then it will return ret i.e '-1'

		for(int i = 1; i < arr.length-1; i++){

			if(arr[i] >= arr[i-1] && arr[i] <= arr[i+1])
				val =  arr[i];					//must return only one and first element
		}

		return val;
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }
		
		int ret = getEle(arr);

		System.out.println("Output : " + ret);
	}
}
