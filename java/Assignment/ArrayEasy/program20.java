/*
Que 20 : Bitonic point
Given an array Arr of n elements that is first strictly increasing and then maybe strictly
decreasing, find the maximum element in the array.
Note: If the array is increasing then just print the last element will be the maximum value.
Example 1:
Input:
n=9
arr[] = {1,15,25,45,42,21,17,12,11}
Output: 45
Explanation: Maximum element is 45.
Example 2:
Input:
n=5
arr[] = {1, 45, 47, 50, 5}
Output: 50
Explanation: Maximum element is 50.
Expected Time Complexity: O(logn)
Expected Auxiliary Space: O(1)
Constraints:
3 ≤ n ≤ 10 6
1 ≤ arr i ≤ 10 6
*/

import java.util.*;
class ArrayDemo{

	static int bitonicPoint(int arr[],int start, int end){

		int max= Integer.MIN_VALUE;

		int mid = arr.length/2;

		if(arr[mid] > arr[arr.length-1])

		for(){

		}
	}
	
/*	static int bitonicPoint(int arr[]){

		int max= Integer.MIN_VALUE;

		for(int i =0; i<arr.length; i++){

			if(max<arr[i])
				max = arr[i];
		}

		return max;
	}

*/
       public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int max = bitonicPoint(arr);

		System.out.println("Output : " + max);
       }
}
