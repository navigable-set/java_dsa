/*
Que 17 : Find all Pairs with given Sum
Given two unsorted arrays A of size N and B of size M of distinct elements, the task is to
find all pairs from both arrays whose sum is equal to X.
Note: All pairs should be printed in increasing order of u. For eg. for two pairs (u1,v1)
and (u2,v2), if u1 < u2 then
(u1,v1) should be printed first else second.
Example 1:
Input:
A[] = {1, 2, 4, 5, 7}
B[] = {5, 6, 3, 4, 8}
X=9
Output:
18
45
54
Explanation: (1, 8), (4, 5), (5, 4) are the pairs which sum to 9.
Example 2:
Input:
A[] = {-1, -2, 4, -6, 5, 7}
B[] = {6, 3, 4, 0}
X=8
Output:
44
53
Expected Time Complexity: O(NLog(N))
Expected Auxiliary Space: O(N)
Constraints:
1 ≤ N, M ≤ 10 6
-10 6 ≤ X, A[i], B[i] ≤ 10 6
*/

import java.util.*;
class ArrayDemo{

	static void getPairs(int arr1[], int arr2[], int sum){

		Arrays.sort(arr1);		//extra arry ghetla...mhanun S.C = O(N)

		for(int i = 0; i<arr1.length ; i++){

			for(int j = 0; j<arr2.length; j++){

				if(arr1[i] + arr2[j] == sum){

					System.out.println(arr1[i] + " " + arr2[j]);
				}
			}
		}
	}

       public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                //array-1
                System.out.println("Enter array-1 size :");
                int size1 = sc.nextInt();

                int arr1[] = new int[size1];

                System.out.println("Enter array-1 elements : ");

                for(int i=0; i<arr1.length; i++){

                        arr1[i] = sc.nextInt();
                }

                //array-2
                System.out.println("Enter array-2 size :");
                int size2 = sc.nextInt();

                int arr2[] = new int[size2];

                System.out.println("Enter array-2 elements : ");

                for(int i=0; i<arr2.length; i++){

                        arr2[i] = sc.nextInt();
                }

		System.out.println("Enter sum : ");
		int sum = sc.nextInt();

		getPairs(arr1,arr2,sum);
       }
}
