/*
Que 21 : Non Repeating Element
Find the first non-repeating element in a given array Arr of N integers.
Note: Array consists of only positive and negative integers and not zero.
Example 1:
Input : arr[] = {-1, 2, -1, 3, 2}
Output : 3
Explanation: -1 and 2 are repeating whereas 3 is the only number occuring once.
Hence, the output is 3.
Example 2:
Input : arr[] = {1, 1, 1}
Output : 0
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= N <= 10 7
-10 16 <= A i <= 10 16
{A i !=0 }
*/
