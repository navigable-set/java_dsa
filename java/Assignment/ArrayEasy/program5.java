/* 
Que 5 : Peak Elements
An element is called a peak element if its value is not smaller than the value of its
adjacent elements(if they exist).
Given an array arr[] of size N, Return the index of any one of its peak elements.
Note: The generated output will always be 1 if the index that you return is correct.
Otherwise output will be 0.
Example 1:
Input:
N=3
arr[] = {1,2,3}
Possible Answer: 2
Generated Output: 1
Explanation: index 2 is 3. It is the peak element as it is greater than its neighbor
2. If 2 is returned then the generated output will be 1 else 0.
Example 2:
Input:
N=3
arr[] = {3,4,2}
Possible Answer: 1
Output: 1
Explanation: 4 (at index 1) is the peak element as it is greater than it's neighbor
elements 3 and 2. If 1 is returned then the generated output will be 1 else 0.
Expected Time Complexity: O(log N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10 5
1 ≤ A[] ≤ 10 6
*/

import java.util.*;
class ArrayDemo{

	static int peakEle(int arr[]){

		int index = -1;

		if(arr[0] > arr[1]){
			index = 0;
			return index;
		}

		for(int i =1; i<arr.length-1; i++){

			if(arr[i]>arr[i-1] && arr[i] > arr[i+1]){
				index = i;
				return index;
			}
		}

		if(arr[arr.length-1] > arr[arr.length-2]){
			index = arr.length-1;
			return index;
		}

		return index;
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }
	
		if(arr.length > 1){
			int ret = peakEle(arr);		

			if(ret == -1)
				System.out.println("Output : " + 0);		 //if peak element does not exist
			else
				System.out.println("Output : " + 1);		//peak element exists
		}else{
			System.out.println("No peak element present as array has only one element");		
		}
	}
}
