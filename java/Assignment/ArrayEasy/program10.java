/*
Que 10 : First Repeating Element
Given an array arr[] of size n, find the first repeating element. The element should occur
more than once and the index of its first occurrence should be the smallest.
Note:- The position you return should be according to 1-based indexing.
Example 1:
Input:
n=7
arr[] = {1, 5, 3, 4, 3, 5, 6}
Output: 2
Explanation: 5 is appearing twice and its first appearance is at index 2 which is
less than 3 whose first occurring index is 3.
Example 2:
Input:
n=4
arr[] = {1, 2, 3, 4}
Output: -1
Explanation: All elements appear only once so the answer is -1.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(N)
Constraints:
1 <= n <= 10 6
0 <= A i <= 10 6
*/


import java.util.*;
class ArrayDemo{

	static int firstRepeat(int arr[]){

		int index = -1;

		for(int i =0; i<arr.length-1; i++){

			for(int j=i+1; j<arr.length; j++){
			
				if(arr[i] == arr[j]){

					index = i;
					return index;
				}
			}
		}
		return index;
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int ret = firstRepeat(arr);

		if(ret != -1)
			System.out.println("Output : " + ++ret);
		else
			System.out.println("Output : " + ret);

	}
}
