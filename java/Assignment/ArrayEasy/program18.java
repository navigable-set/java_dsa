/*
Que 18 : Move all zero two the end of Array
Given an array arr[] of N positive integers. Push all the zeros of the given array to the
right end of the array while maintaining the order of non-zero elements.
Example 1:
Input:
N=5
Arr[] = {3, 5, 0, 0, 4}
Output: 3 5 4 0 0
Explanation: The non-zero elements preserve their order while the 0 elements are
moved to the right.
Example 2:
Input:
N=4
Arr[] = {0, 0, 0, 4}
Output: 4 0 0 0
Explanation: 4 is the only non-zero element and it gets moved to the left.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10^5
0 ≤ arr i ≤ 10^5
*/

import java.util.*;
class ArrayDemo{

	//given condition's are been followed..giving correct output for given example..but will goes wrong for => 5,0,3,0,1 => 5,1,0,3,0 || 1,0,0,2,8=> 1,8,2,0,0
	
	static void moveZero(int arr[]){

		for(int i = 0; i<arr.length; i++){

			if(arr[i] == 0){

				for(int j = i+1; j<arr.length; j++){

					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		moveZero(arr);

		System.out.println("Output : ");
		for(int x : arr){
			System.out.print(x + "	");
		}
		System.out.println();
	}
}
