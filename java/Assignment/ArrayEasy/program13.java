/*
Que 13 : Minimum distance between two number
You are given an array A, of N elements. Find minimum index based distance between
two elements of the array, x and y such that (x!=y).
Example 1:
Input:
N=4
A[] = {1,2,3,2}
x = 1, y = 2
Output: 1
Explanation: x = 1 and y = 2. There are two distances between x and y, which are
1 and 3 out of which the least is 1.
Example 2:
Input:
N=7
A[] = {86,39,90,67,84,66,62}
x = 42, y = 12
Output: -1
Explanation: x = 42 and y = 12. We return -1 as x and y don't exist in the array.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10 5
0 <= A[i], x, y <= 10 5
*/


import java.util.*;
class ArrayDemo{

	static int minDist(int arr[], int x, int y){
	
                        int index1 = -1;
                        int index2 = -1;

                        for(int i =0; i<arr.length; i++){

                                if(x == arr[i]){
                                        index1 = i;
                                        break;
                                }
                        }

                        for(int i =0; i<arr.length; i++){

                                if(y == arr[i]){
                                        index2 = i;
                                        break;
                                }
                        }

                        if(index1 != -1 && index2 != -1){

				if(index1 > index2)
					return index1 - index2;
				else
					return index2 - index1;
			}else{
				return -1;		//if 'x' and 'y' does not prsent in the array
			}
	}

      	public static void main(String[] shweta){

     		Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		System.out.println("Enter number-1 : ");
		int x = sc.nextInt();
		
		System.out.println("Enter number-2 : ");
		int y = sc.nextInt();

		if(x != y){

			int ret = minDist(arr,x,y);
			System.out.println("Output : " + ret);
		}else{
			System.out.println("Output : -1");
		}
	}
}
