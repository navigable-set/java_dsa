/*
Que 8 : Rotate Array
Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise
direction) by D steps, where D is a positive integer.
Example 1:
Input:
N = 5, D = 2
arr[] = {1,2,3,4,5}
Output: 3 4 5 1 2
Explanation: 1 2 3 4 5 when rotated
by 2 elements, it becomes 3 4 5 1 2.
Example 2:
Input:
N = 10, D = 3
arr[] = {2,4,6,8,10,12,14,16,18,20}
Output: 8 10 12 14 16 18 20 2 4 6
Explanation: 2 4 6 8 10 12 14 16 18 20
when rotated by 3 elements, it becomes
8 10 12 14 16 18 20 2 4 6.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 <= N <= 10 6
1 <= D <= 10 6
0 <= arr[i] <= 10 5
*/


import java.util.*;
class ArrayDemo{

	static void rotateArr(int arr[],int steps){

		for(int i=1; i<= steps; i++){

			int val = arr[0];
			int j =0;
			
			for(; j<arr.length-1; j++){
			
				arr[j] = arr[j+1];
			}
			arr[j] = val;
		}
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


		if(size > 0){
	                System.out.println("Enter array elements : ");

        	        for(int i=0; i<arr.length; i++){

                	        arr[i] = sc.nextInt();
                	}
		
			System.out.println("Enter number of steps : ");
			int steps = sc.nextInt();
	
			rotateArr(arr,steps);

			for(int x : arr){

				System.out.print(x + "\t");
			}
			System.out.println();
		}else{
			System.out.println("Array is empty with size = 0");
		}
	}
}
