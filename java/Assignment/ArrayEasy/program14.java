/*
Que 14 : Intersection of two Array
Given two arrays a[] and b[] respectively of size n and m, the task is to print the count of
elements in the intersection (or common elements) of the two arrays.
For this question, the intersection of two arrays can be defined as the set containing
distinct common elements between the two arrays.
Example 1:
Input:
n = 5, m = 3
a[] = {89, 24, 75, 11, 23}
b[] = {89, 2, 4}
Output: 1
Explanation:
89 is the only element in the intersection of two arrays.
Example 2:
Input:
n = 6, m = 5
a[] = {1, 2, 3, 4, 5, 6}
b[] = {3, 4, 5, 6, 7}
Output: 4
Explanation:
3 4 5 and 6 are the elements in the intersection of two arrays.
Expected Time Complexity: O(n + m).
Expected Auxiliary Space: O(min(n,m)).
Constraints:
1 ≤ n, m ≤ 10 5
1 ≤ a[i], b[i] ≤ 10 5
*/



import java.util.*;
class ArrayDemo{

	static int intersection(int arr1[], int arr2[]){

		//array has distinct element in itself...so no need to handle such scenario
		int count = 0;

		for(int i =0; i<arr1.length; i++){
			for(int j =0; j<arr2.length; j++){

				if(arr1[i] == arr2[j]){
					count++;
					break;
				}
			}
		}

		return count;
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

		//array-1
                System.out.println("Enter array-1 size :");
                int size1 = sc.nextInt();

                int arr1[] = new int[size1];

                System.out.println("Enter array-1 elements : ");

                for(int i=0; i<arr1.length; i++){

                        arr1[i] = sc.nextInt();
                }
                
		//array-2
		System.out.println("Enter array-2 size :");
                int size2 = sc.nextInt();

                int arr2[] = new int[size2];

                System.out.println("Enter array-2 elements : ");

                for(int i=0; i<arr2.length; i++){
                
			arr2[i] = sc.nextInt();
                }

		//function call..the function returns count of common elements/intersection
		int count = intersection(arr1,arr2);

		System.out.println("Output : " + count);
	}
}
