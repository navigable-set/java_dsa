/*
Que 6 : Second Largest
Given an array Arr of size N, print the second largest distinct element from an array.
Example 1:
Input:
N=6
Arr[] = {12, 35, 1, 10, 34, 1}
Output: 34
Explanation: The largest element of the array is 35 and the second largest element
is 34.
Example 2:
Input:
N=3
Arr[] = {10, 5, 10}
Output: 5
Explanation: The largest element of the array is 10 and the second largest element
is 5.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
2 ≤ N ≤ 10 5
1 ≤ Arr i ≤ 10 5
*/

import java.util.*;
class ArrayDemo{

	static int secondLarg(int arr[]){

		int max = arr[0];
		int pre = -1;

		for(int i=0; i<arr.length; i++){

			if(max < arr[i]){

				pre = max;
				max = arr[i];

			}else if(pre < arr[i] && max > arr[i]){
				
				pre = arr[i];
			}
		}
		return pre;
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int ret = secondLarg(arr);

		System.out.println("Output : " + ret);
	} 
}
