/*
Que 7 : Check if array is sorted
Given an array arr[] of size N, check if it is sorted in non-decreasing order or not.
Example 1:
Input:
N=5
arr[] = {10, 20, 30, 40, 50}
Output: 1
Explanation: The given array is sorted.
Example 2:
Input:
N=6
arr[] = {90, 80, 100, 70, 40, 30}
Output: 0
Explanation: The given array is not sorted.
Expected Time Complexity: O(N)
Expected Auxiliary Space: O(1)
Constraints:
1 ≤ N ≤ 10 5
1 ≤ Arr[i] ≤ 10 6
*/

import java.util.*;
class ArrayDemo{

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		int flag = 1;

		for(int i=0; i<arr.length-1; i++){

			if(arr[i] > arr[i+1]){
				flag = 0;
				break;
			}
		}

		if(flag == 1)
			System.out.println("Output : 1");
		else
			System.out.println("Output : 0");
	}
}
