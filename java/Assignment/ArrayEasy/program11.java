/*
Que 11 : Frequencies of limited Range Array Elements
Given an array A[] of N positive integers which can contain integers from 1 to P where
elements can be repeated or can be absent from the array. Your task is to count the
frequency of all elements from 1 to N.
Note: The elements greater than N in the array can be ignored for counting and do modify
the array in-place.
Example 1:
Input:
N=5
arr[] = {2, 3, 2, 3, 5}
P=5
Output:
02201
Explanation: Counting frequencies of each array element
We have:
1 occurring 0 times.
2 occurring 2 times.
3 occurring 2 times.
4 occurring 0 times.
5 occurring 1 time.
Example 2:
Input:
N=4
arr[] = {3,3,3,3}
P=3
Output:
0040
Explanation: Counting frequencies of each array element
We have:
1 occurring 0 times.
2 occurring 0 times.
3 occurring 4 times.
4 occurring 0 times.
Can you solve this problem without using extra space (O(1) Space)?
Constraints:
1 ≤ N ≤ 10 5
1 ≤ P ≤ 4*10 4
1 <= A[i] <= P
*/

import java.util.*;
class ArrayDemo{

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }
		
		int arr2[] = new int[arr.length];

		//'arr2 = arr' ==> aas kel tr arr mdha chnage sgla arr2 mdhe reflect hoil..mhanun ek ek element copy kruya!!
		for(int i =0; i<arr.length; i++){

			arr2[i] = arr[i];
		}

		for(int i =1; i<=arr.length; i++){

			int count = 0;
			for(int j=0; j<arr.length; j++){

				//comparing with 'arr2' and making changes in original means 'arr1' array..so that the comaparison won't go wrong!
				if(i == arr2[j])
					count++;
			}

			if(count!= 0)
				arr[i-1] = count;
			else
				arr[i-1] = 0;
		}

		//printing the array
		for(int x : arr){
			System.out.print(x + "	");
		}
		System.out.println();
	}
} 
