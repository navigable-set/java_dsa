/*
10 10 10 10
11 11 11
12 12
13
*/

class Demo{

	public static void main(String [] shweta){

		int val =10;
		int row = 4;

		for(int i=1; i<= row; i++){
			for(int j=1; j<=row-i+1; j++){	//OR : for(int j=row; j>=i; j--)

				System.out.print(val + "\t");
			}
			val++;
			System.out.println();
		}
	}
}
