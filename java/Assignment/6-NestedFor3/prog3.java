/*
write a program to print the following pattern
10
9 8 
7 6 5
4 3 2 1
*/

class Demo{

	public static void main(String [] shweta){

		int val = 10;
		int row = 4;

		for(int i =1; i<=row; i++){

			for(int j=1; j<=i; j++){
				System.out.print(val-- + "\t");
			}
			System.out.println();
		}
	}
}
