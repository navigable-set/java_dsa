/*
9
9 8
9 8 7
9 8 7 6
*/

class Demo{

	public static void main(String [] shweta){

		int row = 4;

		for(int i = 1; i<= row; i++){

			int val = 9;

			for(int j=1; j<=i; j++){

				System.out.print(val-- + "\t");
			}
			System.out.println();
		}
	}
}
