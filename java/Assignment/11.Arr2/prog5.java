/*
WAP to take size of array from user and also take integer elements from user
find the minimum element from the array
input : Enter size : 5
Enter array elements: 1 2 5 0 4
output: min element = 0
*/

import java.io.*;
class ArrayDemo{

	int minEle(int arr[],int size){
		
		int min = arr[0];

		for(int i=1; i<size; i++){

			if(arr[i] < min )
				min = arr[i];
		}

		return min;
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
		
		int min = obj.minEle(arr,size);
	
		System.out.println("Min element in an array is : "+ min);
	}
}

