/*
WAP to print the elements whose addition of digits is even.
Ex. 26 = 2 + 6 = 8 (8 is even so print 26)
Input :
Enter array : 1 2 3 5 15 16 14 28 17 29 123
Output: 2 15 28 17 123
*/

import java.io.*;
class ArrayDemo{

static int flag = 0;

	void arrEle(int x){
	
		int temp = x;
		int sum = 0;
		
		while(temp != 0){
			int rem = temp%10;
			sum = sum + rem;
			temp = temp/10;
		}
		
		if(sum%2 == 0){
			flag = 1;
			System.out.println(x);
		}
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
		System.out.println("output :");
				
		for(int i=0; i<size; i++){
			obj.arrEle(arr[i]);
		}
		
		if(flag == 0)
			System.out.println("No elements with digits whose sum is even!");
	}
}
