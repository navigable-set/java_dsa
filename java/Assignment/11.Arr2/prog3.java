/*
Write a Java program to find the sum of even and odd numbers in an array.
Display the sum value.
Input: 11 12 13 14 15
Output
Odd numbers sum = 39
Even numbers sum = 26
*/

import java.io.*;
class ArrayDemo{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		int even =0;
		int odd = 0;

		for(int i=0; i<size; i++){
			if(arr[i] %2 ==0)
				even = even + arr[i];
			else
				odd = odd + arr[i];
		}

		System.out.println("Sum of Even elements is : " + even);
		System.out.println("Sum of Odd elements is : " + odd);
	}
}
