/*
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2
*/

import java.io.*;
class ArrayDemo{

	int searchEle(int arr[],int size,int key){
		
		int index = -1;

		for(int i=0; i<size; i++){

			if(arr[i] == key){
				index = i;
				break;
			}
		}

		return index;
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		System.out.println("Enter element to search :");

		int key = Integer.parseInt(br.readLine());

		int index = obj.searchEle(arr,size,key);

		if(index == -1)
			System.out.println(key + " not found in given Array");
		else
			System.out.println(key + " found at index " + index);
	}
}

