/*
WAP to find the common elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Common elements :
1
2
*/
//used another function but used extra arra(for better presentation)..also done without extra array
 
		
import java.io.*;
class ArrayDemo{

	static void commnEle(int arr1[],int arr2[], int size){
		
		//'carr' which will store the common elements..can also done without using 3rd array...size will be equal to the size of original array but will insert data equal to the count of common elements
		int carr[] = new int[size];
		int count =0;	//if no common then will notify from this

		for(int i=0; i<arr1.length; i++){
			for(int j=0; j<arr2.length; j++){
				if(arr1[i] == arr2[j]){
					count++;
					carr[count-1] = arr1[i];
					break;
				}
			}
		}

		if(count == 0)
			System.out.println("No common elements found!!");
		else{
			System.out.println("Common elements are :");
			for(int i=0; i<count; i++){
				System.out.println(carr[i] +"\t");
			}
		}
	}

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter array-1 elements :");

		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array-2 elements :");

		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		commnEle(arr1,arr2,size);
	}
}
