/*
Write a program to create an array of ‘n’ integer elements.
Where ‘n’ value should be taken from the user.
Insert the values from users and find the sum of all elements in the array.
Input:
n=6
Enter elements in the array: 2 3 6 9 5 1
Output:
26
*/

import java.io.*;
class ArrayDemo{

	int arrSum(int arr[],int size){
		
		int sum=0;

		for(int i=0; i<size; i++){
			sum = sum + arr[i];
		}

		return sum;
	}

	public static void main(String [] shweta)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array elements :");
		for(int i=0; i<size; i++){
			arr[i] =Integer.parseInt(br.readLine());
		}

		System.out.println("Array is : ");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		int sum = obj.arrSum(arr,size);

		System.out.println("Sum of Array Elements is : " + sum);
	}
}
