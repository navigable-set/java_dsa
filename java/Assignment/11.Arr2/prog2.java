/*
WAP to find the number of even and odd integers in a given array of integers
Input: 1 2 5 4 6 7 8
Output:
Number of Even Elements: 4
Number of Odd Elements : 3
*/

import java.io.*;
class ArrayDemo{

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		int even =0;
		int odd = 0;

		for(int i=0; i<size; i++){
			if(arr[i] %2 ==0)
				even++;
			else
				odd++;
		}

		System.out.println("count of Even elements is : " + even);
		System.out.println("count of Odd elements is : " + odd);
	}
}
