/*
WAP to find the uncommon elements between two arrays.
Input :
Enter first array : 1 2 3 5
Enter Second array: 2 1 9 8
Output: Uncommon elements :
3
5
9
8
*/
 
		
import java.io.*;
class ArrayDemo{

	static int commnEle(int arr1[],int arr2[], int size){
		
		int count = 0;
		int flag =0;

		//System.out.println("uncommon elements are :");
		
		for(int i=0; i<arr1.length; i++){
			
			for(int j=0; j<arr2.length; j++){
				if(arr1[i] != arr2[j]){
					flag=1;
				}else{
					flag =0;
					break;
				}
			}

			if(flag == 1){
				count++;
				System.out.println(arr1[i]);
			}
		}

		flag = 0;

		for(int i=0; i<arr2.length; i++){
			
			for(int j=0; j<arr1.length; j++){
				if(arr2[i] != arr1[j]){
					flag=1;	
				}else{
					flag =0;
					break;
				}
			}

			if(flag == 1){
				count++;
				System.out.println(arr2[i]);
			}
		}
		return count;
	}

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size :");
		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter array-1 elements :");

		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array-2 elements :");

		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println();
		int ret = commnEle(arr1,arr2,size);

		if(ret == 0){
			System.out.println("Both array have common elements!!");
		}
	}
}
