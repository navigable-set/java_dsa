/*
Write a Java program to merge two given arrays.
Array1 = [10, 20, 30, 40, 50]
Array2 = [9, 18, 27, 36, 45]
Output :
Merged Array = [10, 20, 30, 40, 50, 9, 18, 27, 36, 45]
Hint: you can take 3rd array
*/

import java.io.*;
class ArrayDemo{

	void mergeArr(int arr1[],int arr2[],int size1,int size2){
	
		int arr3[] = new int[size1+size2];
		
		int itr=0;
		
		for(int i=0; i<size1; i++){
			arr3[itr] = arr1[i];
			itr++;
		}
		
		for(int i=0; i<size2; i++){
			arr3[itr] = arr2[i];
			itr++;
		}
		
		for(int i=0; i<arr3.length; i++){
			System.out.print(arr3[i] + "\t");
		}
		System.out.println();
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		ArrayDemo obj = new ArrayDemo();

		System.out.println("Enter Array-1 size :");
		int size1 = Integer.parseInt(br.readLine());
		
		System.out.println("Enter Array-2 size :");
		int size2 = Integer.parseInt(br.readLine());


		int arr1[] = new int[size1];
		int arr2[] = new int[size2];

		System.out.println("Enter Array-1 Elements :");

		for(int i=0; i<size1; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Array-2 Elements :");

		for(int i=0; i<size2; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array-1 is :");
		for(int i=0; i<size1; i++){
			System.out.print(arr1[i] + "\t");
		}
		System.out.println();

		System.out.println("Array-2 is :");
		for(int i=0; i<size2; i++){
			System.out.print(arr2[i] + "\t");
		}
		System.out.println();
		
		System.out.println("Merged Array is :");
		obj.mergeArr(arr1,arr2,size1,size2);	
	}
}
