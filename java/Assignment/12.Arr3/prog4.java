/*
WAP to find a prime number from an array and return its index.
Take size and elements from the user
Input: 10 25 36 566 34 53 50 100
Output: prime no 53 found at index: 5
*/

import java.io.*;
class ArrayDemo{

static int flag = 0;

	int primeEle(int key,int index){
	
		int count = 0;
		for(int i=1; i<= key; i++){
			if(key % i == 0)
				count++;
		}

		if(count == 2)
			return index;
		else
			return index = -1;
	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		for(int i=0; i<size; i++){
			int index = obj.primeEle(arr[i],i);	
			if(index != -1){
				flag = 1;
				System.out.println("prime number " + arr[i]+ " found at index : " + index);
			}
		}

		if(flag == 0)
			System.out.println("No prime numbers present in an array!");
	}	
}
