/*
WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
Output: Palindrome no 252 found at index: 2
*/

import java.io.*;
class ArrayDemo{

static int flag = 0;

	int palindromeEle(int key,int index){
	
		int rev =0;
		int temp = key;
		
		while(temp != 0){
			int rem = temp % 10;
			rev = rev *10 + rem;
			temp = temp /10;
		}

		if(key == rev)
			return index;
		else
			return index = -1;

	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		for(int i=0; i<size; i++){
			int index = obj.palindromeEle(arr[i],i);	
			if(index != -1){
				flag = 1;
				System.out.println("palindrome number " + arr[i]+ " found at index : " + index);
			}
		}

		if(flag == 0)
			System.out.println("No palindrome number present in an array!");
	}	
}
