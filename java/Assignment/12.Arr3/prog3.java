/*
WAP to and a composite number from an array and return its index.
Take size and elements from the user
Input: 1 2 3 5 6 7
Output: composite 6 found at index: 4
*/

import java.io.*;
class ArrayDemo{

static int flag = 0;

	int compositeEle(int key,int index){
	
		int count = 0;
		for(int i=1; i<= key; i++){
			if(key % i == 0)
				count++;
		}

		if(count > 2)
			return index;
		else
			return index = -1;
	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		for(int i=0; i<size; i++){
			int index = obj.compositeEle(arr[i],i);	
			if(index != -1){
				flag = 1;
				System.out.println("composite " + arr[i]+ " found at index " + index);
			}
		}

		if(flag == 0)
			System.out.println("No composite numbers present in an array!");
	}	
}
