/*
WAP to find a Strong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564 145
Output: Strong no 145 found at index: 5
"if the sum of factorials of the input numbers every digit is equal to the same input number"
*/
import java.io.*;
class ArrayDemo{

static int flag = 0;

	int strongEle(int key,int index){
	
		int sum = 0;
		int temp = key;
		
		while(temp != 0){

			int rem = temp%10;
			int fact = 1;
			for(int i=1; i<=rem;i++){

				fact = fact * i;
			}
			sum = sum + fact;
			temp = temp/10;
		}

		if(key == sum)
			return index;
		else
			return index = -1;

	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		for(int i=0; i<size; i++){
			int index = obj.strongEle(arr[i],i);	
			if(index != -1){
				flag = 1;
				System.out.println("Strong number " + arr[i]+ " found at index : " + index);
			}
		}

		if(flag == 0)
			System.out.println("No Strong number present in an array!");
	}	
}
