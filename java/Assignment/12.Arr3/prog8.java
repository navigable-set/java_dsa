/*
WAP to find an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4
*/
import java.io.*;
class ArrayDemo{

static int flag = 0;

	int armstrongEle(int key,int index){
	
		int count = 0;
		int temp = key;

		while(temp != 0){

			count ++;
			temp = temp/10;
		}

		temp = key;
		int sum = 0;
			
		while(temp != 0){

			int mul =1;
			int rem = temp%10;
			for(int i = 1; i<= count; i++){
				mul = mul *rem;
			}
			sum = sum + mul;
			temp = temp /10;
		}

		if(key == sum)
			return index;
		else
			return index = -1;

	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		for(int i=0; i<size; i++){
			int index = obj.armstrongEle(arr[i],i);	
			if(index != -1){
				flag = 1;
				System.out.println("Armstrong number " + arr[i]+ " found at index : " + index);
			}
		}

		if(flag == 0)
			System.out.println("No Armstrong number present in an array!");
	}	
}
