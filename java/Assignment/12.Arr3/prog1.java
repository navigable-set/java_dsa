/*
Program 1
Write a program to print count of digits in elements of array.
Input: Enter array elements : 02 255 2 1554
Output: 2 3 1 4
*/
//fail for '02' : expected o/p = 2 || my o/p : 1

import java.io.*;
class ArrayDemo{

	int digitCount(int x){
	
		if(x == 0)
		 	return 1;

		int temp = x;
		int count = 0;
		
		while(temp != 0){
			count++;
			temp = temp/10;
		}
		return count;
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
		System.out.println("output :");
				
		for(int i=0; i<size; i++){
			int count = obj.digitCount(arr[i]);
			System.out.print(count + "\t");
		}	
		System.out.println();
	}	
}
