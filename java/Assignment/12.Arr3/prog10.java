/*
Write a program to print the second min element in the array
Input: Enter array elements: 255 2 1554 15 65 95 89
Output: 15
*/

import java.io.*;
class ArrayDemo{

	int secMinEle(int arr[], int size){

		int min = arr[0];
		int secMin = arr[0];

		if(arr[0] > arr[1]){

			min = arr[1];
			secMin = arr[0];
		}else{

			min = arr[0];
			secMin = arr[1];
		}
		
		for(int i= 1; i<size; i++){
			if(min > arr[i]){

				secMin = min;
				min = arr[i];		
			}
			if(secMin > arr[i] && min < arr[i]){

				secMin = arr[i];
			}
		}
		if(min == secMin){
			return secMin=-1;
		}
		return secMin ;
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		if(size > 1){
			int secMin = obj.secMinEle(arr,size);

			if(secMin!= -1)
				System.out.println("second min element in an array is : " + secMin);
			else
				System.out.println("second min element does not present in an array!!");
	
		}else{
			System.out.println("No second min value as array has only one element in it!");
		}
	}
}		
		
