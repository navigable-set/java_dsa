/*
WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3
" A NUMBER WHOSE SUM OF FACTORS EXCLUDING ITSELF IS EQUAL TO THE ORIGINAL NUMBER IS CALLED A PERFECT NUMBER"
*/

import java.io.*;
class ArrayDemo{

static int flag = 0;

	int perfectEle(int key,int index){
	
		int sum =0;
		for(int i=1; i< key; i++){
			if(key % i == 0)
				sum = sum + i;
		}

		if(sum == key)
			return index;
		else
			return index = -1;
	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		for(int i=0; i<size; i++){
			int index = obj.perfectEle(arr[i],i);	
			if(index != -1){
				flag = 1;
				System.out.println("Perfect number " + arr[i]+ " found at index : " + index);
			}
		}

		if(flag == 0)
			System.out.println("No perfect number present in an array!");
	}	
}
