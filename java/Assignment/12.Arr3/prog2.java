/*
WAP to reverse each element in an array.
Take size and elements from the user
Input: 10 25 252 36 564
Output: 01 52 252 63 465
*/
//fail to handle the condition  for : 10 = 01

import java.io.*;
class ArrayDemo{

	void reverseEle(int arr[],int size){
	
		for(int i=0; i<size; i++){

			int rev = 0;
			int temp = arr[i];

			while(temp != 0){
				
				/*if(temp%10 == 0){


				}*/

				int rem = temp%10;
				rev = rev *10 + rem;
				temp = temp /10;
			}
			arr[i] = rev;
		}

		System.out.println("Reversed array is :");

		for(int i=0; i<size; i++){
			
			System.out.print(arr[i] + "\t");
		}
	}
	
	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
			
		obj.reverseEle(arr,size);	

		System.out.println();
	}	
}
