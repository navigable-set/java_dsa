/*Write a program to print the second max element in the array
Input: Enter array elements: 2 255 2 1554 15 65
Output: 255
*/

import java.io.*;
class ArrayDemo{

	int secMaxEle(int arr[], int size){
		int max = arr[0];
		int secMax = -1;

		for(int i= 1; i<size; i++){
			if(max < arr[i]){
				secMax = max;
				max = arr[i];		
			}
			if(secMax < arr[i] && max > arr[i]){
				secMax = arr[i];
			}
		}
		return secMax;
	}

	public static void main(String [] args)throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		ArrayDemo obj = new ArrayDemo();
		
		System.out.println("Enter Array size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements :");

		for(int i=0; i<size; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array is :");
		for(int i=0; i<size; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();

		int secMax = obj.secMaxEle(arr,size);

		if(secMax != -1)
			System.out.println("second max element in an array is : " + secMax);
		else
			System.out.println("second max element does not present in an array!!");
	}	
}
