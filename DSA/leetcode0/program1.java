/*
1. Reverse Integer (Leetcode:- 7)
Given a signed 32-bit integer x, return x with its digits reversed. If reversing
x causes the value to go outside the signed 32-bit integer range [-231, 231- 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
Example 1:
Input: x = 123
Output: 321
Example 2:
Input: x = -123
Output: -321
Example 3:
Input: x = 120
Output: 21
Constraints:
-231 <= x <= 231 - 1
*/

import java.util.*;
class ReverseInt{

	static int getReverse(int val){

		int rev = 0;

		while(val!=0){

			rev = (rev*10) + (val%10);
			val = val/10;
		}

		return rev;
	}

	public static void main(String[] shweta){

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter any Integer number :");
		int val = sc.nextInt();

		int ret = getReverse(val);
		
		System.out.println("Output : " + ret);
	}
}
