/*
2. Two Sum (Leetcode:- 1)Given an array of integer numbers and an integer target, return indices of
the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you
may not use the same element twice.
You can return the answer in any order.
Example 1:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:
Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:
Input: nums = [3,3], target = 6
Output: [0,1]
Constraints:
2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.
*/

import java.util.*;
class TwoSum{

	static int getTwoSum(int arr[],int target){

		for(int i =0; i<arr.length-1; i++){

			if((arr[i] + arr[i+1]) == target)
				return i;
		}	

		return -1;
	}

	public static void main(String[] shweta){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i =0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter target value :");

		int target = sc.nextInt();

		int ret = getTwoSum(arr,target);

		if(ret == -1)
			System.out.println("Not found");
		else{
			int next = ret+1;
			System.out.println("Output : " + ret + " " + next);
		}
	}
}
