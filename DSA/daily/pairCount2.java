/*
given an character array(lower case) ..return the count of pair(i,j) such that,
a) i < j
b) arr[i] = 'a'
   arr[j] = 'g'

e.g. : [a,b,e,g,a,g]
o/p : 3
*/

//BRUTE-FORCE APPROACH
//t.c. = O(N^2)

class PairCount{

	static int getCount(char arr[], char ch1, char ch2){
		int pair = 0;
		int countA = 0;

		for(int i =0; i<arr.length; i++){

			if(arr[i] == ch1){
				countA++;
			}else if(arr[i] == 'g'){
				pair = pair + countA;
			}
		}

		return pair;
	}

	public static void main(String []shweta){

		char arr[] = new char[]{'a','b','e','g','a','g'};

		//given pair is of ==> 'a' and 'g'
		int count = getCount(arr,'a','g');

		System.out.println("count : " + count);
	}
}
