import java.util.*;
class SubArray{

	static int getSubArr(int arr[]){

		int len = Integer.MAX_VALUE;
		int minlen = Integer.MAX_VALUE;

		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for(int i=0; i<arr.length; i++){

			if(min > arr[i])
				min = arr[i];

			if(max < arr[i])
				max = arr[i];
		}

		for(int i =0; i<arr.length; i++){

			if(arr[i] == min){
				
				for(int j = i+1; j<arr.length; j++){

					if(arr[j] == max){
						len = j-i+1;			//output = end-start+1
						if(len < minlen)	
							minlen = len;
					}
				}
			}else if(arr[i] == max){
				
				for(int j = i+1; j<arr.length; j++){
					if(arr[j] == min){
						len = j-i+1;
						if(len < minlen)	
							minlen = len;
					}
				}
			}
		}

		return minlen;
	}

	public static void main(String []shweta){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size :");
		int size =sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		
		int len = getSubArr(arr);

		System.out.println("Output : " + len);
	}
}
