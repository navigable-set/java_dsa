//In Place 
//t.c = O(N)
//s.c = O(1)	

import java.util.*;
class PrefixSum{

	static void getPrefixSum(int arr[]){
		
		//no extra space for the extra array... so 's.c. = O(1)'
		for(int i = 1; i< arr.length; i++){

			arr[i] = arr[i] + arr[i-1];
		}

		for(int i =0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}

	public static void main(String []shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size =sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter array elements :");

                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }
		
		if(arr.length > 0){
			getPrefixSum(arr);
		}else{
			System.out.println("Invalid array size");
		}
	}
}
