       
import java.util.*;
class EquilibriumIdx{

/*
	static int getEquilibrium(int arr[]){

		for(int i =0; i<arr.length; i++){

			int leftSum = 0;
			int rightSum = 0;

			for(int j=0; j<arr.length; j++){
				
				if(j < i)
					leftSum = leftSum + arr[j];

				if(j > i)
					rightSum = rightSum + arr[j];
			}

			if(leftSum == rightSum)
				return ++i;
		}

		return -1;
	}
*/	
	static int getEquilibrium(int arr[]){

		for(int i =0; i<arr.length; i++){

			int leftSum = 0;
			int rightSum = 0;

			for(int j=0; j<i; j++){
					leftSum = leftSum + arr[j];
			}
			
			for(int j=i+1; j<arr.length; j++){
					rightSum = rightSum + arr[j];
			}

			if(leftSum == rightSum)
				return ++i;
		}
		return -1;
	}

	public static void main(String []shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size =sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter array elements :");

                for(int i=0; i<arr.length; i++){
                        arr[i] = sc.nextInt();
                }

		int index = getEquilibrium(arr);

		System.out.println("Output : " + index);
	}
}
