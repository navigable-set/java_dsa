//array rotate to right
import java.util.*;
class ArrayDemo{

	static void rotateArr(int arr[],int key){

		for(int i =1; i<= key; i++){

			int val = 0;

			for(int j=0; j<arr.length-1; j++){
				
				val = arr[j+1];
				arr[j+1] = arr[i];
				arr[j+2] = val;
			}
			arr[0] = val;
		}
	}
        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size :");
                int size = sc.nextInt();

                int arr[] = new int[size];


                System.out.println("Enter array elements : ");

                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }

		System.out.println("Enter key :");
		int key = sc.nextInt();

		rotateArr(arr,key);

		for(int x : arr){
			System.out.print(x + "	");
		}
		System.out.println();
	}
}
