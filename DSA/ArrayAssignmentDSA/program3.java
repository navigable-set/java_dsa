/*
Q3. Range Sum Query
Problem Description
- You are given an integer array A of length N.
- You are also given a 2D integer array B with dimensions M x 2, where each row denotes a [L, R] query.
- For each query, you have to find the sum of all elements from L to R indices in A (0 - indexed).
- More formally, find A[L] + A[L + 1] + A[L + 2] +... + A[R - 1] + A[R] for each query.
Problem Constraints
1 <= N, M <= 103
1 <= A[i] <= 105
0 <= L <= R < N
Example Input
Input 1:
A = [1, 2, 3, 4, 5]
B = [[0, 3], [1, 2]]
Input 2:
A = [2, 2, 2]
B = [[0, 0], [1, 2]]
Example Output
Output 1:
[10, 5]
Output 2:
[2, 4]
Example Explanation
Explanation 1:
The sum of all elements of A[0 ... 3] = 1 + 2 + 3 + 4 = 10.
The sum of all elements of A[1 ... 2] = 2 + 3 = 5.
Explanation 2:
The sum of all elements of A[0 ... 0] = 2 = 2.
The sum of all elements of A[1 ... 2] = 2 + 2 = 4.
======================================================================================
*/

import java.util.*;
class QuerySum{

	static void getQuerySum(int arr1[], int arr2[][]){

		int prefixArr[] = new int[arr1.length];
		prefixArr[0] = arr1[0];

		for(int i=1; i<arr1.length; i++){
			prefixArr[i] = arr1[i] + prefixArr[i-1];
		}

		int sum = 0;
		System.out.println("Output :");

		for(int i =0; i<arr2.length; i++){

			if(arr2[i][0] == 0)
				sum = prefixArr[arr2[i][1]];
			else
				sum = prefixArr[arr2[i][1]] - prefixArr[arr2[i][0] - 1] ;
				
			System.out.println(sum);
		}
	}
        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array1 size : ");
                int size = sc.nextInt();

                int arr1[] = new int[size];

                System.out.println("Enter array1 elements :");
                for(int i=0; i<arr1.length; i++){
                        arr1[i] =sc.nextInt();
                }
		
		System.out.println("Enter rows for array2(number of queries) :");
		int row = sc.nextInt();

		//since query contains start and end only..so col=2..but number of queries depends on user so 'row' is taken from user
		int arr2[][] = new int[row][2];	

		for(int i =0; i<arr2.length; i++){
			System.out.println("Enter left and right index for query :");
			for(int j = 0; j<arr2[i].length; j++){

				arr2[i][j] = sc.nextInt();
			}
		}

		getQuerySum(arr1,arr2);
	}
}
