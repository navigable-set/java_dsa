/*
Q4. Time to equality
Problem Description
- Given an integer array A of size N.
- In one second, you can increase the value of one element by 1.
- Find the minimum time in seconds to make all elements of the array
equal.
Problem Constraints
1 <= N <= 1000000
1 <= A[i] <= 1000
Example Input
A = [2, 4, 1, 3, 2]
Example Output
8Example Explanation
We can change the array A = [4, 4, 4, 4, 4]. The time required will be 8
seconds.
=======================================================================
==================
*/


import java.util.*;
class ArrayDemo{


	static int getSimilarArr(int arr[]){

		int count = 0;
		int max = Integer.MIN_VALUE;

		for(int i =0; i<arr.length; i++){

			if(max < arr[i])
				max = arr[i];
		}
		
		for(int i =0; i<arr.length; i++){

			while(arr[i] != max){
				arr[i] = arr[i] + 1;
				count++;
			}
		}

		return count;
	}

	public static void main(String[] shweta){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size:");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i=0; i<arr.length; i++){

			arr[i] = sc.nextInt();
		}
		
		int sec = getSimilarArr(arr);

		System.out.println("Output : " + sec);

		for(int i =0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}
}
