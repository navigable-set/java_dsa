/*
Q6. Product array puzzle
Problem Description
- Given an array of integers A, find and return the product array of the same
size where the ith element of the product array will be equal to the
product of all the elements divided by the ith element of the array.
- Note: It is always possible to form the product array with integer (32 bit)
values. Solve it without using the division operator.
Constraints
2 <= length of the array <= 1000
1 <= A[i] <= 10
For Example
Input 1:
A = [1, 2, 3, 4, 5]
Output 1:
[120, 60, 40, 30, 24]
Input 2:
A = [5, 1, 10, 1]
Output 2:
[10, 50, 5, 50]
======================================================================================
*/


import java.util.*;
class ArrayDemo{


	static void getProductArray(int arr[]){

		//to get multiplication of array elements
		int mul = 1;
		for(int i =0; i<arr.length; i++){
			mul = mul * arr[i];
		}

		//to get divion
		for(int i =0; i<arr.length; i++){
			int x = mul;
			int count = 0;
			while(x >= arr[i]){
				x = x - arr[i];
				count++;
			}
			arr[i] = count;
		}
	}

        public static void main(String[] shweta){

                Scanner sc = new Scanner(System.in);

                System.out.println("Enter array size:");
                int size = sc.nextInt();

                int arr[] = new int[size];

                System.out.println("Enter array elements :");
                for(int i=0; i<arr.length; i++){

                        arr[i] = sc.nextInt();
                }
		
		getProductArray(arr);

		System.out.println("Output : ");
		for(int i =0; i<arr.length; i++){
			System.out.print(arr[i] + "\t");
		}
		System.out.println();
	}
}
