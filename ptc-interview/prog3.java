/*reverse only the words of a string
 ##correctly workinh but not getting the speccing* also *no of tokens are manual
*/

import java.io.*;
import java.util.*;

class StringDemo{

	static void reverseWords(String str){

		//let's make token's of string so that we can separate words in a string
		StringTokenizer stt = new StringTokenizer(str, ",");
		
		String str1[] = new String[3];

		//copy these tokens into a string array
		for(int i = 0; i< 3; i++){
			str1[i] = stt.nextToken();
		}
		
		//lets reverse these tokens which are present in the string array using char array
		for(int i=0; i<str1.length; i++){
			char carr1[] = str1[i].toCharArray();
			char carr2[] = new char[carr1.length];

			int k = carr1.length-1;
			for(int j=0; j<carr1.length; j++){
				carr2[j] = carr1[k];
				k--;
			}
		//copying these reversed string's into the string array
			str1[i] = new String(carr2);
		}
		
		//converting string array to string using stringBuffer
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<str1.length; i++){
			sb.append(str1[i]);
		}	

		String result = sb.toString();
		
		System.out.println("result : " + result);
	}

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter the string :");
		String str = br.readLine();

		reverseWords(str);
	}
}
