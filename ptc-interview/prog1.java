/*WAP to search for a given string in an array of strings
 * input : shashi,ashish,kanha,rahul,badhe
 * search string : kanga
 * output : string found at index : 2*/

import java.io.*;
class StringDemo{

	public static void main(String [] shweta) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size:");
		int size = Integer.parseInt(br.readLine());

		String arr[] = new String[size];

		System.out.println("Enter the strings :");

		for(int i = 0; i<arr.length; i++){
			arr[i] = br.readLine();
		}

		System.out.println("Enter the string to search :");
		String search = br.readLine();

		int index = -1;
		for(int i = 0 ; i<arr.length; i++){
			if(arr[i].equals(search)){
				index = i;
				break;
			}
		}

		if(index == -1){
	
			System.out.println(search +" is not present in given string array");
		}else{

			System.out.println(search +" found at index " + index);
		}
	}
}
