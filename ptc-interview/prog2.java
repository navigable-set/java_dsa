/*WAP to reverse a givem string
 * input : Core2Web Tech
 * output : hceT beW2eroC*/ 


import java.io.*;
class StringDemo{

	static void reverseString(String str){

		char carr1[] = str.toCharArray();
		char carr2[] = new char[carr1.length];

		int k = carr1.length -1;
		for(int i = 0; i< carr2.length; i++){
			carr2[i] = carr1[k];
			k--;
		}

		String str2 = new String(carr2);

		/*String str3 = String.valueOf(carr2); ==> also converts char array to string
		 *String str4 = String.copyValueOf(carr2); 
		 */

		System.out.println("Reverse String is :");
		System.out.println(str2);
	}

	public static void main(String[] shweta) throws IOException{

		BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the String :");
		String str = br.readLine();
	
		/*using StringBuffer -> reverse method :
	
		StringBuffer str2 = new StringBuffer(str);

		str = str2.reverse().toString();
		System.out.println(str3);*/

		reverseString(str);
	}
}
